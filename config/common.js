var nodemailer = require('nodemailer');

function Common(){};

Common.prototype.objToStr = function(obj) {
    var str = '';
    for (var p in obj) {
        if (obj.hasOwnProperty(p)) {
            str += p + '::' + obj[p] + '\n';
        }
    }
    return str;
};

Common.prototype.checkAuthentication = function(req, res, next) {
    if(req.xhr || req.headers.accept.indexOf('json') > -1) {
        if (req.isAuthenticated()) {
            return next();
        } else {
            res.status('401').json('Não autorizado');
        }
    } else {
        res.redirect('/painel/auth/logout');
    }
};

Common.prototype.checkUrlAngularJs = function(req, res, next) {
    if(req.xhr || req.headers.accept.indexOf('json') > -1) {
        if (req.isAuthenticated()) {
            return next();
        } else {
            res.status('401').json('Não autorizado');
        }
    } else {
        res.redirect('/#/' + req.url);
    }
};

Common.prototype.checkAuthenticationRestrito = function(req, res, next) {
    if(req.xhr || req.headers.accept.indexOf('json') > -1) {
        console.log(req);
        if (req.isAuthenticated()) {
            return next();
        } else {
            res.status('401').json('Não autorizado');
        }
    } else {
        res.redirect('restrito/auth/logout');
    }
};

Common.prototype.removeOfList = function(val) {
    var i = this.indexOf(val);
    return i>-1 ? this.splice(i, 1) : [];
};

Common.prototype.sendMail = function(To, Subject, Body, cb) {
    var poolConfig = {
        pool: true,
        requiresAuth: true,
        port: parseInt(587, 10),
        host: 'smtp.sendgrid.net',
        auth: {
            user: 'michael.lincohn@gmail.com',
            pass: 'j3q6d2a1@'
        }
    };
    var mailOptions = {
        from: 'GuiaDr <relacionamento@guiadr.com.br>',
        to: To,
        subject: Subject,
        html: Body
    };

    var transporter = nodemailer.createTransport(poolConfig);
    transporter.sendMail(mailOptions, function(error, info) {
        if(error) {
            cb(true, error);
        }
        cb(false, info.response);
    });
}

module.exports = new Common();