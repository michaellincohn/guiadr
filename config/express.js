var express = require('express');
var load = require('express-load');
var session = require('express-session');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var methodOverride = require('method-override');
var passport = require('passport');
var morgan = require('morgan');
var helmet = require('helmet');
var flash = require('connect-flash');
//var csrf = require('csrf');

module.exports = function () {
    var app = express();
    
    app.set('view engine', 'ejs');
    app.set('views', './app/views');
    //app.use(morgan('dev'));

    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());
    app.use(methodOverride());
    app.use(require('method-override')());
    app.use(express.static('./public'));

    app.use(cookieParser());
    app.use(session({
        secret: 'guiadropenshift', 
        resave: false, 
        saveUninitialized: true 
    }));

    var env = process.env.NODE_ENV || 'development';
    if ('development' === env || 'production' === env) {
        //app.use(csrf());
        //app.use(function(req, res, next) {
            //res.cookie('XSRF-TOKEN', req.csrfToken());
            //next();
        //});
    }

    app.use(passport.initialize());
    app.use(passport.session());

    app.use(helmet.xframe());
    app.use(helmet.xssFilter());
    app.use(helmet.nosniff());
    app.disable('x-powered-by');

    app.use(flash());

    load('models', { cwd:'app' })
        .then('controllers/painel')
        .then('controllers/restrito')
        .then('controllers/portal')
        .then('routes/painel/auth.js')
        .then('routes/painel')
        .then('routes/restrito/auth.js')
        .then('routes/restrito')
        .then('routes/portal')
        .into(app);

    app.get('*', function(req, res) {
        res.status(404).render('404');
    });

    return app;
};