function Template(){}; 

Template.prototype.cadastroProfissional = function(obj) {
    var str = '';
    str = 'Olá ' + obj.nome + '!<br /><br />';
    str += 'Obrigado por se cadastrar no GuiaDr!<br />';
    str += 'Abaixo está seus dados de acesso à sua área restrita.<br /><br />';
    str += '==== <b>DADOS DE ACESSO</b> ====<br />';
    str += 'Nome: ' + obj.nome + '<br />';
    str += 'E-mail: ' + obj.email + '<br />';
    str += 'Senha: **** <br />';
    str += 'Link: <a href="http://www.guiadr.com.br/#/login">http://www.guiadr.com.br/#/login</a><br />';
    str += '====-====';
    str += '<br /><br />';
    str += 'Ótimas Consultas! <br />';
    str += 'Equipe GuiaDr. Sua plataforma de consultas médicas! <br />';
    return str;
};

Template.prototype.contato = function(obj) {
    var str = '';
    str = 'Olá ' + obj.nome + '!<br />';
    str += 'Seja bem vindo ao GuiaDr!<br />';
    str += 'Agradecemos seu contato e em breve iremos retornar.<br />';
    str += '<br />=====<br />';
    str += 'Nome: ' + obj.nome + '<br />';
    str += 'E-mail: ' + obj.email + '<br />';
    str += 'Mensagem: ' + obj.mensagem + '<br />';
    //str += 'Enviado em: ' + new Date.now() + '<br />';
    str += '=====';
    return str;
};

module.exports = new Template();