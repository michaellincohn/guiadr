var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var mongoose = require('mongoose');
var bCrypt = require('bcrypt-nodejs');
var Usuario = mongoose.model('Usuario');

module.exports = function(passport, ipaddress) {
    if(ipaddress == "127.0.0.1") {
        var callbackUrl = "http://localhost:8080/cadastro/paciente/facebook/callback";
    } else {
        var callbackUrl = "http://guiadr-michaellincohn.rhcloud.com/cadastro/paciente/facebook/callback";
    }

    passport.use('local', new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password',
            passReqToCallback: true
        },
        function(req, username, password, done) {
            var findUser = function() {
                Usuario.findOne({ email: username, tipo: 1 }, function(err, usuario) {
                    if(err)
                        return done(err);
                    
                    if (!usuario) {
                        return done(null, false, req.flash('message', 'Usuário não encontrado'));
                    }
                    
                    if (!isValidPassword(usuario, password)) {
                        return done(null, false, req.flash('message', 'Senha inválida'));
                    }
                    
                    return done(null, usuario);
                });
            };

            process.nextTick(findUser);
        })
    );

    passport.use('restrito', new LocalStrategy({
            usernameField: 'username',
            passwordField: 'password',
            passReqToCallback: true
        },
        function(req, username, password, done) {
            var findUser = function() {
                Usuario.findOne({ email: username, tipo: { $in: [2, 4] } }, function(err, usuario) {
                    if(err)
                        return done(err);
                    
                    if (!usuario) {
                        return done(null, false, req.flash('message', 'Usuário não encontrado'));
                    }

                    if (!isValidPassword(usuario, password)) {
                        return done(null, false, req.flash('message', 'Senha inválida'));
                    }
                    
                    return done(null, usuario);
                });
            };

            process.nextTick(findUser);
        })
    );

    passport.use('facebook', new FacebookStrategy({
            clientID: '1057725027602211',
            clientSecret: '0024042194a4cb37331cc2a62b48e923',
            callbackURL: callbackUrl,
            profileFields: ['id', 'displayName', 'email', 'photos', 'profileUrl'],
            enableProof: true
        },
        function(accessToken, refreshToken, profile, done) {
            findUser = function() {
                var result = profile._json;
                Usuario.findOne({ email: result.email }, function(err, user) {
                    if (!err) {
                        if (user) {
                            return done(null, user);
                        } else {
                            var imagem = null;
                            if(result.picture != null) {
                                if(result.picture.data != null) {
                                    if(result.picture.data.url != null) {
                                        imagem = result.picture.data.url;
                                    }
                                }
                            }

                            var usuario = new Usuario();
                            usuario.nome = result.name;
                            usuario.email = result.email;
                            usuario.tipo = 4;
                            usuario.authredesociais = {
                                facebook: { profileId: result.id, accessToken: accessToken, image: imagem }
                            };
                            usuario.redesociais = { facebook: result.link };
                            usuario.save(function(err) {
                                if(err) throw err;
                                return done(null, usuario);
                            });
                        }
                    }
                });
            };

            process.nextTick(findUser);
        })
    );

    var isValidPassword = function(user, password) {
        return bCrypt.compareSync(password, user.password);
    }

    var createHash = function(password){
        return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    }

    passport.serializeUser(function(user, done) {
        done(null, user._id);
    });

    passport.deserializeUser(function(id, done) {
        Usuario.findById(id, function(err, user) {
            done(err, user);
        });
    });
};