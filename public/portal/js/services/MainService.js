(function () {
    var Api = function($resource) {
        return {
            Contato: $resource('/contato'),
            Disponibilidade: $resource('/disponibilidade/:id', {id: '@id'}, {get:{method:'GET', isArray:true}}),
            Mensagem: $resource('/mensagem/:id', {id: '@id'}),
            Comentario: $resource('/comentario'),
            Especialidade: $resource('/especialidade/:id', {id: '@id'}),
            Editoria: $resource('/editoria', {query:{method:'GET', isArray:true}}),
            Categoria: $resource('/categoria', {query:{method:'GET', isArray:true}}),
            Blog: $resource('/blog/:id', {id: '@id'}, {get:{method:'GET', isArray:true}}),
            Post: $resource('/blog/:id', {id: '@id'}),
            Pagina: $resource('/pagina/:page', {page: '@page'}, {get:{method:'GET', isArray:true}}),
            Profissional: $resource('/profissional/:id', {id: '@id'}, {get:{method:'GET', isArray:true}, query:{method:'GET', isArray:false}}),
            ProfissionalCad: $resource('/profissional'),
            Clinica: $resource('/clinica/:id', {id: '@id'}, {get:{method:'GET', isArray:true}}),
            EstadoClinica: $resource('/estadoClinica', {query:{method:'GET', isArray:true}}),
            Usuario: $resource('/usuario/:id', {id: '@id'}),
            Pais: $resource('/pais/:id', {id: '@id'}),
            Estado: $resource('/estado/:id', {id: '@id'}),
            Cidade: $resource('/cidade/:id', {id: '@id'}),
            Bairro: $resource('/bairro/:id', {id: '@id'}),
            Logradouro: $resource('/logradouro/:id', {id: '@id'}),
        };
    };

    var fileReader = function($q) {
        var onLoad = function (reader, deferred, Sscope) {
            return function () {
                Sscope.$apply(function () {
                    deferred.resolve(reader.result);
                });
            };
        };

        var onError = function (reader, deferred, Sscope) {
            return function () {
                Sscope.$apply(function () {
                    deferred.reject(reader.result);
                });
            };
        };

        var onProgress = function (reader, Sscope) {
            return function (event) {
                Sscope.$broadcast(
                    "fileProgress", {
                        total: event.total, 
                        loaded: event.loaded
                    }
                );
            };
        };

        var getReader = function (deferred, Sscope) {
            var reader = new FileReader();
            reader.onload = onLoad(reader, deferred, Sscope);
            reader.onerror = onError(reader, deferred, Sscope);
            reader.onprogress = onProgress(reader, Sscope);
            return reader;
        };

        var readAsDataURL = function (file, Sscope) {
            var deferred = $q.defer();
            var reader = getReader(deferred, Sscope);
            reader.readAsDataURL(file);
            return deferred.promise;
        };

        return { readAsDataUrl: readAsDataURL };
    };

    angular.module('guiadr')
        .factory('Api', Api)
        .factory('fileReader', fileReader);
}());