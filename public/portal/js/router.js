angular.module('guiadr', ['ngRoute', 'ngResource', 'ngAnimate', 'ngSanitize', 'ui.bootstrap', 'angular-loading-bar', 'ngTagsInput', 'ngFileUpload', 'ngJcrop', 'infinite-scroll', 'ngScrollTo'])
.config(['$routeProvider', '$httpProvider', 'cfpLoadingBarProvider', 'ngJcropConfigProvider', 'ngScrollToOptionsProvider', 
    function($routeProvider, $httpProvider, cfpLoadingBarProvider, ngJcropConfigProvider, ngScrollToOptionsProvider) {

    $routeProvider.when('/erro/:registerId', { templateUrl: 'portal/partials/single/error.html', controller: 'ErrorCtrl' });

    $routeProvider.when('/home', { templateUrl: 'portal/partials/single/index.html', controller: 'IndexCtrl' });
    $routeProvider.when('/p/:registerPage', { templateUrl: 'portal/partials/single/pagina.html', controller: 'PaginaCtrl' });
    $routeProvider.when('/contato', { templateUrl: 'portal/partials/single/contato.html', controller: 'ContatoCtrl' });
    
    $routeProvider.when('/blog', { templateUrl: 'portal/partials/blog/lista.html', controller: 'BlogCtrl' });
    $routeProvider.when('/blog/:registerId', { templateUrl: 'portal/partials/blog/detalhes.html', controller: 'BlogCtrl' });

    //$routeProvider.when('/profissional/segmento/:registerId', { templateUrl: 'portal/partials/profissional/segmento/list.html', controller: 'SegmentoController' });
    $routeProvider.when('/profissional', { templateUrl: 'portal/partials/profissional/lista.html', controller: 'ProfissionalCtrl' });
    $routeProvider.when('/profissional/:registerId', { templateUrl: 'portal/partials/profissional/detalhes.html', controller: 'ProfissionalCtrl' });

    $routeProvider.when('/cadastro', { templateUrl: 'portal/partials/cadastro/escolha.html' });
    $routeProvider.when('/cadastro/paciente', { templateUrl: 'portal/partials/cadastro/paciente.html' });
    $routeProvider.when('/cadastro/profissional', { templateUrl: 'portal/partials/cadastro/profissional.html', controller: 'CadastroCtrl' });
    $routeProvider.when('/cadastro/paciente/form', { templateUrl: 'portal/partials/cadastro/pacienteForm.html', controller: 'CadastroCtrl' });

    $routeProvider.otherwise({ redirectTo: '/home' });

    // https://github.com/chieffancypants/angular-loading-bar
    cfpLoadingBarProvider.includeSpinner = false;
    cfpLoadingBarProvider.includeBar = true;
}])

.run(function ($rootScope, $location, $window) {
    $rootScope.$on('$routeChangeSuccess', function(e) {
        if (!$window.ga) { return; }
        $window.ga('create', 'UA-61456195-2', 'auto');
        $window.ga('send', 'pageview', { page: $location.path() });
    });
});