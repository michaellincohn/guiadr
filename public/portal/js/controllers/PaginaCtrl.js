angular.module('guiadr').controller('PaginaCtrl', function (Api, $scope, $sce, $routeParams, $location) {
    Api.Pagina.get({page:$routeParams.registerPage}, function (registro) {
        if(registro.length > 0) {
            $scope.titulo = registro[0].titulo;
            $scope.conteudo = $sce.trustAsHtml(registro[0].conteudo);
        } else {
            $location.path('erro/404');
        }
    }, function(erro) {
        $location.path('erro/404');
    });
});