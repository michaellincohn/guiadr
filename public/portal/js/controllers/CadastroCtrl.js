angular.module('guiadr').controller('CadastroCtrl', function (Api, $scope, $http, $routeParams, $location, $timeout) {
    $scope.espec = [];
    $scope.cadastro = {}
    //$scope.cadastro.latlong = "";
    $scope.planos = [
        {tipo: 1, valor: '100,00', titulo: 'Simples', texto: 'Atendimento em clínica das 08h às 18h'},
        {tipo: 2, valor: '180,00', titulo: 'Urgência', texto: 'Atendimento em clínica das 18h às 07h'},
        {tipo: 3, valor: '200,00', titulo: 'Domiciliar', texto: 'Atendimento em domicílio'}
    ];
    $scope.$watch('cadastro.descricao', function(text) {
        if(!text) {
            $scope.wordCountDescricao = 0;
        } 
        else {
            $scope.wordCountDescricao = text.length;
        }
    });
    $scope.$watch('cadastro.educacao', function(text) {
        if(!text) {
            $scope.wordCountEducacao = 0;
        } 
        else {
            $scope.wordCountEducacao = text.length;
        }
    });
    $scope.$watch('tabCadastro', function(page) {
        if(page == 3) {
            Api.Categoria.query(function (registros) {
                if(registros) {
                    $scope.categorias = registros;
                }
            });

            $(".select2To").select2({width: '100%', minimumResultsForSearch: Infinity});
        }
    });

    listAllPais();

    /*$scope.getLocation = function (val) {
        return $http.get('https://maps.googleapis.com/maps/api/geocode/json', {
            params: {
                address: val,
                components: 'country:Brasil',
                sensor: false,
                key: 'AIzaSyDZJPxciXBxK3kkGyQePKy9Awk4ChKrNgQ'
            }
        }).then(function(response) {
            return response.data.results.map(function(item) {
                return item;
            });
        }, function(err) {
            console.log(err);
        });
    };

    $scope.onSelect = function($item, $model, $label) {
        if($item.geometry.location != null) {
            $scope.cadastro.latlong = $item.geometry.location.lat + '|' + $item.geometry.location.lng;
        }

        angular.forEach($item.address_components, function(value, key) {
            if(isInArray(value.types,'route')) {
                $scope.cadastro.logradouro = value.long_name;
            } else if(isInArray(value.types,'sublocality_level_1') || isInArray(value.types,'sublocality')) {
                $scope.cadastro.bairro = value.long_name;
            } else if(isInArray(value.types,'locality')) {
                $scope.cadastro.cidade = value.long_name;
            } else if(isInArray(value.types,'administrative_area_level_1')) {
                $scope.cadastro.estado = value.long_name;
            } else if(isInArray(value.types,'country')) {
                $scope.cadastro.pais = value.long_name;
            }
        });
    };

    $scope.getEspecialidade = function(agendamento) {
        $timeout(function() {
            //$(document).find('.icheck').iCheck({checkboxClass:'icheckbox_flat-green', radioClass:'iradio_flat-green'});
        }, 100);
    };*/

    function listAllPais() {
        Api.Pais.get({limit:1}, function(registros) {
            $scope.paises = registros.itens;
        });
    }

    function getEstado(registro) {
        Api.Estado.get({busca:[{key:'pais', value:registro, type:'normal'}], limit:1000}, function(registros) {
            $scope.estados = registros.itens;
        });
    }

    function getCidade(registro) {
        Api.Cidade.get({busca:[{key:'estado', value:registro, type:'normal'}], limit:1000}, function(registros) {
            $scope.cidades = registros.itens;
        });
    }

    function getBairro(registro) {
        Api.Bairro.get({busca:[{key:'cidade', value:registro, type:'normal'}], limit:1000}, function(registros) {
            $scope.bairros = registros.itens;
        });
    }

    $scope.changePais = function (registro) {
        getEstado(registro);
    };

    $scope.changeEstado = function (registro) {
        getCidade(registro);
    };

    $scope.changeCidade = function (registro) {
        getBairro(registro);
    };

    $scope.buscaCep = function () {
        var registro = $scope.registro.logradouro;
        if(registro != null && !isNaN(registro.cep) && registro.cep != '' && registro.cep.length == 8) {
            Api.Logradouro.get({busca:[{key:'cep', value:registro.cep, type:'normal'}], limit:1}, function(endereco) {
                if(endereco.allItens > 0) {
                    var item = endereco.itens[0];
                    $('#btnBuscarCep').text('Encontrado!').removeClass('btn-inverse').removeClass('btn-danger').addClass('btn-success');
                    Api.Pais.get({limit:1}, function(registrosPaises) {
                        $scope.paises = registrosPaises.itens;

                        $scope.cadastro.pais = item.pais._id;
                        getEstado(item.pais._id);
                        $scope.cadastro.estado = item.estado._id;
                        getCidade(item.estado._id);
                        $scope.cadastro.cidade = item.cidade._id;
                        getBairro(item.cidade._id);
                        $scope.cadastro.bairro = item.bairro._id;
                        $scope.logradouro = item.titulo;
                        $scope.cadastro.logradouro = item._id;
                    });
                } else {
                    $http.get('http://api.postmon.com.br/cep/' + registro.cep).success(function(endereco) {
                        if(endereco != null) {
                            console.log(endereco);
                            $('#btnBuscarCep').text('Encontrado').removeClass('btn-inverse').removeClass('btn-danger').addClass('btn-success');
                            $scope.logradouro = endereco.logradouro;

                            Api.Pais.get({limit:1}, function(registrosPaises) {
                                $scope.paises = registrosPaises.itens;
                            });

                            var estado = new Api.Estado();
                            estado.pais = '570040e256ab833ae04bf117'; // Brasil
                            estado.uf = endereco.estado;
                            estado.titulo = endereco.estado_info.nome;
                            estado.$save().then(function(regEstado) {
                                getEstado(regEstado.reg._id);
                                $scope.cadastro.pais = regEstado.reg.pais;
                                $scope.cadastro.estado = regEstado.reg._id;

                                var cidade = new Api.Cidade();
                                cidade.pais = regEstado.reg.pais;
                                cidade.estado = regEstado.reg._id;
                                cidade.titulo = endereco.cidade;
                                cidade.$save().then(function(regCidade) {
                                    getCidade(regCidade.reg._id);
                                    $scope.cadastro.cidade = regCidade.reg._id;

                                    var bairro = new Api.Bairro();
                                    bairro.pais = regCidade.reg.pais;
                                    bairro.estado = regCidade.reg.estado;
                                    bairro.cidade = regCidade.reg._id;
                                    bairro.titulo = endereco.bairro;
                                    bairro.$save().then(function(regBairro) {
                                        getBairro(regBairro.reg._id);
                                        $scope.cadastro.bairro = regBairro.reg._id;

                                        var logradouro = new Api.Logradouro();
                                        logradouro.pais = regBairro.reg.pais;
                                        logradouro.estado = regBairro.reg.estado;
                                        logradouro.cidade = regBairro.reg.cidade;
                                        logradouro.bairro = regBairro.reg._id;
                                        logradouro.cep = endereco.cep;
                                        logradouro.tipo = '';
                                        logradouro.titulo = endereco.logradouro;
                                        logradouro.$save().then(function(regLogradouro) {
                                            $scope.cadastro.logradouro = regLogradouro.reg._id;
                                        });
                                    });
                                });
                            });
                        } else {
                            $('#btnBuscarCep').text('Não encontrado').removeClass('btn-inverse').removeClass('btn-success').addClass('btn-danger');
                        }
                    }).error(function(err) {
                        console.log(err);
                        $('#btnBuscarCep').text('Não encontrado').removeClass('btn-inverse').removeClass('btn-success').addClass('btn-danger');
                    });
                }
            });
        } else {
           $('#btnBuscarCep').text('Digite o CEP').removeClass('btn-inverse').removeClass('btn-success').addClass('btn-danger'); 
        }
    };

    $scope.gravarDadosBasicos = function(dados) {
        dados['step'] = 1;
        dados['_id'] = null;
        dados['plano'] = null;
        $scope.originalRegistro = angular.copy(dados);
        $("#btnCadastroProfissional").text("Aguarde!");
        Api.Profissional.save(dados, function (user) {
            if(user) {
                $scope.dataProfissional = user;
                $("#btnCadastroProfissional").text("Continuar");
                $scope.tabCadastro = 4;
            } else {
                $("#btnCadastroProfissional").text("Tente novamente mais tarde!");
            }
        }, function(err) {
            $("#btnCadastroProfissional").text("Não salvo!");
        });
    }

    $scope.gravarDadosPessoais = function(dados) {
        $scope.originalRegistro = angular.copy(dados);
        $("#btnCadastroPessoais").text("Aguarde!");
        dados['termo'] = true;
        Api.Usuario.save(dados, function (user) {
            if(user) {
                $("#btnCadastroPessoais").text("Continuar");
                $scope.tabCadastroPaciente = 3;
            } else {
                $("#btnCadastroPessoais").text("Tente novamente mais tarde!");
            }
        }, function(err) {
            $("#btnCadastroPessoais").text(err.data.mensagem);
        });
    }

    $scope.gravarCategorias = function(dados) {
        dados['step'] = 2;
        dados['_id'] = $scope.dataProfissional._id;

        var especialidades = []
        for (var i = 0; i < $scope.espec.length; i++) {
            especialidades.push({
                "id": $scope.espec[i].id,
                "titulo": $scope.espec[i].titulo
            });
        }
        dados['especialidade'] = especialidades;

        $scope.originalRegistro = angular.copy(dados);
        $("#btnCadastroCategorias").text("Aguarde!");
        Api.Profissional.save(dados, function (user) {
            if(user) {
                $scope.tabCadastro = 5;
            } else {
                $("#btnCadastroCategorias").text("Tente novamente mais tarde!");
            }
        }, function(err) {
            $("#btnCadastroCategorias").text("Não salvo!");
        });
    }

    $scope.gravarEndereco = function(endereco) {
        endereco['step'] = 3;
        endereco['_id'] = $scope.dataProfissional._id;
        $scope.originalRegistro = angular.copy(endereco);
        $("#btnCadastroEndereco").text("Aguarde!");
        Api.Profissional.save(endereco, function (user) {
            if(user) {
                $scope.tabCadastro = 6;
            } else {
                $("#btnCadastroEndereco").text("Tente novamente mais tarde!");
            }
        }, function(err) {
            console.log(err);
            $("#btnCadastroEndereco").text("Não salvo!");
        });
    }

    $scope.definirPlano = function(plano) {
        console.log(plano);
        plano['step'] = 4;
        plano['_id'] = $scope.dataProfissional._id;
        $scope.originalRegistro = angular.copy(plano);
        Api.Profissional.save(plano, function (user) {
            if(user) {
                $scope.tabCadastro = 6;
            }
        }, function(err) {
            console.log(err);
        });
    }

    $scope.change = function(c) {
        var espec = {};
        espec.id = c._id;
        espec.titulo = c.titulo;

        $scope.espec.push(espec);
    }

    var isInArray = function (array, search) {
        return array.indexOf(search) >= 0;
    }
});