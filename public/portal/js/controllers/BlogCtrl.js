angular.module('guiadr').controller('BlogCtrl', function (Api, $scope, $routeParams, $sce, BlogPaging) {
    if ($routeParams.registerId) {
        getById();
    } else {
        listAll();
    }

    function listAll() {
        $scope.blog = new BlogPaging();

        listUltimasPostagens();
        listEditoria();
    }

    function listUltimasPostagens() {
        Api.Blog.get({limit:7}, function (registros) {
            $scope.ultimosPosts = registros;
        });
    }

    function listEditoria() {
        Api.Editoria.query(function (registros) {
            $scope.editorias = registros;
        });
    }

    function getById() {
        dados = { id: $routeParams.registerId };
        Api.Post.get(dados, function (registro) {
            $scope.post = registro.post;
            $scope.conteudo = $sce.trustAsHtml(registro.post.conteudo);

            var regs = [];
            if(registro.comments.length > 0) {
                angular.forEach(registro.comments, function(v, k) {
                    if(v.isAtivo)
                        this.push({nome: v.nome, descricao: v.descricao, data: v.dtInclusao});
                }, regs);
            }
            $scope.comentarios = regs;

            Api.Post.save(
                { post: registro.post, idPost: dados.id, visualizacoes:true }, 
                function(res) {
                    $('span#visualizacoes').text(res.dados.visualizacoes);
                }
            );
        });
    }

    $scope.comentar = function (comentario) {
        $scope.originalRegistro = angular.copy(comentario);
        $("#botaoEnviar").text("Aguarde! Estamos enviando...");
        Api.Comentario.save(
            { comentario: comentario, idPost: $routeParams.registerId, module: 'blog'},
            function (res) {
            $("#botaoEnviar").text(res.mensagem).removeClass("btn-primary").removeClass("btn-danger").addClass(res.class);
        }, function() {
            $("#botaoEnviar").text("Por favor, tente mais tarde!").removeClass("btn-danger").addClass("btn-primary");
        });
    };

    $scope.curtidas = function (event, registro) {
        Api.Post.save(
            { post: registro, idPost: $routeParams.registerId, curtidas:true }, 
            function(res) {
                $('span#curtidas').text(res.dados.curtidas);
            }
        );
    };
});

angular.module('guiadr').factory('BlogPaging', function(Api) {
    var BlogPaging = function() {
        this.items = [];
        this.busy = false;
        this.page = 1;
    };

    BlogPaging.prototype.nextPage = function() {
        if (this.busy) return;
        this.busy = true;

        if(this.page <= 20) {
            Api.Blog.get({page: this.page}, function (registros) {
                if(registros) {
                    if(registros.length == 0) {
                        this.page = 21;
                    }

                    var itens = registros;
                    for (var i = 0; i < itens.length; i++) {
                        this.items.push(itens[i]);
                    }
                    this.page += 1;
                }
                this.busy = false;
            }.bind(this));
        }
    };

    return BlogPaging;
});