angular.module('guiadr').controller('ProfissionalCtrl', function (Api, $scope, $routeParams, $location, $timeout, ProfissionalPaging) {
    $scope.isDisponivel = false;
    $scope.profissionais = [];
    $scope.disponibilidades = [];
    $scope.comentarios = new Api.Comentario();

    if ($routeParams.registerId) {
        getById();
    } else {
        listAll();
    }

    function listAll() {
        $scope.profissionais = new ProfissionalPaging();
    }

    function getById() {
        Api.Profissional.query({id: $routeParams.registerId}, function (registro) {
            if (registro) {
                $scope.profissional = registro;
                $scope.enderecoMapa = (registro.endereco.length > 0 ? registro.endereco[0].latlong : '');

                var totalHabilidade = 0;
                var totalEspecialidade = 0;

                var habilidade1 = [];
                var habilidade2 = [];
                var especialidade1 = [];
                var especialidade2 = [];
                for (var i = 0; i < registro.categoria.length; i++) {
                    for (var e = 0; e < registro.categoria[i].especialidade.length; e++) {
                        var especialidade = registro.categoria[i].especialidade[e];
                        if (e % 2 === 0) {
                            especialidade1.push(especialidade);
                        } else {
                            especialidade2.push(especialidade);                    
                        }
                        totalEspecialidade += 1;
                    }

                    for (var h = 0; h < registro.categoria[i].habilidade.length; h++) {
                        var habilidade = registro.categoria[i].habilidade[h];
                        if (h % 2 === 0) {
                            habilidade1.push(habilidade);
                        } else {
                            habilidade2.push(habilidade);
                        }
                        totalHabilidade += 1;
                    }
                }
                $scope.totalHabilidade = totalHabilidade;
                $scope.totalEspecialidade = totalEspecialidade;

                $scope.habilidades1 = habilidade1;
                $scope.habilidades2 = habilidade2;
                $scope.especialidades1 = especialidade1;
                $scope.especialidades2 = especialidade2;

                var regs = [];
                if(registro.comentario.length > 0) {
                    angular.forEach(registro.comentario, function(v, k) {
                        if(v.isAtivo)
                            this.push({nome: v.nome, descricao: v.descricao, data: v.dtInclusao});
                    }, regs);
                }
                $scope.comentarios = regs;

                $(document).find(".select2To").select2({width: '100%', minimumResultsForSearch: Infinity});

                getDisponibilidade($routeParams.registerId);
            }
        });
    }

    $scope.abrirFormComentario = function() {
         $(document).find('#add_review_wrapper').animate({ opacity: '1', height: '100%' }, 500);
    }

    $scope.comentar = function (comentario) {
        $scope.originalRegistro = angular.copy(comentario);
        $("#botaoEnviarComentario").text("Aguarde!");
        Api.Comentario.save(
            { comentario: comentario, idPost: $routeParams.registerId, module: 'profissional' },
            function (res) {
            $("#botaoEnviarComentario").text(res.mensagem).removeClass("btn-default").removeClass("btn-danger").addClass(res.class);
        }, function() {
            $("#botaoEnviarComentario").text("Tente mais tarde!").removeClass("btn-danger").addClass("btn-default");
        });
    };

    function getDisponibilidade(id) {
        Api.Disponibilidade.get({medico:id}, function (registros) {
            if(registros) {
                $scope.disponibilidades = registros;
                $scope.isDisponivel = (registros.length > 0);
            }
        });
    }

    $scope.getEspecialidade = function(agendamento) {
        var registro = agendamento.data;
        $scope.categorias = registro.categoria.especialidade;
        $scope.horarios = registro.horario;
    };

    $scope.agendamentoLoginUser = function(login) {
        $scope.originalRegistro = angular.copy(login);
        $("#btnLogin").text("Aguarde!");

        if(login == null || login.email == null) {
            $("#btnLogin").text("Digite seu e-mail!");
        }
        else if(login.senha == null) {
            $("#btnLogin").text("Digite sua senha!");
        }
        else {
            Api.Usuario.get({ email:login.email, password:login.senha }, function (user) {
                if(user) {
                    $scope.dataUsuario = user;
                    $scope.tabAgendamento = 3;
                }
            }, function(err) {
                $("#btnLogin").text("Não encontrado!");
            });
        }
    }

    $scope.agendamentoCadastroUser = function(cadastro) {
        $scope.originalRegistro = angular.copy(cadastro);
        $("#btnCadastro").text("Aguarde!");

        if(cadastro == null || cadastro.nome == null) {
            $("#btnCadastro").text("Digite seu e-mail!");
        }
        else if(cadastro.email == null) {
            $("#btnCadastro").text("Digite seu e-mail!");
        }
        else if(cadastro.senha == null) {
            $("#btnCadastro").text("Digite sua senha!");
        }
        else if(cadastro.termos == null) {
            $("#btnCadastro").text("Selecione os termos!");
        }
        else {
            Api.Usuario.save({ nome:cadastro.nome, email:cadastro.email, senha:cadastro.senha, termos:cadastro.termos }, function (user) {
                if(user) {
                    $scope.dataUsuario = user;
                    $scope.tabAgendamento = 3;
                }
            }, function(err) {
                $("#btnCadastro").text(err.mensagem);
            });
        }        
    }

    $scope.definirPlano = function(plano) {
        $scope.tabAgendamento = 5;
    }
});

angular.module('guiadr').factory('ProfissionalPaging', function(Api) {
    var ProfissionalPaging = function() {
        this.items = [];
        this.busy = false;
        this.page = 1;
    };

    ProfissionalPaging.prototype.nextPage = function() {
        if (this.busy) return;
        this.busy = true;

        if(this.page <= 20) {
            Api.Profissional.get({page: this.page}, function (registros) {
                if(registros) {
                    if(registros.length == 0) {
                        this.page = 21;
                    }

                    var itens = registros;
                    for (var i = 0; i < itens.length; i++) {
                        this.items.push(itens[i]);
                    }
                    this.page += 1;
                }
                this.busy = false;
            }.bind(this));
        }
    };

    return ProfissionalPaging;
});