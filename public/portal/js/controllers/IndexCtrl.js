angular.module('guiadr').controller('IndexCtrl', function (Api, $scope, $routeParams, $timeout, DisponibilidadePaging, ClinicaPaging) {
    $scope.disponibilidades = [];
    $scope.categorias = [];
    $scope.postagens = [];
    $scope.tipos = [
        {id: 1, titulo: 'Consulta Simples'},
        {id: 3, titulo: 'Consulta Urgência'},
        {id: 4, titulo: 'Atendimento Domiciliar'},
        {id: 2, titulo: 'Retorno'}
    ];

    categorias();
    postagens();
    estados();
    
    function estados() {
        Api.EstadoClinica.query(function(registros) {
            if(registros) {
                console.log(registros);
                var regsEstados = [];
                var regsCidades = {};
                var regsBairros = [];
                var items = [];

                for (var i = 0; i < registros.length; i++) {
                    var registro = registros[i]._id;
                    //console.log('registro', registro.length);
                    
                    for (var e = 0; e < registro.length; e++) {
                        //for (var e = 0; e < registros[i]._id[e].cidade; e++) {
                            //if(!matchesByTitle(regsEstados, registro[e].titulo)) {
                                //regsCidades.push({titulo:registro[e].cidade.titulo});
                            //}
                        //}

                        //if(!matchesByTitle(regsEstados, registro[e].titulo)) {
                            regsCidades['cidade'] = registro[e].cidade.titulo;
                            //regsCidades['estado'][registro[e].titulo];
                            regsEstados.push(regsCidades);
                        //}
                        //regsEstados.push(registro[e].push({cidade:'asasd'}));

                        console.log('regsCidades', regsCidades);
                        console.log('regsEstados', regsEstados);
                        //regsEstados.push({id:registro[e].titulo, titulo:registro[e].titulo});
                        
                        //console.log('c', registro[e].cidade);
                        //for (var e = 0; e < registros[i]._id[e].cidade; e++) {
                            //regsCidades.push({id:registro[e].titulo, titulo:registro[e].titulo});
                            //console.log('e', registros[i]._id[e]);
                        //}
                    }
                }

                /*angular.forEach(registros, function(ve, ke) {
                    console.log('ve', ve._id);
                    if(isNaN(ve._id)) {
                        // Estados
                        this.push({id:ve._id[0], titulo:ve._id[0]});

                        // Cidades
                        if(isNaN(ve.cidade) && ve.cidade != null) {
                            angular.forEach(ve.cidade, function(c, kc) {
                                if(isNaN(c[0]) && c[0] != null) {
                                    if(!matches(this, c[0])) {
                                        this.push({id:c[0], titulo:c[0], estado:ve._id[0]});
                                    }
                                }
                            }, regsCidades);
                        }

                        // Bairros
                        if(isNaN(ve.bairro) && ve.bairro != null) {
                            angular.forEach(ve.bairro, function(b, kb) {
                                if(isNaN(b[0]) && b[0] != null) {
                                    if(!matches(this, b[0])) {
                                        this.push({id:b[0], titulo:b[0], cidade:ve.cidade[0]});
                                    }
                                }
                            }, regsBairros);
                        }
                    }
                }, regsEstados);
                $scope.estados = regsEstados;*/

                /*var regsCidades = [];
                angular.forEach(registros, function(vc, kc) {
                    if(isNaN(vc.cidade) && vc.cidade != null) {
                        angular.forEach(vc.cidade, function(c, ci) {
                            if(isNaN(c[0]) && c[0] != null) {
                                if(!matches(this, c[0])) {
                                    this.push({id:c[0], titulo:c[0], estado:vc._id[0]});
                                }
                            }
                        }, regsCidades);
                    }
                });
                $scope.cidades = regsCidades;

                var regsBairros = [];
                angular.forEach(registros, function(vb, kb) {
                    if(isNaN(vb.bairro) && vb.bairro != null) {
                        angular.forEach(vb.bairro, function(b, k) {
                            if(isNaN(b[0]) && b[0] != null) {
                                if(!matches(this, b[0])) {
                                    this.push({id:b[0], titulo:b[0], cidade:vb.cidade[0]});
                                }
                            }
                        }, regsBairros);
                    }
                });
                console.log(regsBairros);
                $scope.bairros = regsBairros;
                */
            }
        });
    }

    function matchesByTitle(list, phrase) {
        if(phrase == null) return false;
        for (var i = 0; i < list.length; i++) {
            var str = list[i].titulo.toLowerCase();
            var phr = phrase.toLowerCase();
            return (str === phr);
        }
    }

    function matches(list, phrase) {
        if(phrase == null) return false;
        for (var i = 0; i < list.length; i++) {
            var str = list[i].id.toLowerCase();
            var phr = phrase.toLowerCase();
            return (str === phr);
        }
    }
    
    function categorias() {
        Api.Categoria.query(function(registros) {
            if(registros) {
                var regs = [];

                angular.forEach(registros, function(v, k) {
                    this.push({value: v._id, label:v.titulo});
                }, regs);

                //$(document).find('select#categoria').ikSelect('addOptions', regs);
                $scope.categorias = registros;
            }
        });
    }

    function postagens() {
        var dados = {page: 0, limit: 3};
        Api.Blog.get(dados, function (registros) {
            $scope.postagens = registros;
        });
    }

    $scope.buscarConsulta = function(b) {
        if(b == null) {
            $("#btnConsulta").text("Escolhas as opções!");
        } else if(b.categoria == null) {
            $("#btnConsulta").text("Escolha uma categoria!");
        } else if(b.data == null) {
            $("#btnConsulta").text("Escolha uma data!");
        } else if(b.tipo == null) {
            $("#btnConsulta").text("Escolha um tipo!");
        } else {
            $("#btnConsulta").text("Buscando...");
            var categoriaId = (b.categoria != null ? b.categoria._id : 0);
            var especialidadeId = (b.especialidade != null ? b.especialidade._id : 0);
            var data = (b.data != null ? b.data : 0);
            var tipo = (b.tipo != null ? b.tipo : 0);

            var dados = {page: 1, categoria: categoriaId, especialidade: especialidadeId, data: data, tipo: tipo};
            Api.Disponibilidade.get(dados, function (registros) {
                if(registros) {
                    $scope.disponibilidades = registros;
                    if(registros.allItens <= 0){
                        $timeout(function(){ $("#btnConsulta").text("Nada encontrado!"); }, 3000);
                    } else {
                        $("#btnConsulta").text("Buscar");                        
                    }
                }
            });

            $scope.disponibilidades = new DisponibilidadePaging(dados);
        }

        $timeout(function(){ $("#btnConsulta").text("Buscar"); }, 2000);
    };

    $scope.buscarClinica = function(b) {
        if(b == null) {
            $("#btnClinica").text("Escolhas as opções!");
        //} else if(b.estado == null) {
            //$("#btnClinica").text("Escolha um Estado!");
        //} else if(b.cidade == null) {
            //$("#btnClinica").text("Escolha uma Cidade!");
        } else {
            $("#btnClinica").text("Buscando...");
            var dados = {
                page: 1, 
                estado: (b.estado != null ? b.estado._id : 0), 
                cidade: (b.cidade != null ? b.cidade._id : 0), 
                bairro: (b.bairro != null ? b.bairro._id : 0), 
                categoria: (b.categoria != null ? b.categoria._id : 0), 
                especialidade: (b.especialidade != null ? b.especialidade._id : 0)
            };
            Api.Clinica.get(dados, function (registros) {
                if(registros) {
                    $scope.clinicas = registros;
                    if(registros.allItens <= 0){
                        $timeout(function(){ $("#btnClinica").text("Nada encontrado!"); }, 3000);
                    } else {
                        $("#btnClinica").text("Buscar");                        
                    }
                }
            });

            $scope.clinicas = new ClinicaPaging(dados);
        }

        $timeout(function(){ $("#btnClinica").text("Buscar"); }, 2000);
    };
});

angular.module('guiadr').factory('DisponibilidadePaging', function(Api) {
    var DisponibilidadePaging = function(dados) {
        this.dados = dados; 
        this.items = [];
        this.busy = false;
    };

    DisponibilidadePaging.prototype.nextPage = function() {
        if (this.busy) return;
        this.busy = true;

        if(this.dados.page <= 20) {
            var dados = {page: this.dados.page, categoria: this.dados.categoria, especialidade: this.dados.especialidade, data: this.dados.data, tipo: this.dados.tipo};
            Api.Disponibilidade.get(dados, function (registros) {
                if(registros) {
                    var itens = registros;
                    for (var i = 0; i < itens.length; i++) {
                        if($(document).find("#p_" + itens[i]._id).length < 0)
                            this.items.push(itens[i]);
                    }
                    this.dados.page += 1;
                }
                this.busy = false;
                $("#btnConsulta").text("Buscar");
            }.bind(this));
        }
    };

    return DisponibilidadePaging;
});

angular.module('guiadr').factory('ClinicaPaging', function(Api) {
    var ClinicaPaging = function(dados) {
        this.dados = dados; 
        this.items = [];
        this.busy = false;
    };

    ClinicaPaging.prototype.nextPage = function() {
        if (this.busy) return;
        this.busy = true;

        if(this.dados.page <= 20) {
            var dados = {
                page: this.dados.page, 
                categoria: this.dados.categoria, 
                especialidade: this.dados.especialidade, 
                estado: this.dados.estado, 
                cidade: this.dados.cidade, 
                bairro: this.dados.bairro
            };
            Api.Clinica.get(dados, function (registros) {
                if(registros) {
                    var itens = registros;
                    for (var i = 0; i < itens.length; i++) {
                        if($(document).find("#c_" + itens[i]._id).length < 0)
                            this.items.push(itens[i]);
                    }
                    this.dados.page += 1;
                }
                this.busy = false;
                $("#btnClinica").text("Buscar");
            }.bind(this));
        }
    };

    return ClinicaPaging;
});