angular.module('guiadr').controller('ContatoCtrl', function (Api, $scope, $routeParams, $timeout) {
    $scope.registro = new Api.Contato();

    $scope.envia = function () {
        $scope.originalRegistro = angular.copy($scope.registro);
        $("#botaoEnviar").text("Aguarde! Estamos enviando...");
        $scope.registro.$save()
        .then(function (res) {
            $("#botaoEnviar").text(res.message);
            $timeout(function(){
                $("#botaoEnviar").text('Enviar');
            }, 2000);
        }, function() {
            $("#botaoEnviar").text("Por favor, tente mais tarde!").removeClass("btn-danger").addClass("btn-primary");
        });
    };
}); 