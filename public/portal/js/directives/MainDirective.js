(function () {
    var stickers = function () {
        return {
            restrict: 'E',
            templateUrl: 'portal/partials/directives/sticker.html',
            controller: function(Api, $scope, $element, $attrs) {
                var el = $element.find('select#especialidade');

                $scope.getEspecialidade = function() {
                    var id = $scope.busca.categoria._id;
                    Api.Especialidade.get({id:id}, function (registro) {
                        if(registro) {
                            $scope.especialidades = registro.especialidade;
                        }
                    });
                };

                $scope.getCidade = function() {
                    var titulo = $scope.buscaClinica.estado;
                    $scope.cyties = $scope.cidades.filter(function(obj) {
                        return obj.estado == titulo;
                    });
                };

                $scope.getBairro = function() {
                    var titulo = $scope.buscaClinica.cidade;
                    $scope.neighbor = $scope.cidades.filter(function(obj) {
                        return obj.cidade == titulo;
                    });
                };



                $scope.getClinicaEspecialidade = function() {
                    var id = $scope.buscaClinica.categoria._id;
                    Api.Especialidade.get({id:id}, function (registro) {
                        if(registro) {
                            $scope.especialidades = registro.especialidade;
                        }
                    });
                };

                $scope.getClinicaCidade = function() {
                    var titulo = $scope.buscaClinica.estado;
                    $scope.cyties = $scope.cidades.filter(function(obj) {
                        return obj.estado == titulo;
                    });
                };

                $scope.getClinicaBairro = function() {
                    var titulo = $scope.buscaClinica.cidade;
                    $scope.neighbor = $scope.cidades.filter(function(obj) {
                        return obj.cidade == titulo;
                    });
                };
            },
            link: function(scope, element, attrs) {
                $(".select2To").select2({width: '100%', minimumResultsForSearch: Infinity});
                element.find("#book_an_appointment_date").datepicker({ showOtherMonths: true});
                $(document).find("#ui-datepicker-div").addClass("ll-skin-melon");
            }
        };
    };

    var icheck = function ($timeout, $parse) {
        return {
            restrict: 'E',
            require: 'ngModel',
            link: function($scope, element, $attrs, ngModel) {
                return $timeout(function() {
                    var value = $attrs['value'];
                    $scope.$watch($attrs['ngModel'], function(newValue){
                        $(element).icheck('updated');
                    })

                    $scope.$watch($attrs['ngDisabled'], function(newValue) {
                        $(element).icheck(newValue ? 'disable':'enable');
                        $(element).icheck('updated');
                    })

                    return $(element).icheck({
                        checkboxClass: 'icheckbox_flat',
                        radioClass: 'iradio_flat'
                    });
                }, 300);
            },
            compile: function(element, $attrs) {
                var icheckOptions = {
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                };

                var modelAccessor = $parse($attrs['ngModel']);
                return function ($scope, element, $attrs, controller) {
                    var modelChanged = function(event) {
                        $scope.$apply(function() {
                            modelAccessor.assign($scope, event.target.checked);
                        });
                    };

                    $scope.$watch(modelAccessor, function (val) {
                        var action = val ? 'check' : 'uncheck';
                        element.iCheck(icheckOptions,action).on('ifChanged', modelChanged);
                    });
                };
            }
        };
    };

    var tagInput = function () {
        return {
            restrict: 'C',
            require:"ngModel",
            link : function (scope, element, attrs, ngModel) {
                scope.$watch(function() {
                    return ngModel.$modelValue;
                }, function(modelValue) {
                    if(modelValue)
                        element.tagsInput({ width:'100%', height:'auto' }).importTags(modelValue);
                    else
                        element.tagsInput({ width:'100%', height:'auto' });
                });
            }
        };
    };

    var wrapOwlcarousel = function () {
        return { 
            restrict: 'E', 
            templateUrl: 'portal/partials/directives/banner.html',
            link: function (scope, element, attrs) { 
                var options = scope.$eval($(element).attr('data-options')); 
                $(element).owlCarousel(options);
            } 
        };
    };

    var datepicker = function () {
        return {
            restrict: 'A',
            require : 'ngModel',
            link : function (scope, element, attrs, ngModelCtrl) {
                $(function(){
                    element.datepicker({
                        dateFormat:'dd/mm/yy',
                        language: 'pt-BR',
                        autoclose: true,
                        onSelect:function (date) {
                            scope.$apply(function () {
                                ngModelCtrl.$setViewValue(date);
                            });
                        }
                    });
                });
            }
        };
    };

    var dateFormat = function () {
        return {
            restrict: 'A',
            link: function(scope, el, at) {
                console.log(el);
                if(el.val() != "") {
                    var format = at.dateFormat;
                    scope.$watch(at.ngModel, function(date) {
                        var result = moment(date).format(format);
                        el.val(result);
                    });
                }
            }
        };
    };

    var appDatetime = function ($window) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                var moment = $window.moment;

                ngModel.$formatters.push(formatter);
                ngModel.$parsers.push(parser);

                element.on('change', function (e) {
                    var element = e.target;
                    element.value = formatter(ngModel.$modelValue);
                });

                function parser(value) {
                    var m = moment(value);
                    var valid = m.isValid();
                    ngModel.$setValidity('datetime', valid);
                    if (valid) return m.valueOf();
                    else return value;
                }

                function formatter(value) {
                    var m = moment(value);
                    var valid = m.isValid();
                    if (valid) return m.format("LLLL");
                    else return value;
                }
            }
        };
    };

    var starRating = function () {
        return {
            restrict : 'A',
            template : '<ul class="rating">'
                            + ' <li ng-repeat="star in stars ng-class="star" ng-click="toggle($index)">'
                            + '     <i class="fa fa-star-o"></i>'
                            + ' </li>'
                     + '</ul>',
            scope : {
                ratingValue : '=',
                max : '=',
                onRatingSelected : '&'
            },
            link : function(scope, elem, attrs) {
                var updateStars = function() {
                    scope.stars = [];
                    for (var i = 0; i < scope.max; i++) {
                        scope.stars.push({ filled : i < scope.ratingValue });
                    }
                };

                scope.toggle = function(index) {
                    scope.ratingValue = index + 1;
                    scope.onRatingSelected({
                        rating : index + 1
                    });
                };

                scope.$watch('ratingValue', function(oldVal, newVal) {
                    if (newVal) { updateStars(); }
                });
            }
        };
    };

    angular.module('guiadr')
        .directive('stickers', stickers)
        .directive('icheck', icheck)
        .directive('tagInput', tagInput)
        .directive('wrapOwlcarousel', wrapOwlcarousel)
        .directive('datepicker', datepicker)
        .directive('dateFormat', dateFormat)
        .directive('appDatetime', appDatetime)
        .directive('starRating', starRating);
}());