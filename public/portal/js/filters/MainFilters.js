(function () {
    var getMapsFromLatLong = function() {
        return function(value, nome) {
            if(value) {
                var map = '';
                var lt = value.toString().split("|");
                if(lt.length > 1) {
                    var map = new google.maps.Map(document.getElementById("map"), {
                      center: new google.maps.LatLng(lt[0], lt[1]),
                      zoom: 18,
                      mapTypeId: google.maps.MapTypeId.ROADMAP
                    });

                    new google.maps.Marker({
                        map: map,
                        position: new google.maps.LatLng(lt[0], lt[1]),
                        title: nome
                    });
                } else {
                    return '';
                }
            }
        }
    };

    var toShortDateBrazil = function () {
        return function(value) {
            /*
                LT : 'HH:mm',
                L : 'DD/MM/YYYY',
                LL : 'D [de] MMMM [de] YYYY',
                LLL : 'D [de] MMMM [de] YYYY [às] LT',
                LLLL : 'dddd, D [de] MMMM [de] YYYY [às] LT'
                https://gist.github.com/fernandosavio/680a2549e417befea930
            */
            return moment(value).format("L");
        };
    };

    var toDateStringBrasil = function () {
        return function(value) {
            /*
                LT : 'HH:mm',
                L : 'DD/MM/YYYY',
                LL : 'D [de] MMMM [de] YYYY',
                LLL : 'D [de] MMMM [de] YYYY [às] LT',
                LLLL : 'dddd, D [de] MMMM [de] YYYY [às] LT'
                https://gist.github.com/fernandosavio/680a2549e417befea930
            */
            return moment(value).format("LLL");
        };
    };

    var toDateTimeBrazil = function () {
        return function(value) {
            /*
                LT : 'HH:mm',
                L : 'DD/MM/YYYY',
                LL : 'D [de] MMMM [de] YYYY',
                LLL : 'D [de] MMMM [de] YYYY [às] LT',
                LLLL : 'dddd, D [de] MMMM [de] YYYY [às] LT'
                https://gist.github.com/fernandosavio/680a2549e417befea930
            */
            return moment(value).format("L [às] LT");
        };
    };

    var toConcatDataDisponibilidade = function () {
        return function(obj) {
            return moment(obj.data).format("L") + ' - ' + obj.categoria._id.titulo;
        }
    };

    var toStrFromObj = function () {
        return function(obj) {
            var habilidades = '';
            for (var i = 0; i < obj.length; i++) {
                habilidades += obj[i].titulo + ', ';
            }
            habilidades = habilidades.substring(0, (habilidades.length - 2))
            return habilidades;
        }
    };

    var toStrFromTipoAgenda = function () {
        return function(obj) {
            var str = '';
            
            if(obj == 1) { str = 'Consulta'; }
            if(obj == 2) { str = 'Retorno'; }

            return str;
        }
    };

    var toHoraCerta = function () {
        return function(obj) {
            var str = '';
            
            if(obj.sete) { str += ' 07:00 -'; }
            if(obj.setemeia) { str += ' 07:30 -'; }
            if(obj.oito) { str += ' 08:00 -'; }
            if(obj.oitomeia) { str += ' 08:30 -'; }
            if(obj.nove) { str += ' 09:00 -'; }
            if(obj.novemeia) { str += ' 09:30 -'; }
            if(obj.dez) { str += ' 10:00 -'; }
            if(obj.dezmeia) { str += ' 10:30 -'; }
            if(obj.onze) { str += ' 11:00 -'; }
            if(obj.onzemeia) { str += ' 11:30 -'; }
            if(obj.doze) { str += ' 12:00 -'; }
            if(obj.dozemeia) { str += ' 12:30 -'; }
            if(obj.treze) { str += ' 13:00 -'; }
            if(obj.trezemeia) { str += ' 13:30 -'; }
            if(obj.quatorze) { str += ' 14:00 -'; }
            if(obj.quatorzemeia) { str += ' 14:30 -'; }
            if(obj.quinze) { str += ' 15:00 -'; }
            if(obj.quinzemeia) { str += ' 15:30 -'; }
            if(obj.dezeseis) { str += ' 16:00 -'; }
            if(obj.dezeseismeia) { str += ' 16:30 -'; }
            if(obj.dezessete) { str += ' 17:00 -'; }
            if(obj.dezessetemeia) { str += ' 17:30 -'; }
            if(obj.dezoito) { str += ' 18:00 -'; }
            if(obj.dezoitomeia) { str += ' 18:30 -'; }
            if(obj.dezenove) { str += ' 19:00 -'; }
            if(obj.dezenovemeia) { str += ' 19:30 -'; }
            if(obj.vinte) { str += ' 20:00 -'; }
            if(obj.vintemeia) { str += ' 20:30 -'; }
            if(obj.vinteuma) { str += ' 21:00 -'; }
            if(obj.vinteumameia) { str += ' 21:30 -'; }
            if(obj.vinteduas) { str += ' 22:00 -'; }
            if(obj.vinteduasmeia) { str += ' 22:30 -'; }

            str = str.substring(1, str.length - 2);

            return str;
        }
    };

    var totalObj = function () {
        return function(obj) {
            return (obj != null ? parseInt(obj.length) : 0);
        }
    };

    var toBirthdayDateBrazil = function () {
        return function(birthday) {
            var now = moment(Date.now());
            var mid = moment(birthday);
            var days = mid.diff(now, 'year');

            return (birthday != null ? Math.abs(days) : 0);
        }
    };

    var toStatusConsulta = function () {
        return function(status) {
            var txRetorno;
            switch (status) {
                case 1: txRetorno = 'Agendada'; break;
                case 2: txRetorno = 'Em Consulta'; break;
                case 3: txRetorno = 'Efetuada'; break;
                case 4: txRetorno = 'Perdida'; break;
                case 5: txRetorno = 'Cancelada'; break;
                default: txRetorno = '';
            }
            return txRetorno;
        }
    };

    var haveStringOrValue = function () {
        return function(value) {
            return (value ? value : 'NÃO');
        };
    };

    var haveNumberOrZero = function () {
        return function(value) {
            return (value ? parseInt(value) : 0);
        };
    };

    var haveString = function () {
        return function(value) {
            return (value.lenght > 0 ? 'SIM' : 'NÃO');
        };
    };

    var toStrFromPrimeiraConsultaAgenda = function () {
        return function(obj) {
            var str = '';
            
            if(obj == 1) { str = 'Sim'; }
            if(obj == 2) { str = 'Não'; }

            return str;
        }
    };

    var simNao = function () {
        return function(value) {
            return (value ? 'SIM' : 'NÃO');
        }
    };

    angular.module('guiadr')
    .filter('getMapsFromLatLong', getMapsFromLatLong)
    .filter('toDateStringBrasil', toDateStringBrasil)
    .filter('toShortDateBrazil', toShortDateBrazil)
    .filter('toDateTimeBrazil', toDateTimeBrazil)
    .filter('toConcatDataDisponibilidade', toConcatDataDisponibilidade)
    .filter('toStrFromObj', toStrFromObj)
    .filter('toStrFromTipoAgenda', toStrFromTipoAgenda)
    .filter('toHoraCerta', toHoraCerta)
    .filter('totalObj', totalObj)
    .filter('toBirthdayDateBrazil', toBirthdayDateBrazil)
    .filter('toStatusConsulta', toStatusConsulta)
    .filter('haveStringOrValue', haveStringOrValue)
    .filter('haveNumberOrZero', haveNumberOrZero)
    .filter('toStrFromPrimeiraConsultaAgenda', toStrFromPrimeiraConsultaAgenda)
    .filter('haveString', haveString)
    .filter('simNao', simNao);
}());
