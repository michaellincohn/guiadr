angular.module('guiadr', ['ngRoute', 'ngResource', 'ngAnimate', 'ngSanitize', 'ui.bootstrap', 'angular-loading-bar', 'ngTagsInput', 'ngFileUpload', 'ngJcrop', 'infinite-scroll', 'ngScrollTo'])
.config(['$routeProvider', '$httpProvider', 'cfpLoadingBarProvider', 'ngJcropConfigProvider', 'ngScrollToOptionsProvider', 
    function($routeProvider, $httpProvider, cfpLoadingBarProvider, ngJcropConfigProvider, ngScrollToOptionsProvider) {

    $routeProvider.when('/erro/:registerId', { templateUrl: 'partials/single/error.html', controller: 'ErrorRestritoCtrl' });
    $routeProvider.when('/home', { templateUrl: 'partials/single/index.html', controller: 'IndexRestritoCtrl' });

    $routeProvider.when('/horario', { templateUrl: 'partials/horario/list.html', controller: 'HorarioRestritoCtrl' });
    $routeProvider.when('/horario/:registerId', { templateUrl: 'partials/horario/form.html', controller: 'HorarioRestritoCtrl' });

    $routeProvider.when('/disponibilidade', { templateUrl: 'partials/disponibilidade/list.html', controller: 'DisponibilidadeRestritoCtrl' });
    $routeProvider.when('/disponibilidade/:registerId', { templateUrl: 'partials/disponibilidade/form.html', controller: 'DisponibilidadeRestritoCtrl' });

    $routeProvider.when('/profissional', { templateUrl: 'partials/profissional/form.html', controller: 'ProfissionalRestritoCtrl' });
    $routeProvider.when('/profissionalCategoria', { templateUrl: 'partials/profissional/formCategoria.html', controller: 'ProfissionalCategoriaRestritoCtrl' });
    $routeProvider.when('/profissionalLocais', { templateUrl: 'partials/profissional/formLocais.html', controller: 'ProfissionalLocaisRestritoCtrl' });
    $routeProvider.when('/profissionalPerfil', { templateUrl: 'partials/profissional/formPerfil.html', controller: 'ProfissionalPerfilRestritoCtrl' });

    $routeProvider.when('/consulta', { templateUrl: 'partials/consulta/list.html', controller: 'ConsultaRestritoCtrl' });
    $routeProvider.when('/consulta/:registerId', { templateUrl: 'partials/consulta/form.html', controller: 'ConsultaRestritoCtrl' });
    
    $routeProvider.when('/fatura', { templateUrl: 'partials/fatura/list.html', controller: 'FaturaRestritoCtrl' });
    $routeProvider.when('/fatura/:registerId', { templateUrl: 'partials/fatura/details.html', controller: 'FaturaRestritoCtrl' });
    
    $routeProvider.when('/faq', { templateUrl: 'partials/ajuda/faq.html', controller: 'FaqRestritoCtrl' });
    $routeProvider.when('/faleconosco', { templateUrl: 'partials/ajuda/faleconosco.html', controller: 'FaleConoscoRestritoCtrl' });
    $routeProvider.when('/informativo', { templateUrl: 'partials/ajuda/informativo.html', controller: 'InformativoRestritoCtrl' });

    $routeProvider.otherwise({ redirectTo: '/home' });

    cfpLoadingBarProvider.includeSpinner = true;
    cfpLoadingBarProvider.includeBar = true;

    $httpProvider.interceptors.push('RestritoInterceptor');
}]);

angular.module('guiadr').config(function(ngJcropConfigProvider) {
    ngJcropConfigProvider.setJcropConfig('profissional', { bgColor:'black', bgOpacity:.3, aspectRatio:1 });
    ngJcropConfigProvider.setPreviewStyle('profissional', { 'width':'205px', 'height':'205px', 'overflow':'hidden', 'margin-left':'5px'});
});