(function () {
    var Api = function($resource) {
        return {
            Horario: $resource('/restrito/horario/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            Disponibilidade: $resource('/restrito/disponibilidade/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            Consulta: $resource('/restrito/consulta/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            Paciente: $resource('/restrito/paciente/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            Resize: $resource('/restrito/resize/:module', {module: '@module'}/*, { save: { ignoreLoadingBar: false } }*/),
            Upload: $resource('/restrito/upload/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            Categoria: $resource('/restrito/categoria', {query:{method:'GET', isArray:true}}),
            ProfissionalPerfil: $resource('/restrito/profissionalPerfil/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            ProfissionalLocais: $resource('/restrito/profissionalLocais/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            ProfissionalCategoria: $resource('/restrito/profissionalCategoria/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            Profissional: $resource('/restrito/profissional')
        }
    };

    var fileReader = function ($q) {
        var onLoad = function (reader, deferred, Sscope) {
            return function () {
                Sscope.$apply(function () {
                    deferred.resolve(reader.result);
                });
            };
        };

        var onError = function (reader, deferred, Sscope) {
            return function () {
                Sscope.$apply(function () {
                    deferred.reject(reader.result);
                });
            };
        };

        var onProgress = function (reader, Sscope) {
            return function (event) {
                Sscope.$broadcast(
                    "fileProgress", {
                        total: event.total, 
                        loaded: event.loaded
                    }
                );
            };
        };

        var getReader = function (deferred, Sscope) {
            var reader = new FileReader();
            reader.onload = onLoad(reader, deferred, Sscope);
            reader.onerror = onError(reader, deferred, Sscope);
            reader.onprogress = onProgress(reader, Sscope);
            return reader;
        };

        var readAsDataURL = function (file, Sscope) {
            var deferred = $q.defer();
            var reader = getReader(deferred, Sscope);
            reader.readAsDataURL(file);
            return deferred.promise;
        };

        return { readAsDataUrl: readAsDataURL };
    };

    var Auth = function($http, $q) {
        var r = $q.defer();
        $http.get('/restrito/auth/data').then(function(user) {
            r.resolve(user.data);
        });
        return r.promise;
    };

    var RestritoInterceptor = function($q, $window) {
        return {
            request: function (req) {
                req.headers = req.headers || {};
                return req;
            },
            responseError: function(rejection) {
                if (rejection.status == 401) {
                    $window.location = '/restrito/auth/logout';
                }
                return $q.reject(rejection);
            }
        }
    };

    angular.module('guiadr')
        .factory('Api', Api)
        .factory('fileReader', fileReader)
        .factory('Auth', Auth)
        .factory('RestritoInterceptor', RestritoInterceptor);
}());