(function () {
    var icheck = function ($timeout, $parse) {
        return {
            restrict: 'E',
            require: 'ngModel',
            link: function($scope, element, $attrs, ngModel) {
                return $timeout(function() {
                    var value = $attrs['value'];
                    $scope.$watch($attrs['ngModel'], function(newValue){
                        $(element).icheck('updated');
                    })

                    $scope.$watch($attrs['ngDisabled'], function(newValue) {
                        $(element).icheck(newValue ? 'disable':'enable');
                        $(element).icheck('updated');
                    })

                    return $(element).icheck({
                        checkboxClass: 'icheckbox_flat',
                        radioClass: 'iradio_flat'
                    });
                }, 300);
            },
            compile: function(element, $attrs) {
                var icheckOptions = {
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green'
                };

                var modelAccessor = $parse($attrs['ngModel']);
                return function ($scope, element, $attrs, controller) {
                    var modelChanged = function(event) {
                        $scope.$apply(function() {
                            modelAccessor.assign($scope, event.target.checked);
                        });
                    };

                    $scope.$watch(modelAccessor, function (val) {
                        var action = val ? 'check' : 'uncheck';
                        element.iCheck(icheckOptions,action).on('ifChanged', modelChanged);
                    });
                };
            }
        };
    };

    var datepicker = function () {
        return {
            restrict: 'A',
            require : 'ngModel',
            link : function (scope, element, attrs, ngModelCtrl) {
                $(function(){
                    element.datepicker({
                        dateFormat:'dd/mm/yy',
                        language: 'pt-BR',
                        autoclose: true,
                        onSelect:function (date) {
                            scope.$apply(function () {
                                ngModelCtrl.$setViewValue(date);
                            });
                        }
                    });
                });
            }
        };
    };

    var dateFormat = function () {
        return {
            restrict: 'A',
            link: function(scope, el, at) {
                console.log(el);
                if(el.val() != "") {
                    var format = at.dateFormat;
                    scope.$watch(at.ngModel, function(date) {
                        var result = moment(date).format(format);
                        el.val(result);
                    });
                }
            }
        };
    };

    var appDatetime = function ($window) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                var moment = $window.moment;

                ngModel.$formatters.push(formatter);
                ngModel.$parsers.push(parser);

                element.on('change', function (e) {
                    var element = e.target;
                    element.value = formatter(ngModel.$modelValue);
                });

                function parser(value) {
                    var m = moment(value);
                    var valid = m.isValid();
                    ngModel.$setValidity('datetime', valid);
                    if (valid) return m.valueOf();
                    else return value;
                }

                function formatter(value) {
                    var m = moment(value);
                    var valid = m.isValid();
                    if (valid) return m.format("LLLL");
                    else return value;
                }
            }
        };
    };

    angular.module('guiadr')
        .directive('icheck', icheck)
        .directive('datepicker', datepicker)
        .directive('dateFormat', dateFormat)
        .directive('appDatetime', appDatetime);
}());