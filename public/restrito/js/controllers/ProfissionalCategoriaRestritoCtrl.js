angular.module('guiadr').controller('ProfissionalCategoriaRestritoCtrl', function (Api, $scope, $routeParams, $location, $timeout) {
    $scope.espec = [];
    $scope.itens = [];
    $scope.registro = new Api.ProfissionalCategoria();
    listAllCategoria();
    listAll();

    function listAllCategoria() {
        Api.Categoria.get(function(registros) {
            $scope.categorias = registros.itens;
        });
    }

    function listAll() {
        Api.ProfissionalCategoria.get(
            function (registro) {
                $scope.registro = registro;
                $scope.itens = registro.categoria;
            },
            function (err) {
                bootbox.alert('Registro inexistente!', function () {
                    $location.path('profissional');
                });                
            }
        );
    }
    
    $scope.remove = function (registro) {
        console.log(registro);
        bootbox.confirm("Tem certeza que deseja excluir o registro?", function(answer) {
            if(answer == true) {
                Api.ProfissionalCategoria.delete({id: registro._id}, 
                    function(res) {                        
                        var index = -1;
                        var comArr = eval($scope.itens);
                        
                        for(var i = 0; i < comArr.length; i++) {
                            if(comArr[i]._id === registro._id) {
                                index = i;
                                break;
                            }
                        }
                        
                        if(index === -1) {
                            listAll();
                        }

                        $scope.itens.splice(index, 1);
                        $scope.totalItems--;
                    }, 
                    function(erro) {
                        bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
                    }
                );
            }
        });
    };
    
    $scope.salva = function (dados) {
        var especialidades = []
        for (var i = 0; i < $scope.espec.length; i++) {
            especialidades.push({
                "id": $scope.espec[i].id,
                "titulo": $scope.espec[i].titulo
            });
        }
        dados['especialidade'] = especialidades;
        console.log(especialidades);

        $scope.originalRegistro = angular.copy(dados);
        $("#botaoSalvar").text("Aguarde!");
        Api.ProfissionalCategoria.save(dados, function (res) {
            $("#botaoSalvar").text(res.mensagem).removeClass("btn-inverse").removeClass("btn-danger").addClass(res.class);
            listAll();
        }, function(err) {
            $("#botaoSalvar").text("Não salvo!");
        });
    };

    $scope.getEspecialidade = function(agendamento) { };

    $scope.change = function(c) {
        var index = -1;
        var especial = eval($scope.espec);
        if(especial == null) return false;
        for(var i = 0; i < especial.length; i++) {
            if(especial[i].id === c._id) {
                index = i;
                break;
            }
        }

        if(index === -1) {
            var espec = {};
            espec.id = c._id;
            espec.titulo = c.titulo;
            $scope.espec.push(espec);
        } else {
            $scope.espec.splice(index, 1);            
        }
    }

    var categoriaRemove = function (id) {
        var index = -1;
        var categorias = eval($scope.categorias);
        if(categorias == null) return false;
        for(var i = 0; i < categorias.length; i++) {
            if(categorias[i]._id === id) {
                index = i;
                break;
            }
        }
        if(index === -1) {
            listAllCategoria();
        }
        $scope.categorias.splice(index, 1);
    }

    var isInArray = function (array, search) {
        return array.indexOf(search) >= 0;
    }
});