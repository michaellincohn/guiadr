angular.module('guiadr').controller('ProfissionalRestritoCtrl', function (Api, $scope, $routeParams, $location, $timeout) {
    $scope.$watch('registro.info.descricao', function(text) {
        if(!text) {
            $scope.wordCountDescricao = 0 + " caracter";
        } 
        else {
            $scope.wordCountDescricao = text.length + " caracteres";
        }
    });
    $scope.$watch('registro.info.educacao', function(text) {
        if(!text) {
            $scope.wordCountEducacao = 0 + " caracter";
        } 
        else {
            $scope.wordCountEducacao = text.length + " caracteres";
        }
    });

    Api.Profissional.get(function (registro) {
        $scope.registro = registro;
        $scope.registro.password = '';
    }, function (erro) {
        console.log(erro);
    });
    
    $scope.salva = function () {
        $scope.originalRegistro = angular.copy($scope.registro);
        $("#botaoSalvar").text("Aguarde!");
        $scope.registro.$save().then(function (res) {
            $("#botaoSalvar").text(res.mensagem);
            $scope.registro = angular.copy($scope.originalRegistro);
            $scope.form.$setDirty();
        }, function() {
            bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
        });
    };
});