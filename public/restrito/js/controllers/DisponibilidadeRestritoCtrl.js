angular.module('guiadr').controller('DisponibilidadeRestritoCtrl', function (Api, Auth, $filter, $scope, $routeParams, $location, $timeout) {
    $scope.registros = [];
    $scope.filtro = '';
    $scope.nomePagina = '';
    $scope.currentPage = 1;
    $scope.totalItems = 0;
    $scope.pageSize = 10;
    $scope.maxSize = 10;
    $scope.tags = [];
    $scope.pageChanged = function() {
        listAll();
    };
    $scope.search = function (item) {
        if(item.length > 1) {
            $scope.busca = item;
        } else {
            $scope.busca = null;
        }
        listAll();
    };
    $scope.tipos = [
        {id: 1, titulo: 'Consulta'},
        {id: 2, titulo: 'Retorno'}
    ];

    if ($routeParams.registerId) {
        if ($routeParams.registerId == "add") {
            listProfissionalCategorias();
            $scope.registro = new Api.Disponibilidade();
            $scope.nomePagina = 'Cadastrar';
        } else {
            $scope.nomePagina = 'Editar';
            getById();
        }
    } else {
        listAll();
    }
    
    function listAll() {
        var dados = {page: $scope.currentPage};

        Api.Disponibilidade.get(dados,
            function (registros) {
                $scope.registros = registros.itens;
                $scope.totalItems = registros.allItens;
            },
            function (erro) {
                console.log(erro);
            }
        ); 
    }
    
    function getById() {
        listProfissionalCategorias();

        Api.Disponibilidade.get(
            { id: $routeParams.registerId },
            function (registro) {
                $scope.registro = registro;
                $scope.registro.data = $filter('toShortDateBrazil')(registro.data);
            }, 
            function (erro) {
                bootbox.alert('Registro inexistente!');
            }
        );
    }
    
    function listProfissionalCategorias() {
        Auth.then(function(user) {
            Api.ProfissionalCategoria.get(
                { id: user._id },
                function (registro) {
                    $scope.categorias = registro.categoria;
                }
            );
        });
    }

    $scope.loadTags = function(query) {
        Api.Horario.get({}, function (registro) {
            $scope.tags = registro.itens;
        });

        return $scope.tags;
    };
    
    $scope.remove = function (registro) {
        bootbox.confirm("Tem certeza que deseja excluir o registro?", function(answer) {
            if(answer == true) {
                Api.Disponibilidade.delete({id: registro._id}, 
                    function(res) {                        
                        var index = -1;
                        var comArr = eval($scope.registros);
                        
                        for(var i = 0; i < comArr.length; i++) {
                            if(comArr[i]._id === registro._id) {
                                index = i;
                                break;
                            }
                        }
                        
                        if(index === -1) {
                            listAll();
                        }

                        $scope.registros.splice(index, 1);
                        $scope.totalItems--;
                    }, 
                    function(erro) {
                        bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
                    }
                );
            }
        });
    };
    
    $scope.salva = function () {
        $scope.originalRegistro = angular.copy($scope.registro);
        $("#botaoSalvar").text("Aguarde!");
        $scope.registro.$save()
        .then(function (res) {
            $("#botaoSalvar").text(res.mensagem).removeClass("btn-inverse").removeClass("btn-danger").addClass(res.class);
            if(res.class == "btn-danger") {
                $scope.registro = angular.copy($scope.originalRegistro);
                $scope.form.$setDirty();
            } else {
                $timeout(function(){ $location.path('disponibilidade'); }, 1500);
            }
        }, function() {
            bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
        });
    };
});