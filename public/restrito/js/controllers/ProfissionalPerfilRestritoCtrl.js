angular.module('guiadr').controller('ProfissionalPerfilRestritoCtrl', function (Api, Auth, Upload, $scope, $routeParams, $location, $timeout) {
    $scope.obj = {}
    $scope.obj.coords = [10, 10, 110, 110, 100, 50];
    $scope.progressBar = '';
    $scope.haveImagem = 0;
    $scope.imagem = '';
    $scope.usuario = {};

    Api.Profissional.get(
        function (registro) {
            if(registro.imagem != null) {
                $scope.haveImagem = parseInt((registro.imagem == "" ? 0 : 1));
                $scope.imagem = '../portal/_ups/' + registro.imagem;
            }
            $scope.usuario = registro;
        }
    );

    $scope.upload = function (file, err) {
        Upload.upload({
            url: 'upload/profissional',
            data: { file: file, nameid: $scope.usuario._id },
        }).then(function (response) {
            $timeout(function () {
                $scope.obj.src = '../' + response.data.file;
                $scope.obj.thumbnail = true;
                $scope.progressBar = '50';
            });
        }, function (response) {
            if (response.status > 0) $scope.errorMsg = response.status + ': ' + response.data;
        }, function (evt) {
            $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
        });
    }

    $scope.resize = function (file, err) {
        var upload = new Api.Resize({module:'profissional'});
        upload.coords = file.coords;
        upload.file = file.src;
        upload.width = 205;
        upload.height = null;
        $scope.src = "";

        upload.$save().then(function (res) {
            var filePath = res.file.replace('./public/', '../');

            var profissional = new Api.Profissional();
            profissional._image = res.file.replace('./public/portal/_ups/', '');
            profissional._id = $scope.usuario._id;
            profissional.$save()
            .then(function (res) {
                $scope.src = filePath;
                $scope.haveImagem = 0;
            }, function() {
                bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
            });
        }, function(err) {
            bootbox.alert('Ocorreu um problema ao salvar! Tente novamente mais tarde.');
        });

        $scope.progressBar = '100';
    }
});