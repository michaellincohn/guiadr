angular.module('guiadr').controller('IndexRestritoCtrl', function (Api, $scope, $routeParams, $timeout) {
    $scope.disponibilidades = [];
    $scope.buscarConsulta = function(b) {
        if(b == null) {
            $("#btnConsulta").text("Escolhas as opções!");
        } else if(b.categoria == null) {
            $("#btnConsulta").text("Escolha uma categoria!");
        } else if(b.data == null) {
            $("#btnConsulta").text("Escolha uma data!");
        } else if(b.tipo == null) {
            $("#btnConsulta").text("Escolha um tipo!");
        } else {
            $("#btnConsulta").text("Buscando...");
            var categoriaId = (b.categoria != null ? b.categoria._id : 0);
            var especialidadeId = (b.especialidade != null ? b.especialidade._id : 0);
            var data = (b.data != null ? b.data : 0);
            var tipo = (b.tipo != null ? b.tipo : 0);

            var dados = {page: 1, categoria: categoriaId, especialidade: especialidadeId, data: data, tipo: tipo};
            Api.Disponibilidade.get(dados, function (registros) {
                if(registros) {
                    $scope.disponibilidades = registros;
                    if(registros.allItens <= 0){
                        $timeout(function(){ $("#btnConsulta").text("Nada encontrado!"); }, 3000);
                    } else {
                        $("#btnConsulta").text("Buscar");                        
                    }
                }
            });
        }

        $timeout(function(){ $("#btnConsulta").text("Buscar"); }, 2000);
    };
});