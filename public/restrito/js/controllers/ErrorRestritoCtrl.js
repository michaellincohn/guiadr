angular.module('guiadr').controller('ErrorRestritoCtrl', function ($scope, $routeParams) {
	if($routeParams.registerId == '404') {
		$scope.titulo = 'Página não encontrada!';
		$scope.numero = '404';
		$scope.descricao = 'A página que está procurando, não está disponível ou não existe.';
	} else if($routeParams.registerId == '500') {
		$scope.titulo = 'Um erro ocorreu!';
		$scope.numero = '500';
		$scope.descricao = 'A página que está tentando acessar, não está disponível ou não existe.';
	}	
});