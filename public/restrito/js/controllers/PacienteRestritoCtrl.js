angular.module('guiadr').controller('PacienteRestritoCtrl', function (Api, $scope, $routeParams, $location, $timeout) {
    $scope.registros = [];
    $scope.filtro = '';
    $scope.nomePagina = 'Lista';
    $scope.currentPage = 1;
    $scope.pageSize = 10;
    $scope.maxSize = 10;
    $scope.pageChanged = function() {
        listAll();
    };
    $scope.search = function (item) {
        if(item.length > 1) {
            $scope.busca = item;
        } else {
            $scope.busca = null;
        }
        listAll();
    };

    listAll();
    
    function listAll() {
        var dados = {page: $scope.currentPage};

        if($scope.busca) {
            var busca = [];
            var itembusca = [];
            itembusca.push({ key:'nome', value:$scope.busca, type:'like' });
            busca.push(itembusca);
            dados.busca = busca;
        }

        Api.Paciente.get(dados,
            function (registros) {
                $scope.registros = registros.itens;
                $scope.totalItems = registros.allItens;
            },
            function (erro) {
                console.log(erro);
            }
        ); 
    }
    
    function getById() {
        Api.Paciente.get(
            { id: $routeParams.registerId },
            function (registro) {
                $scope.registro = registro;
                $scope.registro.password = '';
            }, 
            function (erro) {
                bootbox.alert('Registro inexistente!', function () {
                    $location.path('paciente');
                });
            }
        );
    }
    
    $scope.remove = function (registro) {
        bootbox.confirm("Tem certeza que deseja excluir o registro?", function(answer) {
            if(answer == true) {
                Api.Paciente.delete({id: registro._id}, 
                    function(res) {                        
                        var index = -1;
                        var comArr = eval($scope.registros);
                        
                        for(var i = 0; i < comArr.length; i++) {
                            if(comArr[i]._id === registro._id) {
                                index = i;
                                break;
                            }
                        }
                        
                        if(index === -1) {
                            listAll();
                        }

                        $scope.registros.splice(index, 1);
                        $scope.totalItems--;
                    }, 
                    function(erro) {
                        bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
                    }
                );
            }
        });
    };

    $scope.ver = function (registro) {
        bootbox.dialog({
                title: "Detalhes do paciente",
                message: '<div class="row">  ' +
                    '<div class="col-md-12"> ' +
                        '<div class="form-horizontal"> ' +
                            '<div class="form-group"> ' +
                                '<label class="col-md-4 control-label">Nome: </label> ' +
                                '<div class="col-md-4"> ' + registro.nome + ' </div> ' +
                            '</div> ' +
                            '<div class="form-group"> ' +
                                '<label class="col-md-4 control-label">E-mail: </label> ' +
                                '<div class="col-md-4"> ' + registro.email + ' </div> ' +
                            '</div> ' +
                            '<div class="form-group"> ' +
                                '<label class="col-md-4 control-label">CPF/CNPJ: </label> ' +
                                '<div class="col-md-4"> ' + registro.documento + ' </div> ' +
                            '</div> ' +
                            '<div class="form-group"> ' +
                                '<label class="col-md-4 control-label">Telefone: </label> ' +
                                '<div class="col-md-4"> ' + registro.endereco[0].telefone[0].numero + ' </div> ' +
                            '</div> ' +
                            '<div class="form-group"> ' +
                                '<label class="col-md-4 control-label">Cidade: </label> ' +
                                '<div class="col-md-4"> ' + registro.endereco[0].cidade + ' </div> ' +
                            '</div> ' +
                            '<div class="form-group"> ' +
                                '<label class="col-md-4 control-label">Estado: </label> ' +
                                '<div class="col-md-4"> ' + registro.endereco[0].estado + ' </div> ' +
                            '</div> ' +
                        '</div> ' +
                    '</div> ' +
                '</div>',
                buttons: {
                    success: {
                        label: "Fechar",
                        className: "btn-info"
                    }
                }
            }
        );
    };
});