angular.module('guiadr').controller('ContatoPainelCtrl', function (Api, $scope, $routeParams, $location, $timeout, $filter) {
    $scope.registros = [];
    $scope.filtro = '';
    $scope.nomePagina = 'Lista';
    $scope.currentPage = 1;
    $scope.pageSize = 10;
    $scope.maxSize = 10;
    $scope.pageChanged = function() {
        listAll();
    };
    $scope.search = function (item) {
        if(item.length > 1) {
            $scope.busca = item;
        } else {
            $scope.busca = null;
        }
        listAll();
    };

    if ($routeParams.registerId) {
        $scope.nomePagina = 'Visualização';
        getById();
    } else {
        listAll();
    }

    function listAll() {
        var dados = {page: $scope.currentPage};

        if($scope.busca) {
            var busca = [];
            var itembusca = [];
            itembusca.push({ key:'nome', value:$scope.busca, type:'like' });
            busca.push(itembusca);
            dados.busca = busca;
        }
        
        Api.Contato.get(dados,
            function (registros) {
                $scope.registros = registros.itens;
                $scope.totalItems = registros.allItens;
            },
            function (erro) {
                console.log(erro);
            }
        ); 
    }

    function getById() {
        Api.Contato.get(
            { id: $routeParams.registerId },
            function (registro) {
                $scope.registro = registro;
            }, 
            function (erro) {
                bootbox.alert('Registro inexistente!');
            }
        );
    }

    $scope.ver = function (registro) {
        bootbox.dialog({
                title: "Detalhes do contato",
                message: registro.mensagem + "<br /><br />Enviado por: <div class='label label-primary'>" + registro.nome + "<" + registro.email + "></div>" +
                        "<br />Data: <div class='label label-primary'>" + $filter('toDateTimeBrazil')(registro.dtInclusao) + "</div>",
                buttons: {
                    success: {
                        label: "Fechar",
                        className: "btn-info"
                    }
                }
            }
        );
    };

    $scope.remove = function (registro) {
        bootbox.confirm("Tem certeza que deseja excluir o registro?", function(answer) {
            if(answer == true) {
                Api.Contato.delete({id: registro._id}, 
                    function(res) {
                        var index = -1;
                        var comArr = eval($scope.registros);
                        
                        for(var i = 0; i < comArr.length; i++) {
                            if(comArr[i]._id === registro._id) {
                                index = i;
                                break;
                            }
                        }
                        
                        if(index === -1) {
                            listAll();
                        }

                        $scope.registros.splice(index, 1);
                        $scope.totalItems--;
                    }, 
                    function(erro) {
                        bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
                    }
                );
            }
        });
    };
});