angular.module('guiadr').controller('ProfissionalLocaisPainelCtrl', function (Api, $scope, $http, $routeParams, $location, $timeout) {
    $scope.itens = [];
    $scope.nomePagina = 'Locais';
    $scope.registro = new Api.ProfissionalLocais();
    listAll();
    listAllPais();
    var dados = {limit: 1000};
    var busca;
    var itembusca;

    function listAllPais() {
        Api.Pais.get({limit:1000}, function(registros) {
            $scope.paises = registros.itens;
        });
    }

    function listAll() {
        Api.ProfissionalLocais.get(
            { id: $routeParams.registerId },
            function (registro) {
                $scope.registro = registro;
                console.log(registro);
                $scope.itens = registro.endereco;
            },
            function (erro) {
                bootbox.alert('Registro inexistente!', function () {
                    $location.path('profissional');
                });
            }
        );
    }

    function getEstado(registro) {
        Api.Estado.get({busca:[{key:'pais', value:registro.pais, type:'normal'}], limit:1000}, function(registros) {
            $scope.estados = registros.itens;
        });
    }

    function getCidade(registro) {
        Api.Cidade.get({busca:[{key:'estado', value:registro.estado, type:'normal'}], limit:1000}, function(registros) {
            $scope.cidades = registros.itens;
        });
    }

    function getBairro(registro) {
        Api.Bairro.get({busca:[{key:'cidade', value:registro.cidade, type:'normal'}], limit:1000}, function(registros) {
            $scope.bairros = registros.itens;
        });
    }

    $scope.changePais = function (registro) {
        getEstado(registro);
    };

    $scope.changeEstado = function (registro) {
        getCidade(registro);
    };

    $scope.changeCidade = function (registro) {
        getBairro(registro);
    };

    $scope.buscaCep = function () {
        var registro = $scope.registro.logradouro;
        if(registro != null && !isNaN(registro.cep) && registro.cep != '' && registro.cep.length == 8) {
            Api.Logradouro.get({busca:[{key:'cep', value:registro.cep, type:'normal'}], limit:1}, function(endereco) {
                if(endereco.allItens > 0) {
                    var item = endereco.itens[0];
                    $('#btnBuscarCep').text('Encontrado!').removeClass('btn-inverse').removeClass('btn-danger').addClass('btn-success');
                    listAllPais();
                    $scope.registro.endereco.pais = item.pais._id;
                    getEstado(item);
                    $scope.registro.endereco.estado = item.estado._id;
                    getCidade(item);
                    $scope.registro.endereco.cidade = item.cidade._id;
                    getBairro(item);
                    $scope.registro.endereco.bairro = item.bairro._id;
                    $scope.logradouro = item.titulo;
                    $scope.registro.endereco.logradouro = item._id;
                } else {
                    $http.get('http://api.postmon.com.br/cep/' + registro.cep).success(function(endereco) {
                        if(endereco != null) {
                            $('#btnBuscarCep').text('Encontrado').removeClass('btn-inverse').removeClass('btn-danger').addClass('btn-success');
                            $scope.logradouro = endereco.logradouro;

                            var estado = new Api.Estado();
                            estado.pais = '56fb0923fba10d0417ebc3b8'; // Brasil
                            estado.uf = endereco.estado;
                            estado.titulo = endereco.estado_info.nome;
                            estado.$save().then(function(regEstado) {
                                listAllPais();
                                getEstado(regEstado.reg);
                                $scope.registro.endereco.pais = regEstado.reg.pais;
                                $scope.registro.endereco.estado = regEstado.reg._id;

                                var cidade = new Api.Cidade();
                                cidade.pais = regEstado.reg.pais;
                                cidade.estado = regEstado.reg._id;
                                cidade.titulo = endereco.cidade;
                                cidade.$save().then(function(regCidade) {
                                    getCidade(regCidade.reg);
                                    $scope.registro.endereco.cidade = regCidade.reg._id;

                                    var bairro = new Api.Bairro();
                                    bairro.pais = regCidade.reg.pais;
                                    bairro.estado = regCidade.reg.estado;
                                    bairro.cidade = regCidade.reg._id;
                                    bairro.titulo = endereco.bairro;
                                    bairro.$save().then(function(regBairro) {
                                        getBairro(regBairro.reg);
                                        $scope.registro.endereco.bairro = regBairro.reg._id;

                                        var logradouro = new Api.Logradouro();
                                        logradouro.pais = regBairro.reg.pais;
                                        logradouro.estado = regBairro.reg.estado;
                                        logradouro.cidade = regBairro.reg.cidade;
                                        logradouro.bairro = regBairro.reg._id;
                                        logradouro.cep = endereco.cep;
                                        logradouro.tipo = '';
                                        logradouro.titulo = endereco.logradouro;
                                        logradouro.$save().then(function(regLogradouro) {
                                            $scope.registro.endereco.logradouro = regLogradouro.reg._id;
                                        });
                                    });
                                });
                            });
                        } else {
                            $('#btnBuscarCep').text('Não encontrado').removeClass('btn-inverse').removeClass('btn-success').addClass('btn-danger');
                        }
                    }).error(function(err) {
                        console.log(err);
                        $('#btnBuscarCep').text('Não encontrado').removeClass('btn-inverse').removeClass('btn-success').addClass('btn-danger');
                    });
                }
            });
        } else {
           $('#btnBuscarCep').text('Digite o CEP').removeClass('btn-inverse').removeClass('btn-success').addClass('btn-danger'); 
        }
    };

    /*$scope.getLocation = function (val) {
        return $http.get('https://maps.googleapis.com/maps/api/geocode/json', {
            params: {
                address: val,
                sensor: false,
                components: 'country:Brasil',
                key: 'AIzaSyDZJPxciXBxK3kkGyQePKy9Awk4ChKrNgQ'
            }
        }).then(function(response) {
            return response.data.results.map(function(item) {
                return item;
            });
        }, function(err) {
            console.log(err);
        });
    };

    $scope.onSelect = function($item, $model, $label) {
        if($item.geometry.location != null) {
            $scope.registro.endereco.latlong = $item.geometry.location.lat + '|' + $item.geometry.location.lng;
        }

        angular.forEach($item.address_components, function(value, key) {
            if(isInArray(value.types,'route')) {
                $scope.registro.endereco.logradouro = value.long_name;
            } else if(isInArray(value.types,'sublocality_level_1') || isInArray(value.types,'sublocality')) {
                $scope.registro.endereco.bairro = value.long_name;
            } else if(isInArray(value.types,'locality')) {
                $scope.registro.endereco.cidade = value.long_name;
            } else if(isInArray(value.types,'administrative_area_level_1')) {
                $scope.registro.endereco.estado = value.long_name;
            } else if(isInArray(value.types,'country')) {
                $scope.registro.endereco.pais = value.long_name;
            }
        });
    };*/
    
    $scope.remove = function (registro) {
        bootbox.confirm("Tem certeza que deseja excluir o registro?", function(answer) {
            if(answer == true) {
                Api.ProfissionalLocais.delete({id: registro._id}, 
                    function(res) {                        
                        var index = -1;
                        var comArr = eval($scope.itens);
                        
                        for(var i = 0; i < comArr.length; i++) {
                            if(comArr[i]._id === registro._id) {
                                index = i;
                                break;
                            }
                        }
                        
                        if(index === -1) {
                            listAll();
                        }

                        $scope.itens.splice(index, 1);
                        $scope.totalItems--;
                    }, 
                    function(erro) {
                        bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
                    }
                );
            }
        });
    };
    
    $scope.salva = function (reg) {
        $("#botaoSalvar").text("Aguarde!");

        var endereco = new Api.ProfissionalLocais();
        endereco._id = $routeParams.registerId;
        endereco.numero = reg.endereco.numero;
        endereco.logradouro = reg.endereco.logradouro;
        endereco.bairro = reg.endereco.bairro;
        endereco.cidade = reg.endereco.cidade;
        endereco.estado = reg.endereco.estado;
        endereco.pais = reg.endereco.pais;
        endereco.telefone = reg.endereco.telefone;
        endereco.latlong = reg.endereco.latlong;
        endereco.$save()
        .then(function (res) {
            $("#botaoSalvar").text(res.mensagem).removeClass("btn-inverse").removeClass("btn-danger").addClass(res.class);
            listAll();
        }, function() {
            bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
        });
    };
    
    $scope.avanca = function () {
        $("#botaoAvanca").text("Aguarde!");
        $location.path('profissionalPerfil/' + $routeParams.registerId);
    };

    $scope.volta = function () {
        $("#botaoVolta").text("Aguarde!");
        $location.path('profissionalCategoria/' + $routeParams.registerId);
    };

    var isInArray = function (array, search) {
        return array.indexOf(search) >= 0;
    }
});