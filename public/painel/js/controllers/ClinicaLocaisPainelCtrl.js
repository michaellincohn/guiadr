angular.module('guiadr').controller('ClinicaLocaisPainelCtrl', function (Api, $scope, $http, $routeParams, $location, $timeout) {
    $scope.itens = [];
    $scope.nomePagina = 'Locais';
    $scope.registro = new Api.ClinicaLocais();
    listAll();

    function listAll() {
        Api.ClinicaLocais.get(
            { id: $routeParams.registerId },
            function (registro) {
                $scope.registro = registro;
                $scope.itens = registro.endereco;
            },
            function (erro) {
                bootbox.alert('Registro inexistente!', function () {
                    $location.path('clinica');
                });
            }
        );
    }

    $scope.getLocation = function (val) {
        return $http.get('https://maps.googleapis.com/maps/api/geocode/json', {
            params: {
                address: val,
                sensor: false,
                key: 'AIzaSyDZJPxciXBxK3kkGyQePKy9Awk4ChKrNgQ'
            }
        }).then(function(response) {
            return response.data.results.map(function(item) {
                return item;
            });
        }, function(err) {
            console.log(err);
        });
    };

    $scope.onSelect = function($item, $model, $label) {
        if($item.geometry.location != null) {
            $scope.registro.endereco.latlong = $item.geometry.location.lat + '|' + $item.geometry.location.lng;
        }

        angular.forEach($item.address_components, function(value, key) {
            if(isInArray(value.types,'route')) {
                $scope.registro.endereco.logradouro = value.long_name;
            } else if(isInArray(value.types,'sublocality_level_1') || isInArray(value.types,'sublocality')) {
                $scope.registro.endereco.bairro = value.long_name;
            } else if(isInArray(value.types,'locality')) {
                $scope.registro.endereco.cidade = value.long_name;
            } else if(isInArray(value.types,'administrative_area_level_1')) {
                $scope.registro.endereco.estado = value.long_name;
            } else if(isInArray(value.types,'country')) {
                $scope.registro.endereco.pais = value.long_name;
            }
        });
    }
    
    $scope.remove = function (registro) {
        bootbox.confirm("Tem certeza que deseja excluir o registro?", function(answer) {
            if(answer == true) {
                Api.ClinicaLocais.delete({id: registro._id}, 
                    function(res) {                        
                        var index = -1;
                        var comArr = eval($scope.itens);
                        
                        for(var i = 0; i < comArr.length; i++) {
                            if(comArr[i]._id === registro._id) {
                                index = i;
                                break;
                            }
                        }
                        
                        if(index === -1) {
                            listAll();
                        }

                        $scope.itens.splice(index, 1);
                        $scope.totalItems--;
                    }, 
                    function(erro) {
                        bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
                    }
                );
            }
        });
    };
    
    $scope.salva = function (reg) {
        $("#botaoSalvar").text("Aguarde!");

        var endereco = new Api.ClinicaLocais();
        endereco._id = $routeParams.registerId;
        endereco.numero = reg.endereco.numero;
        endereco.logradouro = reg.endereco.logradouro;
        endereco.bairro = reg.endereco.bairro;
        endereco.cidade = reg.endereco.cidade;
        endereco.pais = reg.endereco.pais;
        endereco.telefone = reg.endereco.telefone;
        endereco.latlong = reg.endereco.latlong;
        endereco.$save()
        .then(function (res) {
            $("#botaoSalvar").text(res.mensagem).removeClass("btn-inverse").removeClass("btn-danger").addClass(res.class);
            listAll();
        }, function() {
            bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
        });
    };
    
    $scope.avanca = function () {
        $("#botaoAvanca").text("Aguarde!");
        $location.path('clinicaPerfil/' + $routeParams.registerId);
    };

    $scope.volta = function () {
        $("#botaoVolta").text("Aguarde!");
        $location.path('clinicaProfissional/' + $routeParams.registerId);
    };

    var isInArray = function (array, search) {
        return array.indexOf(search) >= 0;
    }
});
// R. Primeiro de Maio - Boa Esperança, Parnamirim - RN, Brasil