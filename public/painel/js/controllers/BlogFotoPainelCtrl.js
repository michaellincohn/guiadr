angular.module('guiadr').controller('BlogFotoPainelCtrl', function (Api, Upload, $scope, $routeParams, $location, $timeout) {
    $scope.obj = {}
    $scope.obj.coords = [10, 10, 650, 230, 100, 50];
    $scope.progressBar = '';
    $scope.nomePagina = 'Foto';
    $scope.haveImagem = 0;
    $scope.imagem = '';
    var imagem = '';
    getById();

    function getById() {
        Api.Blog.get(
            { id: $routeParams.registerId },
            function (registro) {
                $scope.haveImagem = parseInt((registro.imagem == "" ? 0 : 1));

                if(registro.imagem == null || registro.imagem == "") {
                    imagem = '../vendor/painel/img/no-user-image-square.jpg';
                } else {
                    imagem = '../portal/_ups/' + registro.imagem;
                }

                $scope.imagem = imagem;
            }, 
            function (erro) {
                console.log(erro);
                bootbox.alert('Registro inexistente!', function () {
                    $location.path('blog');
                });
            }
        );
    }

    $scope.upload = function (file, err) {
        Upload.upload({
            url: 'upload/blog',
            data: { file: file, nameid: $routeParams.registerId },
        }).then(function (response) {
            $timeout(function () {
                $scope.obj.src = '../' + response.data.file;
                $scope.obj.thumbnail = true;
                $scope.progressBar = '50';
            });
        }, function (response) {
            if (response.status > 0) $scope.errorMsg = response.status + ': ' + response.data;
        }, function (evt) {
            $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
        });
    }

    $scope.resize = function (file, err) {
        var upload = new Api.Resize({module:'blog'});
        upload.coords = file.coords;
        upload.file = file.src;
        upload.width = 650;
        upload.height = 230;
        $scope.src = "";

        upload.$save()
        .then(function (res) {
            if(res.file == null)
                return bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');

            var filePath = res.file.replace('./public/', '../');

            var blogFoto = new Api.Blog();
            blogFoto._image = res.file.replace('./public/portal/_ups/', '');
            blogFoto._id = $routeParams.registerId;
            blogFoto.$save()
            .then(function (res) {
                $scope.src = filePath;
                $scope.haveImagem = 0;
            }, function() {
                bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
            });
        }, function(err) {
            bootbox.alert('Ocorreu um problema ao salvar! Tente novamente mais tarde.');
        });

        $scope.progressBar = '100';
    }

    $scope.finaliza = function () {
        $("#botaoFinaliza").text("Aguarde!");
        $location.path('blog');
    };

    $scope.volta = function () {
        $("#botaoVolta").text("Aguarde!");
        $location.path('blog/' + $routeParams.registerId);
    };
});