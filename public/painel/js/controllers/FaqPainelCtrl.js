angular.module('guiadr').controller('FaqPainelCtrl', function (Api, Auth, $scope, $routeParams, $location, $timeout) {
    $scope.registros = [];
    $scope.filtro = '';
    $scope.nomePagina = 'Lista';
    $scope.currentPage = 1;
    $scope.pageSize = 10;
    $scope.maxSize = 10;
    $scope.userTipo;
    $scope.pageChanged = function() {
        listAll();
    };
    $scope.search = function (item) {
        if(item.length > 1) {
            $scope.busca = item;
        } else {
            $scope.busca = null;
        }
        listAll();
    };
    $scope.editorOptions = {
        language: 'pt',
        //uiColor: '#009688'
    };
    $scope.$on("ckeditor.ready", function( event ) {
        $scope.isReady = true;
    });

    Auth.then(function(user) {
        $scope.userTipo = user.tipo;
    });

    if ($routeParams.registerId) {
        if ($routeParams.registerId == "add") {
            $scope.nomePagina = 'Cadastro';
            $scope.registro = new Api.Faq();
        } else {
            $scope.nomePagina = 'Edição';
            getById();
        }
    } else {
        listAll();
    }
    
    function listAll() {
        var dados = {page: $scope.currentPage};

        if($scope.busca) {
            var busca = [];
            var itembusca = [];
            itembusca.push({ key:'titulo', value:$scope.busca, type:'like' });
            busca.push(itembusca);
            dados.busca = busca;
        }

        Api.Faq.get(dados,
            function (registros) {
                $scope.registros = registros.itens;
                $scope.totalItems = registros.allItens;
            },
            function (erro) {
                console.log(erro);
            }
        ); 
    }
    
    function getById() {
        Api.Faq.get(
            { id: $routeParams.registerId },
            function (registro) {
                $scope.registro = registro;
            }, 
            function (erro) {
                bootbox.alert('Registro inexistente!', function () {
                    $location.path('faq');
                });
            }
        );
    }
    
    $scope.remove = function (registro) {
        bootbox.confirm("Tem certeza que deseja excluir o registro?", function(answer) {
            if(answer == true) {
                Api.Faq.delete({id: registro._id}, 
                    function(res) {                        
                        var index = -1;
                        var comArr = eval($scope.registros);
                        
                        for(var i = 0; i < comArr.length; i++) {
                            if(comArr[i]._id === registro._id) {
                                index = i;
                                break;
                            }
                        }
                        
                        if(index === -1) {
                            listAll();
                        }

                        $scope.registros.splice(index, 1);
                        $scope.totalItems--;
                    }, 
                    function(erro) {
                        bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
                    }
                );
            }
        });
    };
    
    $scope.salva = function () {
        $scope.originalRegistro = angular.copy($scope.registro);
        $("#botaoSalvar").text("Aguarde!");
        $scope.registro.$save()
        .then(function (res) {
            $("#botaoSalvar").text(res.mensagem).removeClass("btn-inverse").removeClass("btn-danger").addClass(res.class);
            if(res.class == "btn-danger") {
                $scope.registro = angular.copy($scope.originalRegistro);
                $scope.form.$setDirty();
            } else {
                $timeout(function(){ $location.path('faq'); }, 1500);
            }
        }, function() {
            bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
        });
    };
});