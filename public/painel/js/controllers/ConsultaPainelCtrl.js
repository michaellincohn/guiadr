angular.module('guiadr').controller('ConsultaPainelCtrl', function (Api, $scope, $http, $routeParams, $location, $timeout) {
    $scope.registros = [];
    $scope.nomePaciente = '';
    $scope.currentPage = 1;
    $scope.pageSize = 10;
    $scope.maxSize = 10;
    $scope.pageChanged = function() {
        listAll();
    };

    listAll();
    
    function listAll() {
        Api.Paciente.get({ id: $routeParams.registerId }, function (registro) {
            $scope.nomePaciente = registro.nome;
        });

        var dados = {page: $scope.currentPage, paciente: $routeParams.registerId };

        Api.Consulta.get(dados,
            function (registros) {
                $scope.registros = registros.itens;
                $scope.totalItems = registros.allItens;
            },
            function (erro) {
                console.log(erro);
            }
        ); 
    }
    
    $scope.status = function (event, registro) {
        Api.Consulta.save(
            { data: registro }, 
            function(res) {
                registro.status = res.status;
            }, 
            function (erro) {
                bootbox.alert('Ocorreu um erro no servidor que impossibilita esta alteração!');
            }
        );
    };
});