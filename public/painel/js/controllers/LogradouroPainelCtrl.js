angular.module('guiadr').controller('LogradouroPainelCtrl', function (Api, $scope, $http, $routeParams, $location, $timeout) {
    $scope.registros = [];
    $scope.filtro = '';
    $scope.nomePagina = 'Lista';
    $scope.currentPage = 1;
    $scope.pageSize = 10;
    $scope.maxSize = 10;
    $scope.pageChanged = function() {
        listAll();
    };
    $scope.search = function (item) {
        if(item.length > 1) {
            $scope.busca = item;
        } else {
            $scope.busca = null;
        }
        listAll();
    };

    if ($routeParams.registerId) {
        if ($routeParams.registerId == "add") {
            $scope.nomePagina = 'Cadastro';
            $scope.registro = new Api.Logradouro();
        } else {
            $scope.nomePagina = 'Edição';
            getById();
        }
    } else {
        listAll();
    }

    function listAllPais() {
        Api.Pais.get({limit:1000}, function(registros) {
            $scope.paises = registros.itens;
        });
    }
    
    function listAll() {
        var dados = {page: $scope.currentPage};

        if($scope.busca) {
            var busca = [];
            var itembusca = [];
            itembusca.push({ key:'titulo', value:$scope.busca, type:'like' });
            busca.push(itembusca);
            dados.busca = busca;
        }

        Api.Logradouro.get(dados,
            function (registros) {
                $scope.registros = registros.itens;
                $scope.totalItems = registros.allItens;
            },
            function (erro) {
                console.log(erro);
            }
        ); 
    }
    
    function getById() {
        Api.Logradouro.get(
            { id: $routeParams.registerId },
            function (registro) {
                listAllPais();
                getEstado(registro);
                getCidade(registro);
                getBairro(registro);
                $scope.registro = registro;
            }, 
            function (erro) {
                bootbox.alert('Registro inexistente!');
            }
        );
    }

    function getEstado(registro) {
        Api.Estado.get({busca:[{key:'pais', value:registro.pais, type:'normal'}], limit:1000}, function(registros) {
            $scope.estados = registros.itens;
        });
    }

    function getCidade(registro) {
        Api.Cidade.get({busca:[{key:'estado', value:registro.estado, type:'normal'}], limit:1000}, function(registros) {
            $scope.cidades = registros.itens;
        });
    }

    function getBairro(registro) {
        Api.Bairro.get({busca:[{key:'cidade', value:registro.cidade, type:'normal'}], limit:1000}, function(registros) {
            $scope.bairros = registros.itens;
        });
    }

    $scope.changePais = function (registro) {
        getEstado(registro);
    };

    $scope.changeEstado = function (registro) {
        getCidade(registro);
    };

    $scope.changeCidade = function (registro) {
        getBairro(registro);
    };

    $scope.buscaCep = function (registro) {
        if(!isNaN(registro.cep) && registro.cep != null && registro.cep != '' && registro.cep.length == 8) {
            Api.Logradouro.get({busca:[{key:'cep', value:registro.cep, type:'normal'}], limit:1}, function(endereco) {
                if(endereco.allItens > 0) {
                    var item = endereco.itens[0];
                    $('#btnBuscarCep').text('Encontrado!').removeClass('btn-inverse').removeClass('btn-danger').addClass('btn-success');
                    listAllPais();
                    $scope.registro.pais = item.pais._id;
                    getEstado(item);
                    $scope.registro.estado = item.estado._id;
                    getCidade(item);
                    $scope.registro.cidade = item.cidade._id;
                    getBairro(item);
                    $scope.registro.bairro = item.bairro._id;
                    $scope.registro.titulo = item.titulo;
                } else {
                    $http.get('http://api.postmon.com.br/cep/' + registro.cep).success(function(endereco) {
                        if(endereco != null) {
                            $('#btnBuscarCep').text('Encontrado').removeClass('btn-inverse').removeClass('btn-danger').addClass('btn-success');
                            $scope.registro.titulo = endereco.logradouro;

                            var estado = new Api.Estado();
                            estado.pais = '56fb0923fba10d0417ebc3b8'; // Brasil
                            estado.uf = endereco.estado;
                            estado.titulo = endereco.estado_info.nome;
                            estado.$save().then(function(regEstado) {
                                listAllPais();
                                getEstado(regEstado.reg);
                                $scope.registro.pais = regEstado.reg.pais;
                                $scope.registro.estado = regEstado.reg._id;

                                var cidade = new Api.Cidade();
                                cidade.pais = regEstado.reg.pais;
                                cidade.estado = regEstado.reg._id;
                                cidade.titulo = endereco.cidade;
                                cidade.$save().then(function(regCidade) {
                                    getCidade(regCidade.reg);
                                    $scope.registro.cidade = regCidade.reg._id;

                                    var bairro = new Api.Bairro();
                                    bairro.pais = regCidade.reg.pais;
                                    bairro.estado = regCidade.reg.estado;
                                    bairro.cidade = regCidade.reg._id;
                                    bairro.titulo = endereco.bairro;
                                    bairro.$save().then(function(regBairro) {
                                        getBairro(regBairro.reg);
                                        $scope.registro.bairro = regBairro.reg._id;
                                    });
                                });
                            });
                        } else {
                            $('#btnBuscarCep').text('Não encontrado').removeClass('btn-inverse').removeClass('btn-success').addClass('btn-danger');
                        }
                    }).error(function(err) {
                        console.log(err);
                        $('#btnBuscarCep').text('Não encontrado').removeClass('btn-inverse').removeClass('btn-success').addClass('btn-danger');
                    });
                }
            });
        }
    };
    
    $scope.remove = function (registro) {
        bootbox.confirm("Tem certeza que deseja excluir o registro?", function(answer) {
            if(answer == true) {
                Api.Logradouro.delete({id: registro._id}, 
                    function(res) {                        
                        var index = -1;
                        var comArr = eval($scope.registros);
                        
                        for(var i = 0; i < comArr.length; i++) {
                            if(comArr[i]._id === registro._id) {
                                index = i;
                                break;
                            }
                        }
                        
                        if(index === -1) {
                            listAll();
                        }

                        $scope.registros.splice(index, 1);
                        $scope.totalItems--;
                    }, 
                    function(erro) {
                        bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
                    }
                );
            }
        });
    };
    
    $scope.salva = function () {
        $scope.originalRegistro = angular.copy($scope.registro);
        $("#botaoSalvar").text("Aguarde!");
        $scope.registro.$save()
        .then(function (res) {
            $("#botaoSalvar").text(res.mensagem).removeClass("btn-inverse").removeClass("btn-danger").addClass(res.class);
            if(res.class == "btn-danger") {
                $scope.registro = angular.copy($scope.originalRegistro);
                $scope.form.$setDirty();
            } else {
                $timeout(function(){ $location.path('logradouro'); }, 1500);
            }
        }, function() {
            bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
        });
    };
});