angular.module('guiadr').controller('IndexPainelCtrl', function (Api, $scope, $routeParams, $location, $timeout) {
    $scope.totalContato = 0;
    $scope.totalProfissional = 0;
    $scope.totalClinica = 0;
    $scope.totalBlog = 0;
    $scope.totalMensagem = 0;
    getAllData();

    function getAllData() {
        Api.Contato.get({}, function (registros) {
            $scope.totalContato = registros.allItens;
        });

        Api.Profissional.get({}, function (registros) {
            $scope.totalProfissional = registros.allItens;
        });

        Api.Clinica.get({}, function (registros) {
            $scope.totalClinica = registros.allItens;
        }); 

        Api.Blog.get({}, function (registros) {
            $scope.totalBlog = registros.allItens;
        }); 

        Api.Mensagem.get({}, function (registros) {
            $scope.totalMensagem = registros.allItens;
        });
    }
});