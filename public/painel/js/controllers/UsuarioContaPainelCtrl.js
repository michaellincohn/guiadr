angular.module('guiadr').controller('UsuarioContaPainelCtrl', function (Api, Auth, $scope, $routeParams, $location, $timeout) {
    $scope.nomePagina = 'Minha Conta';
    $scope.userid;

    getById();
    
    function getById() {
        Auth.then(function(user) {
            $scope.registro = new Api.Usuario();
            $scope.registro = user;
            $scope.userid = user._id;
            $scope.registro.password = '';
        },
        function(erro) {
            bootbox.alert('Conta inexistente!', function () {
                $location.path('home');
            });
        });
    }
    
    $scope.salva = function () {
        $scope.originalRegistro = angular.copy($scope.registro);
        $("#botaoSalvar").text("Aguarde!");

        console.log($scope.userid);

        var usuario = new Api.Usuario();
        usuario._id = $scope.userid;
        usuario.nome = $scope.registro.nome;
        usuario.email = $scope.registro.email;
        usuario.password = $scope.registro.password;
        usuario.$save()
        .then(function (res) {
            $("#botaoSalvar").text(res.mensagem).removeClass("btn-inverse").addClass(res.class);
            $scope.registro = angular.copy($scope.originalRegistro);
            $scope.form.$setDirty();
            $timeout(function() {
                $("#botaoSalvar").text("Salvar").removeClass("btn-danger").removeClass("btn-success").addClass("btn-inverse");
            }, 3000);
        }, function() {
            $("#botaoSalvar").text("Aguarde!").removeClass("btn-inverse").addClass("btn-danger");
            bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.', function () {
                $location.path('home');
            });
        });
    };
});