angular.module('guiadr').controller('ClinicaProfissionalPainelCtrl', function (Api, $scope, $routeParams, $location, $timeout) {
    $scope.itens = [];
    $scope.nomePagina = 'Profissionais';
    $scope.registro = new Api.ClinicaProfissional();
    listAll();

    function listAll() {
        Api.ClinicaProfissional.get(
            { id: $routeParams.registerId },
            function (registro) {
                $scope.registro = registro;
                $scope.itens = registro.referencia;
            },
            function (erro) {
                bootbox.alert('Registro inexistente!', function () {
                    $location.path('clinica');
                });
            }
        );
    }

    $scope.getProfissionais = function (val) {
        var dados = {};

        var busca = [];
        var itembusca = [];
        itembusca.push({ key:'nome', value:val, type:'like' });
        busca.push(itembusca);
        dados.busca = busca;

        return Api.ClinicaProfissional.query(dados).$promise.then(function(r) {
            return r;
        });
    };

    $scope.onSelect = function($item, $model, $label) {
        $scope.registro.referencia = $item;
    }
    
    $scope.remove = function (registro) {
        bootbox.confirm("Tem certeza que deseja excluir o profissional?", function(answer) {
            if(answer == true) {
                Api.ClinicaProfissional.delete({id: registro._id}, 
                    function(res) {                        
                        var index = -1;
                        var comArr = eval($scope.itens);
                        
                        for(var i = 0; i < comArr.length; i++) {
                            if(comArr[i]._id === registro._id) {
                                index = i;
                                break;
                            }
                        }
                        
                        if(index === -1) {
                            listAll();
                        }

                        $scope.itens.splice(index, 1);
                        $scope.totalItems--;
                    }, 
                    function(erro) {
                        bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
                    }
                );
            }
        });
    };
    
    $scope.salva = function () {
        $("#botaoSalvar").text("Aguarde!");
        $scope.registro.$save()
        .then(function (res) {
            $("#botaoSalvar").text(res.mensagem).removeClass("btn-inverse").removeClass("btn-danger").addClass(res.class);
            listAll();
        }, function() {
            bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
        });
    };
    
    $scope.avanca = function () {
        $("#botaoAvanca").text("Aguarde!");
        $location.path('clinicaLocais/' + $routeParams.registerId);
    };

    $scope.volta = function () {
        $("#botaoVolta").text("Aguarde!");
        $location.path('clinica/' + $routeParams.registerId);
    };
});