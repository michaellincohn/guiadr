angular.module('guiadr').controller('ComentarioPainelCtrl', function (Api, $scope, $filter, $routeParams, $location, $timeout) {
    $scope.registros = [];
    $scope.nomePagina = 'Lista';
    $scope.isActive = 0;

    getById();
    
    function getById() {
        Api.Blog.get(
            { id: $routeParams.registerId },
            function (registro) {
                $scope.registros = registro.comentarios;
            }, 
            function (erro) {
                bootbox.alert('Registro inexistente!', function () {
                    $location.path('blog');
                });
            }
        );
    }

    $scope.remove = function (registro) {
        bootbox.confirm("Tem certeza que deseja excluir o registro?", function(answer) {
            if(answer == true) {
                Api.Comentario.delete({id: registro._id}, 
                    function(res) {                        
                        var index = -1;
                        var comArr = eval($scope.registros);
                        
                        for(var i = 0; i < comArr.length; i++) {
                            if(comArr[i]._id === registro._id) {
                                index = i;
                                break;
                            }
                        }
                        
                        if(index === -1) {
                            listAll();
                        }

                        $scope.registros.splice(index, 1);
                        $scope.totalItems--;
                    }, 
                    function(erro) {
                        bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
                    }
                );
            }
        });
    };

    $scope.ver = function (registro) {
        bootbox.dialog({
                title: "Detalhes do comentário",
                message: registro.descricao + "<br /><br />Postado por: <div class='label label-primary'>" + registro.nome + "</div>" +
                        "<br />Data: <div class='label label-primary'>" + $filter('toShortDateBrazil')(registro.dtInclusao) + "</div>",
                buttons: {
                    success: {
                        label: "Fechar",
                        className: "btn-info"
                    }
                }
            }
        );
    };
    
    $scope.status = function (event, registro) {
        Api.Comentario.save(
            { post: registro, idPost: $routeParams.registerId }, 
            function(res) {
                registro.isAtivo = !registro.isAtivo;
            }, 
            function (erro) {
                bootbox.alert('Ocorreu um erro no servidor que impossibilita esta alteração!');
            }
        );
    };
});