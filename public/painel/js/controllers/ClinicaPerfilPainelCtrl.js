angular.module('guiadr').controller('ClinicaPerfilPainelCtrl', function (Api, Upload, $scope, $routeParams, $location, $timeout) {
    $scope.obj = {}
    $scope.obj.coords = [10, 10, 110, 110, 100, 50];
    $scope.progressBar = '';
    $scope.nomePagina = 'Perfil';
    $scope.haveImagem = 0;
    $scope.imagem = '';
    var imagem = '';

    getById();

    function getById() {
        Api.Clinica.get(
            { id: $routeParams.registerId },
            function (registro) {
                $scope.haveImagem = parseInt((registro.imagem == "" ? 0 : 1));

                if(registro.imagem == null || registro.imagem == "") {
                    imagem = '../vendor/painel/img/no-user-image-square.jpg';
                } else {
                    imagem = '../portal/_ups/' + registro.imagem;
                }

                $scope.imagem = imagem;
            }, 
            function (erro) {
                bootbox.alert('Registro inexistente!', function () {
                    $location.path('clinica');
                });
            }
        );
    }

    $scope.upload = function (file, err) {
        Upload.upload({
            url: 'upload/clinica',
            data: { file: file, nameid: $routeParams.registerId },
        }).then(function (response) {
            $timeout(function () {
                $scope.obj.src = '../' + response.data.file;
                $scope.obj.thumbnail = true;
                $scope.progressBar = '50';
            });
        }, function (response) {
            if (response.status > 0) $scope.errorMsg = response.status + ': ' + response.data;
        }, function (evt) {
            $scope.progress = parseInt(100.0 * evt.loaded / evt.total);
        });
    }

    $scope.resize = function (file, err) {
        $scope.src = "";
        var upload = new Api.Resize({module:'clinica'});
        upload.coords = file.coords;
        upload.file = file.src;
        upload.width = 205;
        upload.height = null;
        upload.$save()
        .then(function (res) {
            if(res.file == null)
                return bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');

            var filePath = res.file.replace('./public/', '../');

            var clinica = new Api.Clinica();
            clinica._image = res.file.replace('./public/portal/_ups/', '');
            clinica._id = $routeParams.registerId;
            clinica.$save()
            .then(function (res) {
                $scope.src = filePath;
                $scope.haveImagem = 0;
            }, function() {
                bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
            });
        }, function(err) {
            bootbox.alert('Ocorreu um problema ao salvar! Tente novamente mais tarde.');
        });

        $scope.progressBar = '100';
    }

    $scope.finaliza = function () {
        $("#botaoFinaliza").text("Aguarde!");
        $location.path('clinica');
    };

    $scope.volta = function () {
        $("#botaoVolta").text("Aguarde!");
        $location.path('clinicaLocais/' + $routeParams.registerId);
    };
});