angular.module('guiadr').controller('ProfissionalCategoriaPainelCtrl', function (Api, $scope, $routeParams, $location, $timeout) {
    $scope.itens = [];
    $scope.nomePagina = 'Categorias';
    $scope.registro = new Api.ProfissionalCategoria();
    listAllCategoria();
    listAll();

    function listAllCategoria() {
        Api.Categoria.get(function(registros) {
            $scope.categorias = registros.itens;
        });
    }

    function listAll() {
        Api.ProfissionalCategoria.get(
            { id: $routeParams.registerId },
            function (registro) {
                $scope.registro = registro;
                $scope.itens = registro.categoria;
            },
            function (err) {
                bootbox.alert('Registro inexistente!', function () {
                    $location.path('profissional');
                });                
            }
        );
    }
    
    $scope.remove = function (registro) {
        bootbox.confirm("Tem certeza que deseja excluir o registro?", function(answer) {
            if(answer == true) {
                Api.ProfissionalCategoria.delete({id: registro._id}, 
                    function(res) {                        
                        var index = -1;
                        var comArr = eval($scope.itens);
                        
                        for(var i = 0; i < comArr.length; i++) {
                            if(comArr[i]._id === registro._id) {
                                index = i;
                                break;
                            }
                        }
                        
                        if(index === -1) {
                            listAll();
                        }

                        $scope.itens.splice(index, 1);
                        $scope.totalItems--;
                    }, 
                    function(erro) {
                        bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
                    }
                );
            }
        });
    };
    
    $scope.salva = function () {
        $("#botaoSalvar").text("Aguarde!");
        $scope.registro.$save()
        .then(function (res) {
            $("#botaoSalvar").text(res.mensagem).removeClass("btn-inverse").removeClass("btn-danger").addClass(res.class);
            listAll();
            categoriaRemove(res.id);
        }, function() {
            bootbox.alert('Ocorreu um problema! Tente novamente mais tarde.');
        });
    };
    
    $scope.avanca = function () {
        $("#botaoAvanca").text("Aguarde!");
        $location.path('profissionalLocais/' + $routeParams.registerId);
    };

    $scope.volta = function () {
        $("#botaoVolta").text("Aguarde!");
        $location.path('profissional/' + $routeParams.registerId);
    };

    var categoriaRemove = function (id) {
        var index = -1;
        var categorias = eval($scope.categorias);
        if(categorias == null) return false;
        for(var i = 0; i < categorias.length; i++) {
            if(categorias[i]._id === id) {
                index = i;
                break;
            }
        }
        if(index === -1) {
            listAllCategoria();
        }
        $scope.categorias.splice(index, 1);
    }
});