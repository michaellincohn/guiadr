(function () {
    var toShortDateBrazil = function () {
        return function(value) {
            /*
                LT : 'HH:mm',
                L : 'DD/MM/YYYY',
                LL : 'D [de] MMMM [de] YYYY',
                LLL : 'D [de] MMMM [de] YYYY [às] LT',
                LLLL : 'dddd, D [de] MMMM [de] YYYY [às] LT'
                https://gist.github.com/fernandosavio/680a2549e417befea930
            */
            return moment(value).format("L");
        };
    };

    var toDateTimeBrazil = function () {
        return function(value) {
            /*
                LT : 'HH:mm',
                L : 'DD/MM/YYYY',
                LL : 'D [de] MMMM [de] YYYY',
                LLL : 'D [de] MMMM [de] YYYY [às] LT',
                LLLL : 'dddd, D [de] MMMM [de] YYYY [às] LT'
                https://gist.github.com/fernandosavio/680a2549e417befea930
            */
            return moment(value).format("L [às] LT");
        };
    };

    var toStrFromObj = function () {
        return function(obj) {
            if(obj != null) {
                var str = '';
                for (var i = 0; i < obj.length; i++) {
                    str += obj[i].titulo + ', ';
                }
                str = str.substring(0, (str.length - 2))
                return str;
            }
        }
    };

    var toDisponibilidadeHour = function () {
        return function(obj) {
            var str = '';
            
            for (var i = 0; i < obj.length; i++) {
                if(obj[i].status == 1) {
                    str += ' ' + obj[i].hora + ' -';
                }
            }

            str = str.substring(1, str.length - 2);

            return str;
        }
    };

    var totalObj = function () {
        return function(obj) {
            return (obj != null ? parseInt(obj.length) : 0);
        }
    };

    var toBirthdayDateBrazil = function () {
        return function(birthday) {
            var now = moment(Date.now());
            var mid = moment(birthday);
            var days = mid.diff(now, 'year');

            return (birthday != null ? Math.abs(days) : 0);
        }
    };

    var toStatusConsulta = function () {
        return function(status) {
            var txRetorno;
            switch (status) {
                case 1: txRetorno = 'Agendada'; break;
                case 2: txRetorno = 'Em Consulta'; break;
                case 3: txRetorno = 'Efetuada'; break;
                case 4: txRetorno = 'Perdida'; break;
                case 5: txRetorno = 'Cancelada'; break;
                default: txRetorno = '';
            }
            return txRetorno;
        }
    };

    var haveStringOrValue = function () {
        return function(value) {
            return (value ? value : 'NÃO');
        };
    };

    var haveNumberOrZero = function () {
        return function(value) {
            return (value ? parseInt(value) : 0);
        };
    };

    var haveString = function () {
        return function(value) {
            return (value ? 'SIM' : 'NÃO');
        };
    };

    var simNao = function () {
        return function(value) {
            return (value ? 'SIM' : 'NÃO');
        }
    };

    angular.module('guiadr')
    .filter('toShortDateBrazil', toShortDateBrazil)
    .filter('toDateTimeBrazil', toDateTimeBrazil)
    .filter('toStrFromObj', toStrFromObj)
    .filter('toDisponibilidadeHour', toDisponibilidadeHour)
    .filter('totalObj', totalObj)
    .filter('toBirthdayDateBrazil', toBirthdayDateBrazil)
    .filter('toStatusConsulta', toStatusConsulta)
    .filter('haveStringOrValue', haveStringOrValue)
    .filter('haveNumberOrZero', haveNumberOrZero)
    .filter('haveString', haveString)
    .filter('simNao', simNao);
}());
