(function () {
    var Api = function($resource) {
        return {
            Horario: $resource('/painel/horario/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            Disponibilidade: $resource('/painel/disponibilidade/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            Consulta: $resource('/painel/consulta/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            Paciente: $resource('/painel/paciente/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            Comentario: $resource('/painel/comentario/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            Editoria: $resource('/painel/editoria/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            Categoria: $resource('/painel/categoria/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            Blog: $resource('/painel/blog/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            BlogFoto: $resource('/painel/blogFoto/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            Faq: $resource('/painel/faq/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            Pagina: $resource('/painel/pagina/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            Resize: $resource('/painel/resize/:module', {module: '@module'}/*, { save: { ignoreLoadingBar: false } }*/),
            Upload: $resource('/painel/upload/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            ProfissionalPerfil: $resource('/painel/profissionalPerfil/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            ProfissionalLocais: $resource('/painel/profissionalLocais/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            ProfissionalCategoria: $resource('/painel/profissionalCategoria/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            Profissional: $resource('/painel/profissional/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            ClinicaPerfil: $resource('/painel/clinicaPerfil/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            ClinicaLocais: $resource('/painel/clinicaLocais/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            ClinicaProfissional: $resource('/painel/clinicaProfissional/:id', {id: '@id'}, { query: {method: 'GET', isArray: true } }),
            Clinica: $resource('/painel/clinica/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            Especialidade: $resource('/painel/especialidade/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            Mensagem: $resource('/painel/mensagem/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
			Contato: $resource('/painel/contato/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            Usuario: $resource('/painel/usuario/:id', {id: '@id'}/*, { save: { ignoreLoadingBar: false } }*/),
            Pais: $resource('/painel/pais/:id', {id: '@id'}),
            Estado: $resource('/painel/estado/:id', {id: '@id'}),
            Cidade: $resource('/painel/cidade/:id', {id: '@id'}),
            Bairro: $resource('/painel/bairro/:id', {id: '@id'}),
			Logradouro: $resource('/painel/logradouro/:id', {id: '@id'}),
		}
    };

    var fileReader = function ($q) {
        var onLoad = function (reader, deferred, Sscope) {
            return function () {
                Sscope.$apply(function () {
                    deferred.resolve(reader.result);
                });
            };
        };

        var onError = function (reader, deferred, Sscope) {
            return function () {
                Sscope.$apply(function () {
                    deferred.reject(reader.result);
                });
            };
        };

        var onProgress = function (reader, Sscope) {
            return function (event) {
                Sscope.$broadcast(
                    "fileProgress", {
                        total: event.total, 
                        loaded: event.loaded
                    }
                );
            };
        };

        var getReader = function (deferred, Sscope) {
            var reader = new FileReader();
            reader.onload = onLoad(reader, deferred, Sscope);
            reader.onerror = onError(reader, deferred, Sscope);
            reader.onprogress = onProgress(reader, Sscope);
            return reader;
        };

        var readAsDataURL = function (file, Sscope) {
            var deferred = $q.defer();
            var reader = getReader(deferred, Sscope);
            reader.readAsDataURL(file);
            return deferred.promise;
        };

        return { readAsDataUrl: readAsDataURL };
    };

    var Auth = function($http, $q) {
        var r = $q.defer();
		$http.get('/painel/auth/data').then(function(user) {
			r.resolve(user.data);
		});
		return r.promise;
    };

    var Interceptor = function($q, $window) {
		return {
			request: function (req) {
				req.headers = req.headers || {};
				return req;
			},
			responseError: function(rejection) {
    			if (rejection.status == 401) {
					$window.location = '/painel/auth/logout';
    			}
            	return $q.reject(rejection);
    		}
    	}
    };

    angular.module('guiadr')
    .factory('fileReader', fileReader)
    .factory('Interceptor', Interceptor)
    .factory('Api', Api)
    .factory('Auth', Auth);
}());