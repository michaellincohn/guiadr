(function () {
    var textAreaAutoSize = function () {
        return {
            restrict: 'C',
            link: function (scope, element, attrs) {
                $(element).autosize({append: "\n"});
            }
        };
    };

    var tagInputSelect2 = function () {
        return {
            restrict: 'C',
            link : function (scope, element, attrs, Api) {
                $(element).select2({
                    width: "100%", 
                    tags:[]
                });
            }
        };
    };

    var tagInput = function () {
        return {
            restrict: 'C',
            require:"ngModel",
            link : function (scope, element, attrs, ngModel) {
                scope.$watch(function() {
                    return ngModel.$modelValue;
                }, function(modelValue) {
                    if(modelValue)
                        element.tagsInput({ width:'100%', height:'auto' }).importTags(modelValue);
                    else
                        element.tagsInput({ width:'100%', height:'auto' });
                });
            }
        };
    };

    var inputCheck = function() {
        return {
            restrict: 'C',
            //terminal: true,
            scope: { value: '=' },
            link : function (scope, element) {
                $(function(){
                    element.iCheck({
                        checkboxClass: 'icheckbox_flat-blue',
                        radioClass: 'iradio_flat-blue'
                    });
                });
            }
        };
    };

    var datepicker = function () {
        return {
            restrict: 'A',
            require : 'ngModel',
            link : function (scope, element, attrs, ngModelCtrl) {
                $(function(){
                    element.datepicker({
                        dateFormat:'dd/mm/yyyy',
                        language: 'pt-BR',
                        autoclose: true,
                        onSelect:function (date) {
                            scope.$apply(function () {
                                ngModelCtrl.$setViewValue(date);
                            });
                        }
                    });
                });
            }
        };
    };

    var dateFormat = function () {
        return {
            restrict: 'A',
            link: function(scope, el, at) {
                console.log(el);
                if(el.val() != "") {
                    var format = at.dateFormat;
                    scope.$watch(at.ngModel, function(date) {
                        var result = moment(date).format(format);
                        el.val(result);
                    });
                }
            }
        };
    };

    var appDatetime = function ($window) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
                var moment = $window.moment;

                ngModel.$formatters.push(formatter);
                ngModel.$parsers.push(parser);

                element.on('change', function (e) {
                    var element = e.target;
                    element.value = formatter(ngModel.$modelValue);
                });

                function parser(value) {
                    var m = moment(value);
                    var valid = m.isValid();
                    ngModel.$setValidity('datetime', valid);
                    if (valid) return m.valueOf();
                    else return value;
                }

                function formatter(value) {
                    var m = moment(value);
                    var valid = m.isValid();
                    if (valid) return m.format("LLLL");
                    else return value;
                }
            }
        };
    };

    angular.module('guiadr')
    .directive('textAreaAutoSize', textAreaAutoSize)
    .directive('tagInput', tagInput)
    .directive('tagInputSelect2', tagInputSelect2)
    .directive('inputCheck', inputCheck)
    .directive('datepicker', datepicker)
    .directive('dateFormat', dateFormat)
    .directive('appDatetime', appDatetime);
}());