angular.module('guiadr', ['ngRoute', 'ngResource', 'ngAnimate', 'ngSanitize', 'ui.bootstrap', 'angular-loading-bar', 'ngTagsInput', 'ngFileUpload', 'ngJcrop', 'ngCkeditor'])
.config(['$routeProvider', '$httpProvider', 'cfpLoadingBarProvider', function($routeProvider, $httpProvider, cfpLoadingBarProvider) {

    $routeProvider.when('/pagina', { templateUrl: 'partials/pagina/list.html', controller: 'PaginaPainelCtrl' });
    $routeProvider.when('/pagina/:registerId', { templateUrl: 'partials/pagina/form.html', controller: 'PaginaPainelCtrl' });

    $routeProvider.when('/horario', { templateUrl: 'partials/horario/list.html', controller: 'HorarioPainelCtrl' });
    $routeProvider.when('/horario/:registerId', { templateUrl: 'partials/horario/form.html', controller: 'HorarioPainelCtrl' });

    $routeProvider.when('/disponibilidade', { templateUrl: 'partials/disponibilidade/list.html', controller: 'DisponibilidadePainelCtrl' });
    $routeProvider.when('/disponibilidade/:registerId', { templateUrl: 'partials/disponibilidade/form.html', controller: 'DisponibilidadePainelCtrl' });

    $routeProvider.when('/editoria', { templateUrl: 'partials/editoria/list.html', controller: 'EditoriaPainelCtrl' });
    $routeProvider.when('/editoria/:registerId', { templateUrl: 'partials/editoria/form.html', controller: 'EditoriaPainelCtrl' });

    $routeProvider.when('/categoria', { templateUrl: 'partials/categoria/list.html', controller: 'CategoriaPainelCtrl' });
    $routeProvider.when('/categoria/:registerId', { templateUrl: 'partials/categoria/form.html', controller: 'CategoriaPainelCtrl' });

    $routeProvider.when('/paciente', { templateUrl: 'partials/paciente/list.html', controller: 'PacientePainelCtrl' });
    
    $routeProvider.when('/consulta/:registerId', { templateUrl: 'partials/consulta/list.html', controller: 'ConsultaPainelCtrl' });

    $routeProvider.when('/blog', { templateUrl: 'partials/blog/list.html', controller: 'BlogPainelCtrl' });
    $routeProvider.when('/blog/:registerId', { templateUrl: 'partials/blog/form.html', controller: 'BlogPainelCtrl' });
    $routeProvider.when('/blogFoto/:registerId', { templateUrl: 'partials/blog/formFoto.html', controller: 'BlogFotoPainelCtrl' });

    $routeProvider.when('/comentario/:registerId', { templateUrl: 'partials/comentario/list.html', controller: 'ComentarioPainelCtrl' });

    $routeProvider.when('/profissional', { templateUrl: 'partials/profissional/list.html', controller: 'ProfissionalPainelCtrl' });
    $routeProvider.when('/profissional/:registerId', { templateUrl: 'partials/profissional/form.html', controller: 'ProfissionalPainelCtrl' });
    $routeProvider.when('/profissionalCategoria/:registerId', { templateUrl: 'partials/profissional/formCategoria.html', controller: 'ProfissionalCategoriaPainelCtrl' });
    $routeProvider.when('/profissionalLocais/:registerId', { templateUrl: 'partials/profissional/formLocais.html', controller: 'ProfissionalLocaisPainelCtrl' });
    $routeProvider.when('/profissionalPerfil/:registerId', { templateUrl: 'partials/profissional/formPerfil.html', controller: 'ProfissionalPerfilPainelCtrl' });

    $routeProvider.when('/clinica', { templateUrl: 'partials/clinica/list.html', controller: 'ClinicaPainelCtrl' });
    $routeProvider.when('/clinica/:registerId', { templateUrl: 'partials/clinica/form.html', controller: 'ClinicaPainelCtrl' });
    $routeProvider.when('/clinicaProfissional/:registerId', { templateUrl: 'partials/clinica/formProfissionais.html', controller: 'ClinicaProfissionalPainelCtrl' });
    $routeProvider.when('/clinicaLocais/:registerId', { templateUrl: 'partials/clinica/formLocais.html', controller: 'ClinicaLocaisPainelCtrl' });
    $routeProvider.when('/clinicaPerfil/:registerId', { templateUrl: 'partials/clinica/formPerfil.html', controller: 'ClinicaPerfilPainelCtrl' });

    $routeProvider.when('/faq', { templateUrl: 'partials/faq/list.html', controller: 'FaqPainelCtrl' });
    $routeProvider.when('/faq/:registerId', { templateUrl: 'partials/faq/form.html', controller: 'FaqPainelCtrl' });

    $routeProvider.when('/mensagem', { templateUrl: 'partials/mensagem/list.html', controller: 'MensagemPainelCtrl' });
    $routeProvider.when('/mensagem/:registerId', { templateUrl: 'partials/mensagem/form.html', controller: 'MensagemPainelCtrl' });
    
    $routeProvider.when('/contato', { templateUrl: 'partials/contato/list.html', controller: 'ContatoPainelCtrl' });

    $routeProvider.when('/usuario', { templateUrl: 'partials/usuario/list.html', controller: 'UsuarioPainelCtrl' });
    $routeProvider.when('/usuario/:registerId', { templateUrl: 'partials/usuario/form.html', controller: 'UsuarioPainelCtrl' });

    $routeProvider.when('/logradouro', { templateUrl: 'partials/logradouro/list.html', controller: 'LogradouroPainelCtrl' });
    $routeProvider.when('/logradouro/:registerId', { templateUrl: 'partials/logradouro/form.html', controller: 'LogradouroPainelCtrl' });

    $routeProvider.when('/bairro', { templateUrl: 'partials/bairro/list.html', controller: 'BairroPainelCtrl' });
    $routeProvider.when('/bairro/:registerId', { templateUrl: 'partials/bairro/form.html', controller: 'BairroPainelCtrl' });

    $routeProvider.when('/cidade', { templateUrl: 'partials/cidade/list.html', controller: 'CidadePainelCtrl' });
    $routeProvider.when('/cidade/:registerId', { templateUrl: 'partials/cidade/form.html', controller: 'CidadePainelCtrl' });

    $routeProvider.when('/estado', { templateUrl: 'partials/estado/list.html', controller: 'EstadoPainelCtrl' });
    $routeProvider.when('/estado/:registerId', { templateUrl: 'partials/estado/form.html', controller: 'EstadoPainelCtrl' });

    $routeProvider.when('/pais', { templateUrl: 'partials/pais/list.html', controller: 'PaisPainelCtrl' });
    $routeProvider.when('/pais/:registerId', { templateUrl: 'partials/pais/form.html', controller: 'PaisPainelCtrl' });

    $routeProvider.when('/conta', { templateUrl: 'partials/usuario/formConta.html', controller: 'UsuarioContaPainelCtrl' });
    $routeProvider.when('/home', { templateUrl: 'partials/index.html', controller: 'IndexPainelCtrl' });
    $routeProvider.otherwise({ redirectTo: '/home' });

    $httpProvider.interceptors.push('Interceptor');

    // https://github.com/chieffancypants/angular-loading-bar
    cfpLoadingBarProvider.includeSpinner = true;
    cfpLoadingBarProvider.includeBar = true;
}]);

angular.module('guiadr').config(function(ngJcropConfigProvider) {
    ngJcropConfigProvider.setJcropConfig('blog', { bgColor:'black', bgOpacity:.3, aspectRatio:16/5 });
    ngJcropConfigProvider.setPreviewStyle('blog', { 'width':'650px', 'height':'230px', 'overflow':'hidden', 'margin-left':'5px'});

    ngJcropConfigProvider.setJcropConfig('profissional', { bgColor:'black', bgOpacity:.3, aspectRatio:1 });
    ngJcropConfigProvider.setPreviewStyle('profissional', { 'width':'205px', 'height':'205px', 'overflow':'hidden', 'margin-left':'5px'});
});