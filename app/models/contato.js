var mongoose = require('mongoose');

module.exports = function () {
    var schema = mongoose.Schema({
        nome: String,
        email: String,
        mensagem: String,
        dtInclusao: {
            type: Date,
            default: Date.now()
        },
        dtExclusao: {
            type: Date
        }
    }, {
        versionKey: false
    });

    return mongoose.model('Contato', schema);
};