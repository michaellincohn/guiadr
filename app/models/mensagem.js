var mongoose = require('mongoose');

module.exports = function () {
    var schema = mongoose.Schema({
        nome: String,
        email: String,
        mensagem: String,
        toId: {
            type: mongoose.Schema.ObjectId, 
            ref: 'Usuario', 
            required: true
        },
        dtInclusao: {
            type: Date,
            default: Date.now()
        },
        dtExclusao: {
            type: Date,
            default: null
        }
    }, {
        versionKey: false
    });

    return mongoose.model('Mensagem', schema);
};