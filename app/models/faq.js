var mongoose = require('mongoose');

module.exports = function () {
    var schema = mongoose.Schema({
        titulo: String,
        descricao: String,
        dtInclusao: {
            type: Date,
            default: Date.now()
        },
        dtExclusao: {
            type: Date,
            default: null
        }
    }, {
        versionKey: false
    });

    return mongoose.model('Faq', schema);
};