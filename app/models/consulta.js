var mongoose = require('mongoose');

module.exports = function () {
    var schema = mongoose.Schema({
        paciente: {
            type: mongoose.Schema.ObjectId,
            ref: 'Usuario'
        },
        medico: {
            type: mongoose.Schema.ObjectId,
            ref: 'Usuario'
        },
        especialidade: String,
        status: Number,
        dtNascimento: Date,
        dtAgendamento: Date,
        dtInclusao: {
            type: Date,
            default: Date.now()
        }
    }, {
        versionKey: false
    });

    return mongoose.model('Consulta', schema);
};