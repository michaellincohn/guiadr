var mongoose = require('mongoose');

/*
    Os tipo de usuários:
    1 = Administrador
    2 = Profissional/Medico
    3 = Clinica
    4 = Paciente
*/

module.exports = function () {
    var planoSchema = mongoose.Schema({ tipo: Number, valor: String, titulo: String }, { versionKey: false });
    var habilidadeSchema = mongoose.Schema({ titulo: String }, { versionKey: false });
    var especialidadeSchema = mongoose.Schema({ titulo: String }, { versionKey: false });
    var horariosSchema = mongoose.Schema({ hora: String, status: { type: Number, default: 0 } }, { versionKey: false });
    var categoriaSchema = mongoose.Schema({ catId: { type: mongoose.Schema.Types.ObjectId }, titulo: String, especialidade: [especialidadeSchema], habilidade: [habilidadeSchema], horarios: [horariosSchema] }, { versionKey: false });
    var redesociaisSchema = mongoose.Schema({ linkedin: String, facebook: String, twitter: String }, { versionKey: false });
    var infoSchema = mongoose.Schema({ descricao: String, educacao: String, numreg: String, documento: String }, { versionKey: false });
    var comentarioSchema = mongoose.Schema({ nome: String, email: String, descricao: String, isAtivo: { type:Boolean, default:false }, dtInclusao: { type: Date, default: Date.now() } }, { versionKey: false });
    var enderecoSchema = mongoose.Schema({ 
        pais: {
            type: mongoose.Schema.ObjectId,
            ref: 'Pais'
        },
        estado: {
            type: mongoose.Schema.ObjectId,
            ref: 'Estado'
        },
        cidade: {
            type: mongoose.Schema.ObjectId,
            ref: 'Cidade'
        },
        bairro: {
            type: mongoose.Schema.ObjectId,
            ref: 'Bairro'
        },
        logradouro: {
            type: mongoose.Schema.ObjectId,
            ref: 'Logradouro'
        },
        numero: String,
        latlong: String, 
        telefone: [{ numero: String }]
    }, { versionKey: false });

    var schema = mongoose.Schema({
        nome: String,
        email: String,
        password: String,
        imagem: String,
        tipo: Number,
        aceitoTermos: Boolean,
        redesociais: redesociaisSchema,
        authredesociais: {
            facebook: {
                profileId: String,
                accessToken: String,
                image: String
            },
            linkedin: {},
            twitter: {},
        },
        info: infoSchema,
        plano: planoSchema,
        categoria: [categoriaSchema],
        endereco: [enderecoSchema],
        comentario: [comentarioSchema],
        referencia: [{  // Ex: profissional relacionado a uma clinica
            type: mongoose.Schema.ObjectId,
            ref: 'Usuario'
        }],
        dtInclusao: {
            type: Date,
            default: Date.now()
        },
        dtExclusao: {
            type: Date,
            default: null
        }
    }, {
        versionKey: false
    });

    return mongoose.model('Usuario', schema);
};