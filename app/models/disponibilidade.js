var mongoose = require('mongoose');

module.exports = function () {
    // 1=Livre; 2=Ocupado; 3=Indisponivel
    var horariosSchema = mongoose.Schema({ hora: String, status: { type: Number, default: 0 } }, { versionKey: false });
    var especialidadeSchema = mongoose.Schema({ titulo: String }, { versionKey: false });
    var categoriaSchema = mongoose.Schema({ _id: {type: mongoose.Schema.ObjectId, ref: 'Categoria' }, especialidade: [especialidadeSchema] }, { versionKey: false });

    var schema = mongoose.Schema({
        data: Date,
        tipo: Number, // 1=Consulta; 2=Retorno
        horario: [horariosSchema],
        categoria: categoriaSchema,
        medico: {
            type: mongoose.Schema.ObjectId,
            ref: 'Usuario'
        },
        dtInclusao: {
            type: Date,
            default: Date.now()
        },
        dtExclusao: {
            type: Date,
            default: null
        }
    }, {
        versionKey: false
    });

    return mongoose.model('Disponibilidade', schema);
};