var mongoose = require('mongoose');

module.exports = function () {
    var comentarioSchema = mongoose.Schema({
        nome: String,
        email: String,
        descricao: String,
        isAtivo: {
            type:Boolean,
            default:false
        },
        dtInclusao: {
            type: Date,
            default: Date.now()
        }
    }, {
        versionKey: false
    });

    var schema = mongoose.Schema({
        titulo: String,
        editoria: { type: mongoose.Schema.ObjectId, ref: 'Editoria' },
        chamada: String,
        conteudo: String,
        imagem: String,
        visualizacoes: { type: Number, default: 0 },
        curtidas: { type: Number, default: 0 },
        comentarios: [comentarioSchema],
        dtInclusao: { type: Date, default: Date.now() },
        dtExclusao: { type: Date, default: null }
    }, {
        versionKey: false
    });

    return mongoose.model('Blog', schema);
};