var mongoose = require('mongoose');

module.exports = function () {
    var especialidadeSchema = mongoose.Schema({ titulo: String }, { versionKey: false });

    var schema = mongoose.Schema({
        titulo: String,
        especialidade: [especialidadeSchema],
        dtInclusao: {
            type: Date,
            default: Date.now()
        },
        dtExclusao: {
            type: Date,
            default: null
        }
    }, {
        versionKey: false
    });

    return mongoose.model('Categoria', schema);
};