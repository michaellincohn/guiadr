var mongoose = require('mongoose');

module.exports = function () {
    var schema = mongoose.Schema({
        titulo: String,
        dtExclusao: {
            type: Date,
            default: null
        }
    }, {
        versionKey: false
    });

    return mongoose.model('Pais', schema);
};