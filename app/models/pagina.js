var mongoose = require('mongoose');

module.exports = function () {
    var schema = mongoose.Schema({
        titulo: String,
        conteudo: String,
        slug: String,
        dtInclusao: {
            type: Date,
            default: Date.now()
        },
        dtExclusao: {
            type: Date,
            default: null
        }
    }, {
        versionKey: false
    });

    return mongoose.model('Pagina', schema);
};