var mongoose = require('mongoose');

module.exports = function () {
    var schema = mongoose.Schema({
        pais: {
            type: mongoose.Schema.ObjectId,
            ref: 'Pais'
        },
        uf: String,
        titulo: String,
        dtExclusao: {
            type: Date,
            default: null
        }
    }, {
        versionKey: false
    });

    return mongoose.model('Estado', schema);
};