var mongoose = require('mongoose');

module.exports = function () {
    var schema = mongoose.Schema({
        pais: {
            type: mongoose.Schema.ObjectId,
            ref: 'Pais'
        },
        estado: {
            type: mongoose.Schema.ObjectId,
            ref: 'Estado'
        },
        cidade: {
            type: mongoose.Schema.ObjectId,
            ref: 'Cidade'
        },
        bairro: {
            type: mongoose.Schema.ObjectId,
            ref: 'Bairro'
        },
        cep: Number,
        tipo: String,
        titulo: String,
        dtExclusao: {
            type: Date,
            default: null
        }
    }, {
        versionKey: false
    });

    return mongoose.model('Logradouro', schema);
};