var mongoose = require('mongoose');

module.exports = function () {
    var schema = mongoose.Schema({
        pais: {
            type: mongoose.Schema.ObjectId,
            ref: 'Pais'
        },
        estado: {
            type: mongoose.Schema.ObjectId,
            ref: 'Estado'
        },
        titulo: String,
        dtExclusao: {
            type: Date,
            default: null
        }
    }, {
        versionKey: false
    });

    return mongoose.model('Cidade', schema);
};