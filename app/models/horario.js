var mongoose = require('mongoose');

module.exports = function () {
    var schema = mongoose.Schema({
        hora: String,
        dtExclusao: {
            type: Date,
            default: null
        }
    }, {
        versionKey: false
    });

    return mongoose.model('Horario', schema);
};