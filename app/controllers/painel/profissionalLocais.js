var sanitize = require('mongo-sanitize');
var bCrypt = require('bcrypt-nodejs');
var common = require('../../../config/common');
var template = require('../../../config/template');

module.exports = function(app) {
    var Model = app.models.usuario;
    var ModelPais = app.models.pais;
    var controller = {}

    controller.getById = function(req, res) {
        var _id = sanitize(req.params.id);

        var populateQuery = [
            {path:'endereco.pais'},
            {path:'endereco.estado'},
            {path:'endereco.cidade'},
            {path:'endereco.bairro'},
            {path:'endereco.logradouro', select:'titulo'}
        ];

        Model
        .findById(_id)
        .populate('endereco')
        .exec(function(err, register) {
            Model.populate(register, populateQuery, function(err, docs) {
                if (err) res.status(500).json("Registro não encontrado");
                res.status(201).json(register);
            });
        },
        function(erro) {
            res.status(404).json(erro)
        });
    }; 

    controller.removeById = function(req, res) {
        var _id = sanitize(req.params.id);

        if (_id) {
            Model.update(
                { 'endereco._id': _id },
                { $pull: { 'endereco': { '_id': _id } } }
            )
            .exec()
            .then(function (register) {
                res.status(201).json({mensagem:"Registro alterado com sucesso!", class:"btn-info", add:false, id:register._id});
            }, function(erro) {
                res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false, id:0});
            });
        }
    };

    controller.save = function(req, res) {
        var _id = sanitize(req.body._id);

        var endereco = {};
        var enderecos = [];

        var telefones = [];
        if(req.body.telefone != null) {
            for (var i = 0; i < req.body.telefone.length; i++) {
                var telefone = {};
                telefone['numero'] = req.body.telefone[i].titulo;
                telefones.push(telefone);
            }
        }

        endereco['pais'] = req.body.pais;
        endereco['estado'] = req.body.estado;
        endereco['cidade'] = req.body.cidade;
        endereco['bairro'] = req.body.bairro;
        endereco['logradouro'] = req.body.logradouro;
        endereco['numero'] = req.body.numero;
        endereco['latlong'] = req.body.latlong;
        endereco['telefone'] = telefones;
        enderecos.push(endereco);

        if (_id) {
            Model.update(
                { '_id': _id },
                { $pushAll: { 'endereco': enderecos } }
            )
            .exec()
            .then(function (register) {
                common.sendMail(register.email, 'Cadastro de Profissional', template.cadastroProfissional(register));
                res.status(201).json({mensagem:"Salvar", class:"btn-info", add:false, id:0});
            }, function(erro) {
                res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false, id:0});
            });
        }
        else
        {
            res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false, id:0});
        }
    };

    return controller;
};