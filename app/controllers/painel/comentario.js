var sanitize = require('mongo-sanitize');

module.exports = function(app) {
    var Model = app.models.blog;
    var controller = {}
    var perPage = 10;
    var totalItens = 0;

    controller.listAll = function(req, res) {
        var _id = sanitize(req.params.id);
        Model.findById(_id)
        .exec()
        .then(function(register) {
            if (!register) throw new Error("Registro não encontrado");
            res.json(register);
        },
        function(erro) {
            res.status(404).json(erro)
        });
    };

    controller.removeById = function(req, res) {
        var _id = sanitize(req.params.id);

        if (_id) {
            Model.update(
                { 'comentarios._id': _id },
                { $pull: { 'comentarios': { '_id': _id } } }
            )
            .exec()
            .then(function (register) {
                res.status(201).json({mensagem:"Registro alterado com sucesso!", class:"btn-info", add:false, id:register._id});
            }, function(erro) {
                res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false, id:0});
            });
        }
    };

    controller.status = function(req, res) {
        var _post = req.body.post;
        var _id = sanitize(_post._id);
        var _idPost = sanitize(req.body.idPost);
        var _isAtivo = !_post.isAtivo;

        if (_idPost) {
            Model.update(
                { 'comentarios._id': _id },
                { $set: { 'comentarios.$.isAtivo': _isAtivo } }
            )
            .exec()
            .then(function (register) {
                var cssClass = (_isAtivo ? "btn-primary" : "btn-danger");
                res.status(201).json({mensagem:"Registro alterado com sucesso!", class:cssClass, add:false, id:0});
            }, function(erro) {
                res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false, id:0});
            });
        }
    };

    var getTypeFunction = function(value, type) {
        if(type == 'like')
            return new RegExp('^' + value, 'i');
        else if(type == 'match')
            return new RegExp('^' + value + '$', 'i');
        else if(type == 'equals')
            return {$e : value};
        else if(type == 'notequals')
            return {$ne : value};
    }

    return controller;
};