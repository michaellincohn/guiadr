var sanitize = require('mongo-sanitize');

module.exports = function(app) {
    var Model = app.models.logradouro;
    var controller = {}
    var perPage = 10;
    var totalItens = 0;

    controller.listAll = function(req, res) {
        var json = {};
        var page = Math.max(0, (req.query.page - 1));
        var conditions = {dtExclusao: null};

        if(req.query.busca) {
            var busca = JSON.parse(req.query.busca);
            if(typeof busca === 'array') {
                for (var i = 0; i < busca.length; i++) {
                    conditions[busca[i].key] = getTypeFunction(busca[i].value, busca[i].type);
                }
            } else {
                conditions[busca.key] = getTypeFunction(busca.value, busca.type);
            }
        }

        Model.find(conditions)
        .populate('pais')
        .populate('estado')
        .populate('cidade')
        .populate('bairro')
        .limit(perPage)
        .skip(perPage * page)
        .sort({_id: 'asc'})
        .exec()
        .then(function(registers) {
            json.itens = registers;
            if(registers != null) {
                Model.count(conditions, function(e, total) {
                    json.allItens = total;
                    res.json(json);
                });
            }
        },
        function(erro) {
            res.status(500).json(erro);
        });
    };

    controller.getById = function(req, res) {
        var _id = sanitize(req.params.id);
        Model
        .findById(_id)
        .exec()
        .then(function(register) {
            if (!register) throw new Error("Registro não encontrado");
            res.json(register);
        },
        function(erro) {
            res.status(404).json(erro)
        });
    };

    controller.removeById = function(req, res) {
        var _id = sanitize(req.params.id);

        if (_id) {
            var dados = {
               "dtExclusao": Date.now()
            }

            Model
            .findByIdAndUpdate(_id, dados)
            .exec()
            .then(function(reg) {
                res.end();
            }, function(erro) {
                console.log(erro);
            });
        }
    };

    controller.save = function(req, res) {
        var _id = sanitize(req.body._id);
        var dados = {
            "cep": req.body.cep,
            "tipo": req.body.tipo,
            "pais": req.body.pais,
            "estado": req.body.estado,
            "cidade": req.body.cidade,
            "bairro": req.body.bairro,
            "titulo": req.body.titulo
        }

        if (_id) {
            Model.count({ 
                titulo: {$regex: getTypeFunction(req.body.titulo, 'match') }, 
                bairro: req.body.bairro, 
                dtExclusao: null, 
                _id: {$ne:_id}
            }, function(q, t) {
                if(t <= 0) {
                    Model.findByIdAndUpdate(_id, dados)
                    .exec().then(function(register) {
                        res.status(201).json({mensagem:"Registro alterado com sucesso!", class:"btn-primary", add:false, reg:register});
                    }, function(erro) {
                        res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false});
                    });
                } else {
                    res.status(201).json({mensagem:"Um registro foi encontrado com mesmo título", class:"btn-danger", add:false});
                }
            });
        } else {
            Model.count({ 
                titulo: {$regex: getTypeFunction(req.body.titulo, 'match') }, 
                bairro: req.body.bairro, 
                dtExclusao: null 
            }, function(q, t) {
                if(t <= 0) {
                    Model.create(dados)
                    .then(function(register) {
                        res.status(201).json({mensagem:"Registro inserido com sucesso!", class:"btn-primary", add:true, reg:register});
                    }, function(erro) {
                        res.status(500).json({add:false});
                    });
                } else {
                    Model.find({ 
                        titulo: {$regex: getTypeFunction(req.body.titulo, 'match') }, 
                        bairro: req.body.bairro, 
                        dtExclusao: null 
                    }).exec().then(function(register) {
                        res.status(201).json({mensagem:"Já existe um registro com este mesmo título", class:"btn-danger", add:true, reg:register[0]});
                    });
                }
            });
        }
    };

    var getTypeFunction = function(value, type) {
        if(type == 'like')
            return new RegExp('^' + value, 'i');
        else if(type == 'match')
            return new RegExp('^' + value + '$', 'i');
        else if(type == 'equals')
            return {$e : value};
        else if(type == 'notequals')
            return {$ne : value};
        else if(type == 'normal')
            return value;
    }

    return controller;
};