var sanitize = require('mongo-sanitize');

module.exports = function(app) {
    var ModelLogradouro = app.models.logradouro;
    var ModelBairro = app.models.bairro;
    var ModelCidade = app.models.cidade;
    var ModelEstado = app.models.estado;
    var ModelPais = app.models.pais;
    var controller = {};

    controller.estado = function(req, res) {
        ModelEstado.find().limit(10000)
        .exec().then(function(register) {
            setInter(0, function(i) {
                var regs = register[i];
                if(regs != null) {
                    ModelPais.findOne({id:regs.pais_id})
                    .exec().then(function(reg) {
                        var alterar = {pais:reg._id, dtExclusao:null};
                        ModelEstado.findByIdAndUpdate(regs._id, alterar)
                        .exec().then(function(r) {
                            res.json({msg:r});
                        }, function(e) {
                            res.json({erro:e});
                        });
                    });
                }
            });
        },
        function(erro) {
            res.status(500).json(erro);
        });
    };

    controller.cidade = function(req, res) {
        ModelCidade.find().limit(10000)
        .exec().then(function(register) {
            setInter(0, function(i) {
                var regs = register[i];
                if(regs != null) {
                    ModelEstado.findOne({id:regs.estado_id})
                    .exec().then(function(reg) {
                        var alterar = {estado:reg._id, pais:reg.pais, dtExclusao:null};
                        ModelCidade.findByIdAndUpdate(regs._id, alterar)
                        .exec().then(function(r) {
                            res.json({msg:r});
                        }, function(e) {
                            res.json({erro:e});
                        });
                    });
                }
            });
        },
        function(erro) {
            res.status(500).json(erro);
        });
    };

    controller.bairro = function(req, res) {
        ModelBairro.find().limit(100000)
        .exec().then(function(register) {
            setInter(0, function(i) {
                var regs = register[i];
                if(regs != null) {
                    ModelCidade.findOne({id:regs.cidade_id})
                    .exec().then(function(reg) {
                        var alterar = {cidade:reg._id, estado:reg.estado, pais:reg.pais, dtExclusao:null};
                        ModelBairro.findByIdAndUpdate(regs._id, alterar)
                        .exec().then(function(r) {
                            res.json({msg:r});
                        }, function(e) {
                            res.json({erro:e});
                        });
                    });
                }
            });
        },
        function(erro) {
            res.status(500).json(erro);
        });
    };

    controller.logradouro = function(req, res) {
        ModelLogradouro.find().limit(250000).skip(250000)
        .exec().then(function(register) {
            setInter(0, function(i) {
                var regs = register[i];
                if(regs != null) {
                    ModelBairro.findOne({id:regs.bairro_id})
                    .exec().then(function(reg) {
                        var alterar = {bairro:reg._id, cidade:reg.cidade, estado:reg.estado, pais:reg.pais, dtExclusao:null};
                        ModelLogradouro.findByIdAndUpdate(regs._id, alterar)
                        .exec().then(function(r) {
                            res.json({msg:r});
                        }, function(e) {
                            res.json({erro:e});
                        });
                    });
                }
            });
        },
        function(erro) {
            res.status(500).json(erro);
        });
    };

    var setInter = function(t, cb) {
        var run = t;
        var interval = setInterval(function() {
            cb(run);
            run += 1;
        }, 10);
    };

    return controller;
};