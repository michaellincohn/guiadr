var path = require('path');
var easyimg = require('easyimage');
var fs = require('fs');
var mime = require('mime');
var im = require("imagemagick"); 

module.exports = function(app) {
    var Model = app.models.usuario;
    var controller = {}

    controller.upload = function(req, res) {
        var file = req.files.file;
        var uploadDir = './public/portal/_ups/temp/';
        var ext = mime.extension(file.type);

        if(ext == 'jpg' || ext == 'jpeg' || ext == 'png' || ext == 'gif') {
            fs.readFile(file.path, function (err, data) {
                var uploadPath = uploadDir + file.name;

                fs.writeFile(uploadPath, data, function (err, data) {
                    if (err) {
                        res.status(500).json();
                        console.log(err);
                    }

                    var dt = new Date();
                    var salt = dt.getTime();
                    var name = req.body.nameid + salt + '.' + ext;
                    var newname = uploadPath.replace(file.name, name);

                    fs.rename(uploadPath, newname, function(err) {
                        if (err) {
                            res.status(500).json();
                            console.log(err);
                        }
                    });

                    res.status(201).json({
                        file: newname.replace('./public/', '')
                    });
                });
            });
        } else {
            res.status(500).json();
        }
    };

    controller.resize = function(req, res) {
        var _module = req.params.module;
        var attrs = req.body;

        var file = attrs.file.replace('../', './public/');
        var filecopy = file.replace('/temp/', '/' + _module + '/');

        var coords = attrs.coords;
        var x = coords[0];
        var y = coords[1];
        var x2 = coords[2];
        var y2 = coords[3];
        var w = coords[4];
        var h = coords[5];

        var Cwidth = attrs.width;
        var Cheight = attrs.height;

        var args = [
            file,
            "-crop",
            w + "x" + h + "+" + x + "+" + y,
            filecopy
        ];

        var coord = {};
        coord.src = filecopy;
        coord.dst = filecopy;
        coord.width = Cwidth;
        coord.height = Cheight;

        im.convert(args, function(err) {
            if(err) return res.status(201).json({ mensage:'Ocorreu um erro ao editar a imagem!', file:null });
            easyimg.resize(coord).then(function(image) {
                fs.unlink(file, function (err) {
                    if (err) return console.log(err);
                    res.status(201).json({file: filecopy});
                });
            }, function (err) {
                return res.status(201).json({ mensage:'Ocorreu um erro ao editar a imagem!', file:null });
            });
        });
    }

    var randomIntInc = function(low, high) {
        return Math.floor(Math.random() * (high - low + 1) + low);
    }

    return controller;
};