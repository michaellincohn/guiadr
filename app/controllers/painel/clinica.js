var sanitize = require('mongo-sanitize');
var bCrypt = require('bcrypt-nodejs');

module.exports = function(app) {
    var Model = app.models.usuario;
    var controller = {}
    var perPage = 10;
    var totalItens = 0;

    controller.listAll = function(req, res) {
        var json = {};
        var page = Math.max(0, (req.query.page - 1));
        var conditions = {dtExclusao: null, tipo: 3};

        if(req.query.busca) {
            var busca = JSON.parse(req.query.busca);
            for (var i = 0; i < busca.length; i++) {
                conditions[busca[i].key] = getTypeFunction(busca[i].value, busca[i].type);
            }
        }

        Model.find(conditions)
        .limit(perPage)
        .skip(perPage * page)
        .sort({_id: 'asc'})
        .exec()
        .then(function(registers) {
            json.itens = registers;
            if(registers != null) {
                Model.count(conditions, function(e, total) {
                    json.allItens = total;
                    res.json(json);
                });
            }
        },
        function(erro) {
            res.status(500).json(erro);
        });
    };

    controller.getById = function(req, res) {
        var _id = sanitize(req.params.id);
        Model
        .findById(_id)
        .exec()
        .then(function(register) {
            if (!register) throw new Error("Registro não encontrado");
            res.json(register);
        },
        function(erro) {
            res.status(404).json(erro)
        });
    };

    controller.removeById = function(req, res) {
        var _id = sanitize(req.params.id);

        if (_id) {
            var dados = {
               "dtExclusao": Date.now()
            }

            Model
            .findByIdAndUpdate(_id, dados)
            .exec()
            .then(function(reg) {
                res.end();
            }, function(erro) {
                console.log(erro);
            });
        }
    };

    controller.save = function(req, res) {
        var _id = sanitize(req.body._id);
        var _image = sanitize(req.body._image);

        if(_image) {
            var dados = {
                "imagem": _image,
            }

            Model.findByIdAndUpdate(_id, dados)
            .exec()
            .then(function(register) {
                res.status(201).json({mensagem:"Registro alterado com sucesso!", class:"btn-primary", add:false, id:register._id});
            },
            function(erro) {
                res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false, id:0});
            });
        } else {
            var dados = {
                "nome": req.body.nome,
                "email": req.body.email,
                "password": createHash(req.body.password),
                "redesociais": {
                    "twitter": req.body.redesociais.twitter,
                    "linkedin": req.body.redesociais.linkedin,
                    "facebook": req.body.redesociais.facebook,
                },
                "info": {
                    "numdoc": req.body.info.numdoc,
                    "descricao": req.body.info.descricao
                }
            };

            if (_id) {
                if(req.body.password == '' || req.body.password == null) {
                    delete dados["password"];
                }

                Model.count({email: {$regex: getTypeFunction(req.body.email, 'match') }, dtExclusao: null, _id: {$ne:_id}}, function(q, t) {
                    if(t <= 0) {
                        Model
                        .findByIdAndUpdate(_id, dados)
                        .exec()
                        .then(function(register) {
                            res.status(201).json({mensagem:"Registro alterado com sucesso!", class:"btn-primary", add:false, id:register._id});
                        },
                        function(erro) {
                            res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false, id:0});
                        });
                    }
                    else {
                        res.status(201).json({mensagem:"Uma clínica foi encontrada com mesmo e-mail. Alteração não concluída!", class:"btn-danger", add:false});
                    }
                });
            } else {
                dados['tipo'] = 3; // Clinica

                Model.count({email: {$regex: getTypeFunction(req.body.email, 'match') }, dtExclusao: null }, function(q, t) {
                    if(t <= 0) {
                        Model
                        .create(dados)
                        .then(function(register) {
                            res.status(201).json({mensagem:"Registro inserido com sucesso!", class:"btn-primary", add:true, id:register._id});
                        },
                        function(erro) {
                            res.status(500).json({add:false, id:0});
                        });
                    }
                    else {
                        res.status(201).json({mensagem:"Uma clínica foi encontrada com mesmo e-mail. Cadastro não concluído!", class:"btn-danger", add:true});
                    }
                });
            }
        }
    };

    var getTypeFunction = function(value, type) {
        if(type == 'like')
            return new RegExp('^' + value, 'i');
        else if(type == 'match')
            return new RegExp('^' + value + '$', 'i');
        else if(type == 'equals')
            return {$e : value};
        else if(type == 'notequals')
            return {$ne : value};
    }
    
    var createHash = function(password) {
        if(password != '')
            return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    }

    return controller;
};