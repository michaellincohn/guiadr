var sanitize = require('mongo-sanitize');

module.exports = function(app) {
    var Model = app.models.blog;
    var controller = {}
    var perPage = 10;
    var totalItens = 0;

    controller.listAll = function(req, res) {
        var json = {};
        var page = Math.max(0, (req.query.page - 1));
        var conditions = {dtExclusao: null};

        if(req.query.busca) {
            var busca = JSON.parse(req.query.busca);
            for (var i = 0; i < busca.length; i++) {
                conditions[busca[i].key] = getTypeFunction(busca[i].value, busca[i].type);
            }
        }

        Model.find(conditions)
        .populate('editoria')
        .limit(perPage)
        .skip(perPage * page)
        .sort({_id: 'asc'})
        .exec()
        .then(function(registers) {
            json.itens = registers;
            if(registers != null) {
                Model.count(conditions, function(e, total) {
                    json.allItens = total;
                    res.json(json);
                });
            }
        },
        function(erro) {
            res.status(500).json(erro);
        });
    };

    controller.getById = function(req, res) {
        var _id = sanitize(req.params.id);
        Model.findById(_id)
        .exec()
        .then(function(register) {
            if (!register) throw new Error("Registro não encontrado");
            res.json(register);
        },
        function(erro) {
            res.status(404).json(erro)
        });
    };

    controller.removeById = function(req, res) {
        var _id = sanitize(req.params.id);

        if (_id) {
            var dados = {
               "dtExclusao": Date.now()
            }

            Model
            .findByIdAndUpdate(_id, dados)
            .exec()
            .then(function(reg) {
                res.end();
            }, function(erro) {
                console.log(erro);
            });
        }
    };

    controller.save = function(req, res) {
        var _id = sanitize(req.body._id);
        var _image = sanitize(req.body._image);

        if(_image) {
            var dados = {
                "imagem": _image,
            }

            Model.findByIdAndUpdate(_id, dados)
            .exec()
            .then(function(register) {
                res.status(201).json({mensagem:"Registro alterado com sucesso!", class:"btn-primary", add:false, id:register._id});
            },
            function(erro) {
                res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false, id:0});
            });
        } else {
            var dados = {
                "titulo": req.body.titulo,
                "editoria": req.body.editoria,
                "chamada": req.body.chamada,
                "conteudo": req.body.conteudo,
            };

            if (_id) {
                Model.count({titulo: {$regex: getTypeFunction(req.body.titulo, 'match') }, dtExclusao: null, _id: {$ne:_id}}, function(q, t) {
                    if(t <= 0) {
                        Model
                        .findByIdAndUpdate(_id, dados)
                        .exec()
                        .then(function(register) {
                            res.status(201).json({mensagem:"Registro alterado com sucesso!", class:"btn-primary", add:false, id:register._id});
                        },
                        function(erro) {
                            res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false, id:0});
                        });
                    } else {
                        res.status(201).json({mensagem:"Uma postagem foi encontrada com mesmo título. Alteração não concluída!", class:"btn-danger", add:false});
                    }
                });
            } else {
                dados['comentarios'] = [];
                dados['curtidas'] = 0;
                dados['visualizacoes'] = 0;

                Model.count({titulo: {$regex: getTypeFunction(req.body.titulo, 'match') }, dtExclusao: null }, function(q, t) {
                    if(t <= 0) {
                        Model
                        .create(dados)
                        .then(function(register) {
                            res.status(201).json({mensagem:"Registro inserido com sucesso!", class:"btn-primary", add:true, id:register._id});
                        },
                        function(erro) {
                            res.status(500).json({add:false, id:0});
                        });
                    } else {
                        res.status(201).json({mensagem:"Uma postagem foi encontrada com mesmo título. Cadastro não concluído!", class:"btn-danger", add:true});
                    }
                });
            }
        }
    };

    var getTypeFunction = function(value, type) {
        if(type == 'like')
            return new RegExp('^' + value, 'i');
        else if(type == 'match')
            return new RegExp('^' + value + '$', 'i');
        else if(type == 'equals')
            return {$e : value};
        else if(type == 'notequals')
            return {$ne : value};
    }

    return controller;
};