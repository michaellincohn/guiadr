var sanitize = require('mongo-sanitize');

module.exports = function(app) {
    var Model = app.models.disponibilidade;
    var controller = {}
    var perPage = 10;
    var totalItens = 0;

    controller.listAll = function(req, res) {
        var json = {};
        var page = Math.max(0, (req.query.page - 1));
        var conditions = {dtExclusao: null, medico: req.user._id};

        if(req.query.busca) {
            var busca = JSON.parse(req.query.busca);
            for (var i = 0; i < busca.length; i++) {
                conditions[busca[i].key] = getTypeFunction(busca[i].value, busca[i].type);
            }
        }

        Model.find(conditions)
        .populate('categoria._id')
        .limit(perPage)
        .skip(perPage * page)
        .sort({_id: 'asc'})
        .exec()
        .then(function(registers) {
            json.itens = registers;
            if(registers != null) {
                Model.count(conditions, function(e, total) {
                    json.allItens = total;
                    res.json(json);
                });
            }
        },
        function(erro) {
            res.status(500).json(erro);
        });
    };

    controller.getById = function(req, res) {
        var _id = sanitize(req.params.id);
        Model
        .findById(_id)
        .exec()
        .then(function(register) {
            if (!register) throw new Error("Registro não encontrado");
            res.json(register);
        },
        function(erro) {
            res.status(404).json(erro)
        });
    };

    controller.removeById = function(req, res) {
        var _id = sanitize(req.params.id);

        if (_id) {
            var dados = {
               "dtExclusao": Date.now()
            }

            Model
            .findByIdAndUpdate(_id, dados)
            .exec()
            .then(function(reg) {
                res.end();
            }, function(erro) {
                console.log(erro);
            });
        }
    };

    controller.save = function(req, res) {
        var _id = sanitize(req.body._id);

        var horario = {};
        var horarios = [];
        var categoria = {};
        var categorias = [];
        var especialidade = {};
        var especialidades = [];

        if(req.body.horario.length > 0) {
            for (var i = 0; i < req.body.horario.length; i++) {
                horario = {};
                horario['status'] = 1;
                horario['hora'] = req.body.horario[i].hora;
                horarios.push(horario);
            }
        }

        if(req.body.categoria.especialidade.length > 0) {
            for (var i = 0; i < req.body.categoria.especialidade.length; i++) {
                especialidade = {};
                especialidade['_id'] = req.body.categoria.especialidade[i]._id;
                especialidade['titulo'] = req.body.categoria.especialidade[i].titulo;
                especialidades.push(especialidade);
            }
        }

        var dtFrom = req.body.data.split('/');
        var dados = {
            data: new Date(dtFrom[2], dtFrom[1] - 1, dtFrom[0]),
            medico: req.user.id,
            tipo: parseInt(req.body.tipo),
            horario: horarios,
            categoria: req.body.categoria
        };

        if (_id) {
            Model
            .findByIdAndUpdate(_id, dados)
            .exec()
            .then(function(register) {
                res.status(201).json({mensagem:"Registro alterado com sucesso!", class:"btn-primary", add:false});
            }, function(erro) {
                res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false});
            });
        } else {
            Model
            .create(dados)
            .then(function(register) {
                res.status(201).json({mensagem:"Registro inserido com sucesso!", class:"btn-primary", add:true});
            }, function(erro) {
                res.status(500).json({add:false});
            });
        }
    };

    var getTypeFunction = function(value, type) {
        if(type == 'like')
            return new RegExp('^' + value, '');
        else if(type == 'match')
            return new RegExp('^' + value + '$', '');
        else if(type == 'equals')
            return {$e : value};
        else if(type == 'notequals')
            return {$ne : value};
    }

    return controller;
};