var sanitize = require('mongo-sanitize');

module.exports = function(app) {
    var controller = {}

    controller.home = function(req, res) {
        res.render('restrito-profissional', {
            "usuarioLogado": (req.user ? req.user.nome : ''),
            "tipoLogado": (req.user ? req.user.tipo : 0)
        });
    };

    return controller;
};