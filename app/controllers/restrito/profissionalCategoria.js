var sanitize = require('mongo-sanitize');
var bCrypt = require('bcrypt-nodejs');

module.exports = function(app) {
    var Model = app.models.usuario;
    var controller = {}

    controller.getById = function(req, res) {
        var _id = sanitize(req.user._id);
        Model.findById(_id).exec()
        .then(function(register) {
            if (!register) throw new Error("Registro não encontrado");
            res.json(register);
        },
        function(erro) {
            res.status(404).json(erro)
        });
    };

    controller.removeById = function(req, res) {
        var _id = sanitize(req.params.id);

        if (_id) {
            Model.update(
                { 'categoria._id': _id },
                { $pull: { 'categoria': { '_id': _id } } }
            )
            .exec()
            .then(function (register) {
                res.status(201).json({mensagem:"Registro alterado com sucesso!", class:"btn-info", add:false, id:0});
            }, function(erro) {
                res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false, id:0});
            });
        }
    };

    controller.save = function(req, res) {
        var _id = sanitize(req.user._id);

        var habilidade = {};
        var habilidades = [];
        var categoria = {};
        var categorias = [];
        var especialidade = {};
        var especialidades = [];

        if(req.body.categoria.habilidade) {
            for (var i = 0; i < req.body.categoria.habilidade.length; i++) {
                habilidade = {};
                habilidade['titulo'] = req.body.categoria.habilidade[i].titulo;
                habilidades.push(habilidade);
            }
        }

        if(req.body.especialidade.length > 0) {
            for (var i = 0; i < req.body.especialidade.length; i++) {
                especialidade = {};
                especialidade['_id'] = req.body.especialidade[i].id;
                especialidade['titulo'] = req.body.especialidade[i].titulo;
                especialidades.push(especialidade);
            }
        }

        var categoriaId = req.body.categoria._id;
        categoria['catId'] = req.body.categoria._id;
        categoria['titulo'] = req.body.categoria.titulo;
        categoria['habilidade'] = habilidades;
        categoria['especialidade'] = especialidades;
        categorias.push(categoria);

        if (_id) {
            Model.update(
                { '_id': _id },
                { $pushAll: { 'categoria': categorias } }
            )
            .exec()
            .then(function (register) {
                res.status(201).json({mensagem:"Salvar", class:"btn-info", add:false, id:categoriaId});
            }, function(erro) {
                res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false, id:0});
            });
        }
        else
        {
            res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false, id:0});
        }
    };

    return controller;
};