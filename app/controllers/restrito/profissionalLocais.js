var sanitize = require('mongo-sanitize');
var bCrypt = require('bcrypt-nodejs');

module.exports = function(app) {
    var Model = app.models.usuario;
    var controller = {}

    controller.getById = function(req, res) {
        var _id = sanitize(req.user._id);
        Model.findById(_id).exec()
        .then(function(register) {
            if (!register) throw new Error("Registro não encontrado");
            res.json(register);
        },
        function(erro) {
            res.status(404).json(erro);
        });
    };

    controller.removeById = function(req, res) {
        var _id = sanitize(req.params.id);

        if (_id) {
            Model.update(
                { 'endereco._id': _id },
                { $pull: { 'endereco': { '_id': _id } } }
            )
            .exec()
            .then(function (register) {
                res.status(201).json({mensagem:"Registro alterado com sucesso!", class:"btn-info", add:false, id:register._id});
            }, function(erro) {
                res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false, id:0});
            });
        }
    };

    controller.save = function(req, res) {
        var _id = sanitize(req.user._id);

        var endereco = {};
        var enderecos = [];

        var telefones = [];
        if(req.body.telefone) {
            for (var i = 0; i < req.body.telefone.length; i++) {
                var telefone = {};
                telefone['numero'] = req.body.telefone[i].titulo;
                telefones.push(telefone);
            }
        }

        endereco['numero'] = req.body.numero;
        endereco['logradouro'] = req.body.logradouro;
        endereco['bairro'] = req.body.bairro;
        endereco['cidade'] = req.body.cidade;
        endereco['estado'] = req.body.estado;
        endereco['pais'] = req.body.pais;
        endereco['latlong'] = req.body.latlong;
        endereco['telefone'] = telefones;
        enderecos.push(endereco);

        if (_id) {
            Model.update(
                { '_id': _id },
                { $pushAll: { 'endereco': enderecos } }
            )
            .exec()
            .then(function (register) {
                res.status(201).json({mensagem:"Salvar", class:"btn-info", add:false, id:0});
            }, function(erro) {
                res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false, id:0});
            });
        }
        else
        {
            res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false, id:0});
        }
    };

    return controller;
};