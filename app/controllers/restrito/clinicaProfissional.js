var sanitize = require('mongo-sanitize');
var bCrypt = require('bcrypt-nodejs');

module.exports = function(app) {
    var Model = app.models.usuario;
    var controller = {}

    controller.listAll = function(req, res) {
        var conditions = {dtExclusao: null, tipo: 2};

        if(req.query.busca) {
            var busca = JSON.parse(req.query.busca);
            for (var i = 0; i < busca.length; i++) {
                conditions[busca[i].key] = getTypeFunction(busca[i].value, busca[i].type);
            }
        }

        Model.find(conditions)
        .sort({nome: 1})
        .exec()
        .then(function(registers) {
            res.json(registers);
        },
        function(erro) {
            res.status(500).json(erro);
        });
    };

    controller.getById = function(req, res) {
        var _id = sanitize(req.params.id);
        Model
        .findById(_id)
        .populate('referencia')
        .exec()
        .then(function(register) {
            if (!register) throw new Error("Registro não encontrado");
            res.json(register);
        },
        function(erro) {
            res.status(404).json(erro)
        });
    };

    controller.removeById = function(req, res) {
        var _id = sanitize(req.params.id);

        if (_id) {
            Model.update(
                { 'referencia': _id },
                { $pull: { 'referencia': _id } }
            )
            .exec()
            .then(function (register) {
                res.status(201).json({mensagem:"Registro alterado com sucesso!", class:"btn-primary", add:false, id:register._id});
            }, function(erro) {
                res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde.", class:"btn-danger", add:false, id:0});
            });
        }
    };

    controller.save = function(req, res) {
        var _id = sanitize(req.body._id);

        var referencia = {};
        var referencias = [];

        referencia = req.body.referencia;
        referencias.push(referencia);

        if (_id) {
            Model.update(
                { '_id': _id },
                { $pushAll: { 'referencia': referencias } }
            )
            .exec()
            .then(function (register) {
                res.status(201).json({mensagem:"Salvar", class:"btn-info", add:false, id:0});
            }, function(erro) {
                console.log(erro);
                res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde.", class:"btn-danger", add:false, id:0});
            });
        }
        else
        {
            res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde.", class:"btn-danger", add:false, id:0});
        }
    };

    var getTypeFunction = function(value, type) {
        if(type == 'like')
            return new RegExp('^' + value, 'i');
        else if(type == 'match')
            return new RegExp('^' + value + '$', 'i');
        else if(type == 'equals')
            return {$e : value};
        else if(type == 'notequals')
            return {$ne : value};
    }

    return controller;
};