var sanitize = require('mongo-sanitize');

module.exports = function(app) {
    var Model = app.models.categoria;
    var controller = {}
    var perPage = 10;
    var totalItens = 0;

    controller.listAll = function(req, res) {
        var json = {};
        var page = Math.max(0, (req.query.page - 1));
        var conditions = {dtExclusao: null};

        if(req.query.busca) {
            var busca = JSON.parse(req.query.busca);
            for (var i = 0; i < busca.length; i++) {
                conditions[busca[i].key] = getTypeFunction(busca[i].value, busca[i].type);
            }
        }

        Model.find(conditions)
        .limit(perPage)
        .skip(perPage * page)
        .sort({_id: 'asc'})
        .exec()
        .then(function(registers) {
            json.itens = registers;
            if(registers != null) {
                Model.count(conditions, function(e, total) {
                    json.allItens = total;
                    res.json(json);
                });
            }
        },
        function(erro) {
            res.status(500).json(erro);
        });
    };

    controller.getById = function(req, res) {
        var _id = sanitize(req.params.id);
        Model
        .findById(_id)
        .exec()
        .then(function(register) {
            if (!register) throw new Error("Registro não encontrado");
            res.json(register);
        },
        function(erro) {
            res.status(404).json(erro)
        });
    };

    controller.removeById = function(req, res) {
        var _id = sanitize(req.params.id);

        if (_id) {
            var dados = {
               "dtExclusao": Date.now()
            }

            Model
            .findByIdAndUpdate(_id, dados)
            .exec()
            .then(function(reg) {
                res.end();
            }, function(erro) {
                console.log(erro);
            });
        }
    };

    controller.save = function(req, res) {
        var _id = sanitize(req.body._id);
        var dados = {
            "titulo": req.body.titulo
        }

        var especialidades = [];
        for (var i = 0; i < req.body.especialidade.length; i++) {
            var especialidade = {};
            especialidade['titulo'] = req.body.especialidade[i].text;
            especialidades.push(especialidade);
        }
        dados.especialidade = especialidades;

        if (_id) {
            Model.count({ titulo: {$regex: getTypeFunction(req.body.titulo, 'match') }, dtExclusao: null, _id: {$ne:_id}}, function(q, t) {
                if(t <= 0) {
                    Model
                    .findByIdAndUpdate(_id, dados)
                    .exec()
                    .then(function(register) {
                        res.status(201).json({mensagem:"Registro alterado com sucesso!", class:"btn-primary", add:false});
                    },
                    function(erro) {
                        res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false});
                    });
                }
                else {
                    res.status(201).json({mensagem:"Um registro foi encontrado com mesmo título", class:"btn-danger", add:false});
                }
            });
        } else {
            Model.count({ titulo: {$regex: getTypeFunction(req.body.titulo, 'match') }, dtExclusao: null }, function(q, t) {
                if(t <= 0) {
                    Model
                    .create(dados)
                    .then(function(register) {
                        res.status(201).json({mensagem:"Registro inserido com sucesso!", class:"btn-primary", add:true});
                    },
                    function(erro) {
                        res.status(500).json({add:false});
                    });
                }
                else {
                    res.status(201).json({mensagem:"Já existe um registro com este mesmo título", class:"btn-danger", add:true});
                }
            });
        }
    };

    controller.default = function(req, res) {
        var dados = {};
        var category = {};
        var categories = [];

        category = {
            'titulo': 'Medicina',
            'especialidade': [
                { titulo: 'Acupuntura' },
                { titulo: 'Alergia e Imunologia' },
                { titulo: 'Anestesiologia' },
                { titulo: 'Angiologia' },
                { titulo: 'Cardiologia' },
                { titulo: 'Cirurgia Cardiovascular' },
                { titulo: 'Cirurgia da Mão' },
                { titulo: 'Cirurgia da Cabeça e Pescoço' },
                { titulo: 'Cirurgia do Aparelho Digestivo' },
                { titulo: 'Cirurgia Geral' },
                { titulo: 'Cirurgia Pediátrica' },
                { titulo: 'Cirurgia Plástica' },
                { titulo: 'Cirurgia Torácica' },
                { titulo: 'Cirurgia Vascular' },
                { titulo: 'Clínica Médica' },
                { titulo: 'Coloproctologia' },
                { titulo: 'Dermatologia' },
                { titulo: 'Endocrinologia' },
                { titulo: 'Endoscopia' },
                { titulo: 'Gastroenterologia' },
                { titulo: 'Genética Médica' },
                { titulo: 'Geriatria' },
                { titulo: 'Ginecologia/Obstetrícia' },
                { titulo: 'Hematologia/Hemoterapia' },
                { titulo: 'Homeopatia' },
                { titulo: 'Infectologia' },
                { titulo: 'Mastologia' },
                { titulo: 'Medicina de Família e Comunidade' },
                { titulo: 'Medicina do trabalho' },
                { titulo: 'Medicina do Tráfego' },
                { titulo: 'Medicina Esportiva' },
                { titulo: 'Medicina Física e Reabilitação' },
                { titulo: 'Medicina Intensiva' },
                { titulo: 'Medicina Legal' },
                { titulo: 'Medicina Nuclear' },
                { titulo: 'Nefrologia' },
                { titulo: 'Neurocirurgia' },
                { titulo: 'Neurologia' },
                { titulo: 'Nutrologia' },
                { titulo: 'Oftalmologia' },
                { titulo: 'Oncologia' },
                { titulo: 'Ortopedia/Traumatologia' },
                { titulo: 'Otorrinolaringologia' },
                { titulo: 'Patologia' },
                { titulo: 'Pediatria' },
                { titulo: 'Neonatologia' },
                { titulo: 'Pneumologia' },
                { titulo: 'Psiquiatria' },
                { titulo: 'Radiologia/Diagnóstico por Imagem' },
                { titulo: 'Radioterapia' },
                { titulo: 'Reumatologia' },
                { titulo: 'Urologia' },
            ]
        };
        categories.push(category);

        category = {
            'titulo': 'Odontologia',
            'especialidade': [
                { titulo: 'Cirurgia e Traumatologia Buco-Maxilo-Facial' },
                { titulo: 'Dentística' },
                { titulo: 'Dentística Restauradora' },
                { titulo: 'Disfunção Têmporo-Mandibular e Dor-Orofacial' },
                { titulo: 'Endodontia' },
                { titulo: 'Estomatologia' },
                { titulo: 'Implantodontia' },
                { titulo: 'Odontologia Legal' },
                { titulo: 'Odontogeriatria' },
                { titulo: 'Odontopediatria' },
                { titulo: 'Odontologia do Trabalho' },
                { titulo: 'Odontologia para Pacientes com Necessidades Especiais' },
                { titulo: 'Ortodontia' },
                { titulo: 'Ortopedia Funcional dos Maxilares' },
                { titulo: 'Patologia Bucal' },
                { titulo: 'Periodontia' },
                { titulo: 'Prótese Buco-Maxilo-Facial' },
                { titulo: 'Prótese Dentária' },
                { titulo: 'Radiologia Odontológica e Imaginologia' },
                { titulo: 'Saúde Coletiva' },
            ]
        };
        categories.push(category);

        category = {
            'titulo': 'Fisioterapia',
            'especialidade': [
                { titulo: 'Acupuntura' },
                { titulo: 'Aquática' },
                { titulo: 'Cardiovascular' },
                { titulo: 'Dermatofuncional' },
                { titulo: 'Esportiva' },
                { titulo: 'Do Trabalho' },
                { titulo: 'Neurofuncional' },
                { titulo: 'Oncologia' },
                { titulo: 'Respiratória' },
                { titulo: 'Traumato-Ortopedia' },
                { titulo: 'Osteopatia' },
                { titulo: 'Quiropraxia' },
                { titulo: 'Saúde da Mulher' },
                { titulo: 'Terapia Intensiva' },
            ]
        };
        categories.push(category);

        category = {
            'titulo': 'Enfermagem',
            'especialidade': [
                { titulo: 'Aeroespacial' },
                { titulo: 'Auditoria e Pesquisa' },            
                { titulo: 'Cardiologia' },
                { titulo: 'Centro Cirúrgico' },
                { titulo: 'Dermatológica' },
                { titulo: 'Diagnóstico por Imagens' },
                { titulo: 'Doenças infecciosas e parasitarias' },
                { titulo: 'Educação em Enfermagem' },
                { titulo: 'Endocrinologia' },
                { titulo: 'Farmacologia' },
                { titulo: 'Gerenciamento/Gestão' },
                { titulo: 'Hanseníase' },
                { titulo: 'Hematologia e Hemoterapia' },
                { titulo: 'Hemoterapia' },
                { titulo: 'Infecção Hospitalar' },
                { titulo: 'Informática em Saúde' },
                { titulo: 'Legislação' },
                { titulo: 'Neurologia' },
                { titulo: 'Nutrição Parenteral e Enteral' },
                { titulo: 'Oftalmologia' },
                { titulo: 'Oncologia' },
                { titulo: 'Otorrinolaringologia' },
                { titulo: 'Pneumologia Sanitária' },
                { titulo: 'Políticas Públicas' },
                { titulo: 'Saúde Complementar' },
                { titulo: 'Saúde da Criança e do Adolescente' },
                { titulo: 'Saúde da Familia' },
                { titulo: 'Saúde da Mulher' },
                { titulo: 'Saúde do Adulto' },
                { titulo: 'Saúde do Homem' },
                { titulo: 'Saúde do Idoso' },
                { titulo: 'Saúde Mental' },
                { titulo: 'Saúde Pública' },
                { titulo: 'Saúde do Trabalhador' },
                { titulo: 'Saúde Indígena' },
                { titulo: 'Sexologia Humana' },
                { titulo: 'Terapias Holísticas Complementares' },
                { titulo: 'Terapia Intensiva' },
                { titulo: 'Transplantes' },
                { titulo: 'Traumato-Ortopedia' },
                { titulo: 'Urgência e Emergência' },
                { titulo: 'Enfermagem em Vigilância' },
                { titulo: 'Enfermagem offshore e aquaviária' },
            ]
        };
        categories.push(category);

        category = {
            'titulo': 'Nutrição',
            'especialidade': [
                { titulo: 'Alimentação Coletiva' },
                { titulo: 'Nutrição Clínica' },
                { titulo: 'Saúde Coletiva' },
                { titulo: 'Nutrição em Esportes' },
            ]
        };
        categories.push(category);

        category = {
            'titulo': 'Psicologia',
            'especialidade': [
                { titulo: 'Clínica' },
                { titulo: 'Escolar/Educacional' },
                { titulo: 'Esporte' },
                { titulo: 'Hospitalar' },
                { titulo: 'Jurídica' },
                { titulo: 'Neuropsicologia' },
                { titulo: 'Organizacional e do Trabalho' },
                { titulo: 'Psicopedagogia' },
                { titulo: 'Psicomotricidade' },
                { titulo: 'Psicologia Social' },
                { titulo: 'Trânsito' },
            ]
        };
        categories.push(category);

        category = {
            'titulo': 'Educação Física',
            'especialidade': []
        };
        categories.push(category);

        category = {
            'titulo': 'Fonoaudiologia',
            'especialidade': [
                { titulo: 'Audiologia' },
                { titulo: 'Disfagia' },
                { titulo: 'Educacional' },
                { titulo: 'Gerontologia' },
                { titulo: 'Linguagem' },
                { titulo: 'Motricidade Orofacial' },
                { titulo: 'Neurofuncional' },
                { titulo: 'Neuropsicologia' },
                { titulo: 'Saúde Coletiva' },
                { titulo: 'Trabalho' },
                { titulo: 'Voz' }
            ]
        };
        categories.push(category);

        dados = categories;
        Model.create(dados).then(function(register) {
            res.status(201).json({mensagem:"Registro inserido com sucesso!", class:"btn-primary", add:true});
        });
    };

    var getTypeFunction = function(value, type) {
        if(type == 'like')
            return new RegExp('^' + value, 'i');
        else if(type == 'match')
            return new RegExp('^' + value + '$', 'i');
        else if(type == 'equals')
            return {$e : value};
        else if(type == 'notequals')
            return {$ne : value};
    }

    return controller;
};