var sanitize = require('mongo-sanitize');

module.exports = function(app) {
    var Model = app.models.usuario;
    var ModelConsulta = app.models.consulta;
    var controller = {}
    var perPage = 10;
    var totalItens = 0;

    controller.listAll = function(req, res) {
        var json = {};
        var page = Math.max(0, (req.query.page - 1));
        var conditions = {dtExclusao: null, tipo: 4};

        if(req.query.busca) {
            var busca = JSON.parse(req.query.busca);
            for (var i = 0; i < busca.length; i++) {
                conditions[busca[i].key] = getTypeFunction(busca[i].value, busca[i].type);
            }
        }

        Model.find(conditions)
        .limit(perPage)
        .skip(perPage * page)
        .sort({_id: 'asc'})
        .exec()
        .then(function(registers) {
            json.itens = registers;
            if(registers != null) {
                Model.count(conditions, function(e, total) {
                    json.allItens = total;
                    res.json(json);
                });
            }
        },
        function(erro) {
            res.status(500).json(erro);
        });
    };

    controller.getById = function(req, res) {
        var _id = sanitize(req.params.id);
        Model
        .findById(_id)
        .exec()
        .then(function(register) {
            if (!register) throw new Error("Registro não encontrado");
            res.json(register);
        },
        function(erro) {
            res.status(404).json(erro)
        });
    };

    controller.removeById = function(req, res) {
        var _id = sanitize(req.params.id);

        if (_id) {
            var dados = {
               "dtExclusao": Date.now()
            }

            Model
            .findByIdAndUpdate(_id, dados)
            .exec()
            .then(function(reg) {
                res.end();
            }, function(erro) {
                console.log(erro);
            });
        }
    };

    controller.default = function(req, res) {
        var dados = {};
        var category = {};
        var categories = [];

        category = {
            'nome': 'Paciente 1',
            'email': 'paciente@guiadr.com.br',
            'documento': '123.456.789-00',
            'tipo': 4,
            'imagem': '',
            'endereco': [{
                'numero': 009,
                'logradouro': 'rua do paciente',
                'bairro': 'bairro do paciente',
                'cidade': 'cidade do paciente',
                'pais': 'pais do paciente',
                'latlong': '-22.9426268|-45.4664531',
                'telefone': [{
                    'numero': '214215235'
                }]
            }]
        };
        categories.push(category);

        dados = categories;
        Model.create(dados).then(function(register) {
            var consulta = {};
            consulta.paciente =  register[0]._id;
            consulta.medico = register[0]._id;
            consulta.especialidade = 'Otorrinolaringologista';
            consulta.status = 1;
            consulta.dtNascimento = '1982-07-27 23:40:00.000Z';
            consulta.dtAgendamento = '2016-03-06 19:30:00.000Z';

            ModelConsulta.collection.insert(consulta, function(err, list) {
                if (err) {
                    throw err;
                }

                console.log("\nlist:", list);
                res.status(201).json({mensagem:"Registro inserido com sucesso!", class:"btn-primary", add:true});
            });
        });
    };

    var getTypeFunction = function(value, type) {
        if(type == 'like')
            return new RegExp('^' + value, 'i');
        else if(type == 'match')
            return new RegExp('^' + value + '$', 'i');
        else if(type == 'equals')
            return {$e : value};
        else if(type == 'notequals')
            return {$ne : value};
    }
    
    return controller;
};