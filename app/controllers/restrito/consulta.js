var sanitize = require('mongo-sanitize');

module.exports = function(app) {
    var Model = app.models.consulta;
    var controller = {}
    var perPage = 10;
    var totalItens = 0;

    controller.listAll = function(req, res) {
        var json = {};
        var page = Math.max(0, (req.query.page - 1));
        var conditions = {paciente: req.query.paciente};

        Model.find(conditions)
        .populate('medico')
        .limit(perPage)
        .skip(perPage * page)
        .sort({_id: 'asc'})
        .exec()
        .then(function(registers) {
            json.itens = registers;
            if(registers != null) {
                Model.count(conditions, function(e, total) {
                    json.allItens = total;
                    res.json(json);
                });
            }
        },
        function(erro) {
            res.status(500).json(erro);
        });
    };

    controller.getById = function(req, res) {
        var _id = sanitize(req.params.id);
        Model
        .findById(_id)
        .exec()
        .then(function(register) {
            if (!register) throw new Error("Registro não encontrado");
            res.json(register);
        },
        function(erro) {
            res.status(404).json(erro)
        });
    };

    controller.status = function(req, res) {
        var _data = req.body.data;
        var _id = sanitize(_data._id);
        var setStatus = _data.status;

        switch(_data.status) {
            case 1: setStatus = 2; break;
            case 2: setStatus = 3; break;
            case 3: setStatus = 4; break;
            case 4: setStatus = 5; break;
            case 5: setStatus = 1; break;
        };

        if (_id) {
            Model
            .findByIdAndUpdate(_id, { status: setStatus })
            .exec()
            .then(function(register) {
                res.status(201).json({mensagem:"Registro alterado com sucesso!", class:"btn-primary", status:setStatus});
            },
            function(erro) {
                res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false});
            });
        }
    };

    var getTypeFunction = function(value, type) {
        if(type == 'like')
            return new RegExp('^' + value, 'i');
        else if(type == 'match')
            return new RegExp('^' + value + '$', 'i');
        else if(type == 'equals')
            return {$e : value};
        else if(type == 'notequals')
            return {$ne : value};
    };

    return controller;
};