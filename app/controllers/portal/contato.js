var sanitize = require('mongo-sanitize');
var common = require('../../../config/common');
var template = require('../../../config/template');

module.exports = function(app) {
    var Model = app.models.contato;
    var controller = {}

    controller.save = function(req, res) {
        //var _id = sanitize(req.body._id);
        var dados = {
            "nome": req.body.nome,
            "email": sanitize(req.body.email),
            "mensagem": req.body.mensagem
        }

        Model.create(dados)
        .then(function(register) {
            common.sendMail(dados.email, 'Contato', template.contato(dados), function(error, info) {
                if(error) {
                    res.status(201).json({message:"Contato não enviado! Tente mais tarde.", class:"btn-danger"});
                } else {
                    res.status(201).json({message:"Contato enviado com sucesso!", class:"btn-primary"});
                }
                console.log(info);
            });
        }, function(erro) {
            res.status(500).json({erro:erro});
        });
    };

    return controller;
};