var sanitize = require('mongo-sanitize');
var bCrypt = require('bcrypt-nodejs');
var common = require('../../../config/common');
var template = require('../../../config/template');

module.exports = function(app) {
    var Model = app.models.usuario;
    var ModelEstado = app.models.estado;
    var controller = {};
    var perPage = 10;
    var totalItens = 0;

    controller.listAll = function(req, res) {
        var page = Math.max(0, (req.query.page - 1));
        var conditions = {dtExclusao: null, tipo: 2, imagem: { $ne: null } };
        var populateQuery = [
            {path:'endereco.pais', select:'titulo'},
            {path:'endereco.estado', select:'titulo'},
            {path:'endereco.cidade', select:'titulo'},
            {path:'endereco.bairro', select:'titulo'},
            {path:'endereco.logradouro', select:'titulo'}
        ];

        Model
        .find(conditions)
        .limit(perPage)
        .skip(perPage * page)
        .sort({ _id: 'desc'})
        .exec(function(err, register) {
            Model.populate(register, populateQuery, function(err, docs) {
                if (err) res.status(500).json("Registro não encontrado");
                res.status(201).json(register);
            });
        });
        //.then(function(registers) {
            //res.json(registers);
        //});
    }; 

    controller.getById = function(req, res) {
        var _id = sanitize(req.params.id);

        var populateQuery = [
            {path:'endereco.pais', select:'titulo'},
            {path:'endereco.estado', select:'titulo'},
            {path:'endereco.cidade', select:'titulo'},
            {path:'endereco.bairro', select:'titulo'},
            {path:'endereco.logradouro', select:'titulo cep'}
        ];

        Model
        .findById(_id)
        .populate('endereco')
        .exec(function(err, register) {
            Model.populate(register, populateQuery, function(err, docs) {
                if (err) res.status(500).json("Registro não encontrado");
                console.log(register.endereco);
                res.status(201).json(register);
            });
        },
        //.then(function(register) {
            //if (!register) throw new Error("Registro não encontrado");
            //res.json(register);
        //},
        function(erro) {
            res.status(404).json(erro)
        });
    };

    controller.save = function(req, res) {
        var _id = sanitize(req.body._id);
        var step = sanitize(req.body.step);

        if(step == 1) { // Dados basicos
            var nome = sanitize(req.body.nome);
            var email = sanitize(req.body.email);
            var senha = sanitize(req.body.senha);
            var numreg = sanitize(req.body.numreg);
            var facebook = sanitize(req.body.facebook);
            var twitter = sanitize(req.body.twitter);
            var linkedin = sanitize(req.body.linkedin);
            var documento = sanitize(req.body.documento);
            var descricao = sanitize(req.body.descricao);
            var educacao = sanitize(req.body.educacao);

            var dados = {
                "nome": nome,
                "email": email,
                "password": createHash(senha),
                "tipo": 2,
                "plano": null,
                "info": {
                    "descricao": descricao,
                    "educacao": educacao,
                    "numreg": numreg,
                    "documento": documento
                },
                "redesociais": {
                    "linkedin": linkedin,
                    "facebook": facebook,
                    "twitter": twitter
                }
            }

            Model.count({ email: {$regex: getTypeFunction(email, 'match') }, dtExclusao: null, tipo: 2 }, function(q, t) {
                if(t <= 0) {
                    Model.create(dados).then(function(register) {
                        res.status(201).json(register);
                    },
                    function(erro) {
                        res.status(500).json({mensagem:"Tente mais tarde!"});
                    });
                }
                else {
                    res.status(201).json(dados);
                }
            });
        } else if(step == 2) { // Categoria
            var habilidade = {};
            var habilidades = [];
            var categoria = {};
            var categorias = [];
            var especialidade = {};
            var especialidades = [];

            if(req.body.categoria.habilidade != null) {
                for (var i = 0; i < req.body.categoria.habilidade.length; i++) {
                    habilidade = {};
                    habilidade['titulo'] = req.body.categoria.habilidade[i].titulo;
                    habilidades.push(habilidade);
                }
            }

            if(req.body.especialidade.length > 0) {
                for (var i = 0; i < req.body.especialidade.length; i++) {
                    especialidade = {};
                    especialidade['_id'] = req.body.especialidade[i].id;
                    especialidade['titulo'] = req.body.especialidade[i].titulo;
                    especialidades.push(especialidade);
                }
            }

            var categoriaId = req.body.categoria._id;
            categoria['_id'] = req.body.categoria._id;
            categoria['titulo'] = req.body.categoria.titulo;
            categoria['habilidade'] = habilidades;
            categoria['especialidade'] = especialidades;
            categorias.push(categoria);

            Model.update(
                { '_id': _id },
                { $pushAll: { 'categoria': categorias } }
            )
            .exec()
            .then(function (register) {
                res.status(201).json(register);
            }, function(erro) {
                res.status(500).json({mensagem:"Tente mais tarde!"});
            });
        } else if(step == 3) { // Enderecos
            var endereco = {};
            var enderecos = [];

            var telefones = [];
            if(req.body.telefone != null) {
                for (var i = 0; i < req.body.telefone.length; i++) {
                    var telefone = {};
                    telefone['numero'] = req.body.telefone[i].titulo;
                    telefones.push(telefone);
                }
            }

            endereco['numero'] = req.body.numero;
            endereco['logradouro'] = req.body.logradouro;
            endereco['bairro'] = req.body.bairro;
            endereco['cidade'] = req.body.cidade;
            endereco['estado'] = req.body.estado;
            endereco['pais'] = req.body.pais;
            endereco['latlong'] = req.body.latlong;
            endereco['telefone'] = telefones;
            enderecos.push(endereco);
            
            Model.update(
                { '_id': _id },
                { $pushAll: { 'endereco': enderecos } }
            )
            .exec()
            .then(function (register) {
                res.status(201).json({mensagem:"Salvar", class:"btn-info", add:false, id:categoriaId});
            }, function(erro) {
                res.status(201).json({mensagem:"Ocorreu um problema! Tente novamente mais tarde", class:"btn-danger", add:false, id:0});
            });

        } else if(step == 4) { // Plano
            var dados = {
                'plano': {
                    'tipo': req.body.tipo,
                    'titulo': req.body.titulo,
                    'valor': req.body.valor
                }
            }

            Model.findByIdAndUpdate(_id, dados)
            .exec()
            .then(function(register) {
                common.sendMail(register.email, 'Cadastro de Profissional', template.cadastroProfissional(register));
                res.status(201).json(register);
            },
            function(erro) {
                res.status(500).json({mensagem:"Tente mais tarde!"});
            });
        }
    };

    var isValidPassword = function(user, password) {
        return bCrypt.compareSync(password, user.password);
    };

    var createHash = function(password) {
        if(password != '')
            return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    };

    var getTypeFunction = function(value, type) {
        if(type == 'like')
            return new RegExp('^' + value, 'i');
        else if(type == 'match')
            return new RegExp('^' + value + '$', 'i');
        else if(type == 'equals')
            return {$e : value};
        else if(type == 'notequals')
            return {$ne : value};
    };

    return controller;
};