var sanitize = require('mongo-sanitize');
var bCrypt = require('bcrypt-nodejs');

module.exports = function(app) {
    var Model = app.models.usuario;
    var controller = {};

    controller.login = function(req, res) {
        var email = sanitize(req.query.email);
        var password = req.query.password;

        Model.findOne({ email: email, tipo: 4 }, function(err, usuario) {
            if(err) {
                res.status(404).json(erro)
            } else {
                if (usuario == null) {
                    res.status(404).json('Usuário não encontrado');
                } else {
                    if (!isValidPassword(usuario, password)) {
                        res.status(404).json('Senha inválida');
                    } else {
                        res.json(usuario);
                    }
                }
            }
        });
    };

    controller.save = function(req, res) {
        var nome = sanitize(req.body.nome);
        var email = sanitize(req.body.email);
        var senha = sanitize(req.body.senha);
        var termo = parseInt(req.body.termos);

        var dados = {
            "nome": nome,
            "email": email,
            "password": createHash(senha),
            "aceitoTermos": termo,
            "tipo": 4
        }

        Model.count({ email: {$regex: getTypeFunction(email, 'match') }, dtExclusao: null, tipo: 4 }, function(q, t) {
            if(t <= 0) {
                Model.create(dados).then(function(register) {
                    res.status(201).json(register);
                },
                function(erro) {
                    res.status(500).json({mensagem:"Tente mais tarde!"});
                });
            }
            else {
                res.status(500).json({mensagem:"Utilize outro email!"});
            }
        });
    };

    var isValidPassword = function(user, password) {
        return bCrypt.compareSync(password, user.password);
    }

    var createHash = function(password) {
        if(password != '')
            return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
    }

    var getTypeFunction = function(value, type) {
        if(type == 'like')
            return new RegExp('^' + value, 'i');
        else if(type == 'match')
            return new RegExp('^' + value + '$', 'i');
        else if(type == 'equals')
            return {$e : value};
        else if(type == 'notequals')
            return {$ne : value};
    }

    return controller;
};