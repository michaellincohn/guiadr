var sanitize = require('mongo-sanitize');

module.exports = function(app) {
    var Model = app.models.editoria;
    var controller = {}

    controller.listAll = function(req, res) {
        var conditions = {dtExclusao: null};

        Model
        .find(conditions)
        .sort({titulo: 1})
        .exec()
        .then(function(registers) {
            res.json(registers);
        });
    };

    controller.getById = function(req, res) {
        var _id = sanitize(req.params.id);
        Model
        .findById(_id)
        .exec()
        .then(function(register) {
            if (!register) throw new Error("Registro não encontrado");
            res.json(register);
        },
        function(erro) {
            res.status(404).json(erro)
        });
    };

    var getTypeFunction = function(value, type) {
        if(type == 'like')
            return new RegExp('^' + value, 'i');
        else if(type == 'match')
            return new RegExp('^' + value + '$', 'i');
        else if(type == 'equals')
            return {$e : value};
        else if(type == 'notequals')
            return {$ne : value};
    }

    return controller;
};