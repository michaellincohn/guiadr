var sanitize = require('mongo-sanitize');

module.exports = function(app) {
    var Model = app.models.blog;
    var controller = {};
    var perPage = 10;
    var totalItens = 0;

    controller.listAll = function(req, res) {
        var page = Math.max(0, (req.query.page - 1));
        var conditions = {dtExclusao: null, imagem: { $ne: null } };

        if(req.query.limit != null) {
            perPage = parseInt(req.query.limit);
        }

        Model
        .find(conditions)
        .populate('editoria')
        .limit(perPage)
        .skip(perPage * page)
        .sort({ _id: -1})
        .exec()
        .then(function(registers) {
            res.json(registers);
        });
    };

    controller.getById = function(req, res) {
        var _id = sanitize(req.params.id);
        Model
        .findById(_id)
        .populate('editoria')
        .exec()
        .then(function(register) {
            if (!register) throw new Error("Registro não encontrado");

            var comments = [];
            for (var i = 0; i < register.comentarios.length; i++) {
                if(register.comentarios[i].isAtivo) {
                    comments.push(register.comentarios[i]);
                }
            }

            res.json({
                post: register,
                comments: comments
            });
        },
        function(erro) {
            res.status(404).json(erro)
        });
    };

    controller.save = function(req, res) {
        var _id = sanitize(req.body.idPost);

        if(req.body.curtidas) {
            var dados = {
                "curtidas": req.body.post.curtidas + 1
            };
        }

        if(req.body.visualizacoes) {
            var dados = {
                "visualizacoes": req.body.post.visualizacoes + 1
            };
        }

        if(dados) {
            Model.findByIdAndUpdate(_id, dados)
            .exec()
            .then(function(register) {
                res.status(201).json({dados:dados});
            }, function(erro) {
                res.status(500).json({});
            });
        }
    };

    var getTypeFunction = function(value, type) {
        if(type == 'like')
            return new RegExp('^' + value, 'i');
        else if(type == 'match')
            return new RegExp('^' + value + '$', 'i');
        else if(type == 'equals')
            return {$e : value};
        else if(type == 'notequals')
            return {$ne : value};
    }

    return controller;
};