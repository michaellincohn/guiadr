var sanitize = require('mongo-sanitize');

module.exports = function(app) {
    var Model = app.models.usuario;
    var controller = {}
    var perPage = 1;
    var totalItens = 0;

    controller.listAll = function(req, res) {
        var page = Math.max(0, (req.query.page - 1));
        var cate = (req.query.categoria > 0 ? true : false);
        var espe = (req.query.especialidade > 0 ? true : false);
        var estado = (req.query.estado > 0 ? true : false);
        var cidade = (req.query.cidade > 0 ? true : false);
        var bairro = (req.query.bairro > 0 ? true : false);

        var conditions = {dtExclusao: null, tipo: 2, imagem: { $ne: null } };

        if(req.query.limit != null) {
            perPage = parseInt(req.query.limit);
        }

        if(estado) {
            conditions['endereco.estado'] = parseInt(req.query.estado);
        }
        if(cidade) {
            conditions['endereco.cidade._id'] = parseInt(req.query.cidade);
        }
        if(bairro) {
            conditions['endereco.bairro._id'] = parseInt(req.query.bairro);
        }
        if(cate) {
            conditions['categoria._id'] = parseInt(req.query.categoria);
        }
        if(espe) {
            conditions['categoria.especialidade._id'] = parseInt(req.query.especialidade);
        }

        Model
        .find(conditions)
        .limit(perPage)
        .skip(perPage * page)
        .sort({nome: 1})
        .exec()
        .then(function(registers) {
            res.json(registers);
            for (var i = 0; i < registers.length; i++) {
                console.log(registers[i].endereco);
            }
        },
        function(erro) {
            res.status(500).json(erro);
        });
    };

    controller.getById = function(req, res) {
        var _id = sanitize(req.params.id);
        Model
        .findById(_id)
        .exec()
        .then(function(register) {
            if (!register) throw new Error("Registro não encontrado");
            res.json(register);
        },
        function(erro) {
            res.status(404).json(erro)
        });
    };

    var getTypeFunction = function(value, type) {
        if(type == 'like')
            return new RegExp('^' + value, 'i');
        else if(type == 'match')
            return new RegExp('^' + value + '$', 'i');
        else if(type == 'equals')
            return {$e : value};
        else if(type == 'notequals')
            return {$ne : value};
    }

    return controller;
};