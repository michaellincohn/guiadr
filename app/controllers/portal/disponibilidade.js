var sanitize = require('mongo-sanitize');

module.exports = function(app) {
    var Model = app.models.disponibilidade;
    var controller = {}
    var perPage = 10;
    var totalItens = 0;

    controller.listAll = function(req, res) {
        var json = {};
        var page = Math.max(0, (req.query.page - 1));
        var tipo = (req.query.tipo ? true : false);
        var cate = (req.query.categoria ? true : false);
        var espe = (req.query.especialidade ? true : false);
        var data = (req.query.data ? true : false);
        var medi = (req.query.medico ? true : false);

        var conditions = {dtExclusao: null};

        if(cate) {
            conditions['categoria._id'] = req.query.categoria;
        }
        if(tipo) {
            conditions['tipo'] = parseInt(req.query.tipo);
        }
        if(espe) {
            conditions['categoria.especialidade._id'] = req.query.especialidade;
        }
        if(data) {
            var dtFrom = req.query.data.split('/');
            conditions['data'] = new Date(dtFrom[2], dtFrom[1] - 1, dtFrom[0]);
        }

        if(medi) {
            conditions['medico'] = req.query.medico;

            Model
            .find(conditions)
            .populate('categoria._id')
            .populate('horario')
            .exec()
            .then(function(registers) {
                res.json(registers);
            },
            function(erro) {
                res.status(500).json(erro);
            });
        } else {
            Model
            .find(conditions)
            .populate('medico')
            .populate('categoria._id')
            .limit(perPage)
            .skip(perPage * page)
            .sort({titulo: 1})
            .exec()
            .then(function(registers) {
                res.json(registers);
            },
            function(erro) {
                res.status(500).json(erro);
            });
        }
    };

    controller.getById = function(req, res) {
        var _id = sanitize(req.params.id);
        Model
        .findById(_id)
        .exec()
        .then(function(register) {
            if (!register) throw new Error("Registro não encontrado");
            res.json(register);
        },
        function(erro) {
            res.status(404).json(erro)
        });
    };

    var getTypeFunction = function(value, type) {
        if(type == 'like')
            return new RegExp('^' + value, 'i');
        else if(type == 'match')
            return new RegExp('^' + value + '$', 'i');
        else if(type == 'equals')
            return {$e : value};
        else if(type == 'notequals')
            return {$ne : value};
    }

    return controller;
};