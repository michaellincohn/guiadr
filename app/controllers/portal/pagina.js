var sanitize = require('mongo-sanitize');

module.exports = function(app) {
    var Model = app.models.pagina;
    var controller = {}

    controller.getByPage = function(req, res) {
        var _page = sanitize(req.params.page);
        Model
        .find({slug:_page})
        .exec()
        .then(function(register) {
            res.json(register);
        },
        function(erro) {
            res.status(404).json(erro)
        });
    };

    return controller;
};