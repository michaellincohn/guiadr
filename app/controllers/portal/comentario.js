var sanitize = require('mongo-sanitize');

module.exports = function(app) {
    var ModelBlog = app.models.blog;
    var ModelProfissional = app.models.blog;
    var controller = {}

    controller.save = function(req, res) {
        var _id = sanitize(req.body.idPost);
        var dados = {
            "nome": req.body.comentario.nome,
            "email": req.body.comentario.email,
            "descricao": req.body.comentario.descricao
        }

        if(req.body.module == 'blog') {
            ModelBlog.update(
                { '_id': _id },
                { $pushAll: { 'comentarios': [dados] } }
            )
            .exec()
            .then(function(register) {
                res.status(201).json({mensagem:"Obrigado por comentar!", class:"btn-primary"});
            }, function(erro) {
                res.status(500).json({});
            });
        } else if(req.body.module == 'profissional') {
            ModelProfissional.update(
                { '_id': _id },
                { $pushAll: { 'comentarios': [dados] } }
            )
            .exec()
            .then(function(register) {
                res.status(201).json({mensagem:"Obrigado por comentar!", class:"btn-primary"});
            }, function(erro) {
                res.status(500).json({});
            });
        }
    };

    return controller;
};