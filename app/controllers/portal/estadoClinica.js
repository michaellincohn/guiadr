module.exports = function(app) {
    var Model = app.models.usuario;
    var controller = {}

    controller.listAll = function(req, res) {
        var aggregate = [
            {$match: {
                tipo: 2,
                'endereco.pais': 'Brasil',
                dtExclusao: null
            }},
            {$project: {
                _id: 0,
                estado: '$endereco.pais.estado',
            }},
            {$group: {
                _id: '$estado',
            }},
            {$limit: 1000}
        ];

        Model.aggregate(aggregate, function(err, registro) {
            if(err) { res.status(500).json(err); }
            res.json(registro);
        });
    };

    return controller;
};