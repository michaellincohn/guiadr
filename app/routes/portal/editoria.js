module.exports = function(app) {
    var controller = app.controllers.portal.editoria;
    app.route('/editoria').get(controller.listAll);
};