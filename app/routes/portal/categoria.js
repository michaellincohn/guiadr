module.exports = function(app) {
    var controller = app.controllers.portal.categoria;
    app.route('/categoria').get(controller.listAll);
};