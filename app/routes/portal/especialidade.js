module.exports = function(app) {
    var controller = app.controllers.portal.especialidade;
    app.route('/especialidade').get(controller.listAll);
    app.route('/especialidade/:id').get(controller.getById);
};