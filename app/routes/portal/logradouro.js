module.exports = function(app) {
    var ctrl = app.controllers.portal.logradouro;
    app.route('/logradouro').get(ctrl.listAll).post(ctrl.save);
    app.route('/logradouro/:id').get(ctrl.getById);
};