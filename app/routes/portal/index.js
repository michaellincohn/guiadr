module.exports = function(app) {
	var controller = app.controllers.portal.index;
    app.route('/').get(controller.index);
};