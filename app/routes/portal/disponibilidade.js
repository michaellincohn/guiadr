module.exports = function(app) {
    var controller = app.controllers.portal.disponibilidade;
    app.route('/disponibilidade').get(controller.listAll);
    app.route('/disponibilidade/:id').get(controller.getById);
};