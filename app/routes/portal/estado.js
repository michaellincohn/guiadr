module.exports = function(app) {
    var ctrl = app.controllers.portal.estado;
    app.route('/estado').get(ctrl.listAll).post(ctrl.save);
    app.route('/estado/:id').get(ctrl.getById);
};