module.exports = function(app) {
	var passport = require('passport');
	app.get('/cadastro/paciente/facebook', passport.authenticate('facebook', { scope: 'email'}));
    app.get('/cadastro/paciente/facebook/callback', passport.authenticate('facebook', {
        successRedirect : '/#/conta/paciente',
        failureRedirect : '/#/cadastro/paciente'
    }));
};