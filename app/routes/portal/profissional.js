module.exports = function(app) {
    var controller = app.controllers.portal.profissional;
    app.route('/profissional')
    	.get(controller.listAll)
    	.post(controller.save);

    app.route('/profissional/:id')
    	.get(controller.getById);
};