module.exports = function(app) {
    var controller = app.controllers.portal.estadoClinica;
    app.route('/estadoClinica').get(controller.listAll);
};