module.exports = function(app) {
    var ctrl = app.controllers.portal.bairro;
    app.route('/bairro').get(ctrl.listAll).post(ctrl.save);
    app.route('/bairro/:id').get(ctrl.getById);
};