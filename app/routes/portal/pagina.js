module.exports = function(app) {
    var controller = app.controllers.portal.pagina;
    app.route('/pagina/:page').get(controller.getByPage);
};