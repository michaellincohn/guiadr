module.exports = function(app) {
    var controller = app.controllers.portal.blog;

    app.route('/blog')
    	.get(controller.listAll)
    	.post(controller.save);

    app.route('/blog/:id')
    	.get(controller.getById);
};