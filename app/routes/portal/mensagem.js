module.exports = function(app) {
    var controller = app.controllers.portal.mensagem;
    app.route('/mensagem').post(controller.save);
};