module.exports = function(app) {
    var controller = app.controllers.portal.usuario;
    app.route('/usuario')
    	.get(controller.login)
    	.post(controller.save);
};