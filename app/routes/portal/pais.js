module.exports = function(app) {
    var ctrl = app.controllers.portal.pais;
    app.route('/pais').get(ctrl.listAll).post(ctrl.save);
    app.route('/pais/:id').get(ctrl.getById);
};