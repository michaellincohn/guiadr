module.exports = function(app) {
    var controller = app.controllers.portal.clinica;
    app.route('/clinica').get(controller.listAll);
    app.route('/clinica/:id').get(controller.getById);
};