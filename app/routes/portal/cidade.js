module.exports = function(app) {
    var ctrl = app.controllers.portal.cidade;
    app.route('/cidade').get(ctrl.listAll).post(ctrl.save);
    app.route('/cidade/:id').get(ctrl.getById);
};