module.exports = function(app) {
    var controller = app.controllers.portal.contato;
    app.route('/contato').post(controller.save);
};