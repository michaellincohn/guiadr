var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.bairro;

    app.route('/painel/bairro')
        .get(common.checkAuthentication, ctrl.listAll)
        .post(common.checkAuthentication, ctrl.save);

    app.route('/painel/bairro/:id')
        .get(common.checkAuthentication, ctrl.getById)
        .delete(common.checkAuthentication, ctrl.removeById);
};