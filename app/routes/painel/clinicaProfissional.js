var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.clinicaProfissional;

    app.route('/painel/clinicaProfissional')
        .get(common.checkAuthentication, ctrl.listAll)
        .post(common.checkAuthentication, ctrl.save);

    app.route('/painel/clinicaProfissional/:id')
        .get(common.checkAuthentication, ctrl.getById)
        .delete(common.checkAuthentication, ctrl.removeById);
};