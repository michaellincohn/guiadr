module.exports = function(app) {
    app.get('/painel/auth/data', function(req, res) {
        var user = (req.user ? req.user : '' );
        res.json(user);
    });
    app.get('/painel/auth/logout', function(req, res) {
        req.logOut();
        res.redirect('/painel/auth/login');
    });

    // Login local
    var passport = require('passport');
    app.get('/painel/auth/login', function(req, res) {
        res.render('auth', { message: req.flash('message') });
    });
    app.post('/painel/auth/login', passport.authenticate('local', {
        successRedirect : '/painel/',
        failureRedirect : '/painel/auth/login',
        failureFlash    : true
    }));
};