var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.comentario;

    app.route('/painel/comentario')
        .post(common.checkAuthentication, ctrl.status);

    app.route('/painel/comentario/:id')
        .get(common.checkAuthentication, ctrl.listAll)
        .delete(common.checkAuthentication, ctrl.removeById);
};