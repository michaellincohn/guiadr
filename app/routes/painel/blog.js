var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.blog;

    app.route('/painel/blog')
        .get(common.checkAuthentication, ctrl.listAll)
        .post(common.checkAuthentication, ctrl.save);

    app.route('/painel/blog/:id')
        .get(common.checkAuthentication, ctrl.getById)
        .delete(common.checkAuthentication, ctrl.removeById);
};