var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.clinicaLocais;

    app.route('/painel/clinicaLocais')
        .get(common.checkAuthentication, ctrl.getById)
        .post(common.checkAuthentication, ctrl.save);

    app.route('/painel/clinicaLocais/:id')
        .get(common.checkAuthentication, ctrl.getById)
        .delete(common.checkAuthentication, ctrl.removeById);
};