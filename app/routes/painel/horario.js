var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.horario;

    app.route('/painel/horario')
        .get(common.checkAuthentication, ctrl.listAll)
        .post(common.checkAuthentication, ctrl.save);

    app.route('/painel/horario/:id')
        .get(common.checkAuthentication, ctrl.getById)
        .delete(common.checkAuthentication, ctrl.removeById);
};