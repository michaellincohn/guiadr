var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.consulta;
    
    app.route('/painel/consulta')
    	.get(common.checkAuthentication, ctrl.listAll)
    	.post(common.checkAuthentication, ctrl.status);
};