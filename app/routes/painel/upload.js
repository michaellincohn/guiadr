var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.upload;

    app.route('/painel/upload/:module')
        .post(common.checkAuthentication, multipartMiddleware, ctrl.upload);

    app.route('/painel/resize/:module')
        .post(common.checkAuthentication, ctrl.resize);
};