var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.pais;

    app.route('/painel/pais')
        .get(common.checkAuthentication, ctrl.listAll)
        .post(common.checkAuthentication, ctrl.save);

    app.route('/painel/pais/:id')
        .get(common.checkAuthentication, ctrl.getById)
        .delete(common.checkAuthentication, ctrl.removeById);
};