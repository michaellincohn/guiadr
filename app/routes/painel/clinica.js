var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.clinica;

    app.route('/painel/clinica')
        .get(common.checkAuthentication, ctrl.listAll)
        .post(common.checkAuthentication, ctrl.save);

    app.route('/painel/clinica/:id')
        .get(common.checkAuthentication, ctrl.getById)
        .delete(common.checkAuthentication, ctrl.removeById);
};