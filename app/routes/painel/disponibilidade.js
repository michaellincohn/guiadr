var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.disponibilidade;

    app.route('/painel/disponibilidade')
        .get(common.checkAuthentication, ctrl.listAll)
        .post(common.checkAuthentication, ctrl.save);

    app.route('/painel/disponibilidade/:id')
        .get(common.checkAuthentication, ctrl.getById)
        .delete(common.checkAuthentication, ctrl.removeById);
};