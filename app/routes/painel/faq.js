var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.faq;

    app.route('/painel/faq')
        .get(common.checkAuthentication, ctrl.listAll)
        .post(common.checkAuthentication, ctrl.save);

    app.route('/painel/faq/:id')
        .get(common.checkAuthentication, ctrl.getById)
        .delete(common.checkAuthentication, ctrl.removeById);
};