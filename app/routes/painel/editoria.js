var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.editoria;

    app.route('/painel/editoria')
        .get(common.checkAuthentication, ctrl.listAll)
        .post(common.checkAuthentication, ctrl.save);

    app.route('/painel/editoria/:id')
        .get(common.checkAuthentication, ctrl.getById)
        .delete(common.checkAuthentication, ctrl.removeById);
};