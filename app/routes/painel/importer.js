module.exports = function(app) {
    var ctrl = app.controllers.painel.importer;
    app.route('/painel/importer/bairro').get(ctrl.bairro);
    app.route('/painel/importer/cidade').get(ctrl.cidade);
    app.route('/painel/importer/estado').get(ctrl.estado);
    app.route('/painel/importer/logradouro').get(ctrl.logradouro);
};