var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.cidade;

    app.route('/painel/cidade')
        .get(common.checkAuthentication, ctrl.listAll)
        .post(common.checkAuthentication, ctrl.save);

    app.route('/painel/cidade/:id')
        .get(common.checkAuthentication, ctrl.getById)
        .delete(common.checkAuthentication, ctrl.removeById);
};