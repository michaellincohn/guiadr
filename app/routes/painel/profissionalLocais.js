var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.profissionalLocais;

    app.route('/painel/profissionalLocais')
        .get(common.checkAuthentication, ctrl.getById)
        .post(common.checkAuthentication, ctrl.save);

    app.route('/painel/profissionalLocais/:id')
        .get(common.checkAuthentication, ctrl.getById)
        .delete(common.checkAuthentication, ctrl.removeById);
};