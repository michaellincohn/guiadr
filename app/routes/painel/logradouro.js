var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.logradouro;

    app.route('/painel/logradouro')
        .get(common.checkAuthentication, ctrl.listAll)
        .post(common.checkAuthentication, ctrl.save);

    app.route('/painel/logradouro/:id')
        .get(common.checkAuthentication, ctrl.getById)
        .delete(common.checkAuthentication, ctrl.removeById);
};