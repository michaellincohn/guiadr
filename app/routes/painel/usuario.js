var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.usuario;

    app.route('/painel/usuario/insert').get(ctrl.example);

    app.route('/painel/usuario')
        .get(common.checkAuthentication, ctrl.listAll)
        .post(common.checkAuthentication, ctrl.save);

    app.route('/painel/usuario/:id')
        .get(common.checkAuthentication, ctrl.getById)
        .delete(common.checkAuthentication, ctrl.removeById);
};