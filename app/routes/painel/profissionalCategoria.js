var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.profissionalCategoria;

    app.route('/painel/profissionalCategoria')
        .get(common.checkAuthentication, ctrl.getById)
        .post(common.checkAuthentication, ctrl.save);

    app.route('/painel/profissionalCategoria/:id')
        .get(common.checkAuthentication, ctrl.getById)
        .delete(common.checkAuthentication, ctrl.removeById);
};