var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.pagina;

    app.route('/painel/pagina')
        .get(common.checkAuthentication, ctrl.listAll)
        .post(common.checkAuthentication, ctrl.save);

    app.route('/painel/pagina/:id')
        .get(common.checkAuthentication, ctrl.getById)
        .delete(common.checkAuthentication, ctrl.removeById);
};