var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.estado;

    app.route('/painel/estado')
        .get(common.checkAuthentication, ctrl.listAll)
        .post(common.checkAuthentication, ctrl.save);

    app.route('/painel/estado/:id')
        .get(common.checkAuthentication, ctrl.getById)
        .delete(common.checkAuthentication, ctrl.removeById);
};