var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.paciente;

    app.route('/painel/paciente/insert').get(ctrl.default);

    app.route('/painel/paciente')
        .get(common.checkAuthentication, ctrl.listAll);

    app.route('/painel/paciente/:id')
        .get(common.checkAuthentication, ctrl.getById)
        .delete(common.checkAuthentication, ctrl.removeById);
};