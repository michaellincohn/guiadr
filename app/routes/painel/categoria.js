var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.categoria;

    app.route('/painel/categoria/insert').get(ctrl.default);

    app.route('/painel/categoria')
        .get(common.checkAuthentication, ctrl.listAll)
        .post(common.checkAuthentication, ctrl.save);

    app.route('/painel/categoria/:id')
        .get(common.checkAuthentication, ctrl.getById)
        .delete(common.checkAuthentication, ctrl.removeById);
};