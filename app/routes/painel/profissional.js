var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.profissional;

    app.route('/painel/profissional')
        .get(common.checkAuthentication, ctrl.listAll)
        .post(common.checkAuthentication, ctrl.save);

    app.route('/painel/profissional/:id')
        .get(common.checkAuthentication, ctrl.getById)
        .delete(common.checkAuthentication, ctrl.removeById);
};