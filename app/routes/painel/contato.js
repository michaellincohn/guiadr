var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.contato;

    app.route('/painel/contato')
        .get(common.checkAuthentication, ctrl.listAll);

    app.route('/painel/contato/:id')
        .get(common.checkAuthentication, ctrl.getById)
        .delete(common.checkAuthentication, ctrl.removeById);
};