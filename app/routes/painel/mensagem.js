var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.painel.mensagem;

    app.route('/painel/mensagem')
        .get(common.checkAuthentication, ctrl.listAll);

    app.route('/painel/mensagem/:id')
        .get(common.checkAuthentication, ctrl.getById)
        .delete(common.checkAuthentication, ctrl.removeById);
};