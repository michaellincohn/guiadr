var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.restrito.profissionalLocais;

    app.route('/restrito/profissionalLocais')
        .get(common.checkAuthenticationRestrito, ctrl.getById)
        .post(common.checkAuthenticationRestrito, ctrl.save);

    app.route('/restrito/profissionalLocais/:id')
        .get(common.checkAuthenticationRestrito, ctrl.getById)
        .delete(common.checkAuthenticationRestrito, ctrl.removeById);
};