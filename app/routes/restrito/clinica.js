var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.restrito.clinica;

    app.route('/restrito/clinica')
        .get(common.checkAuthenticationRestrito, ctrl.listAll)
        .post(common.checkAuthenticationRestrito, ctrl.save);

    app.route('/restrito/clinica/:id')
        .get(common.checkAuthenticationRestrito, ctrl.getById)
        .delete(common.checkAuthenticationRestrito, ctrl.removeById);
};