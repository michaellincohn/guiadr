var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.restrito.disponibilidade;

    app.route('/restrito/disponibilidade')
        .get(common.checkAuthenticationRestrito, ctrl.listAll)
        .post(common.checkAuthenticationRestrito, ctrl.save);

    app.route('/restrito/disponibilidade/:id')
        .get(common.checkAuthenticationRestrito, ctrl.getById)
        .delete(common.checkAuthenticationRestrito, ctrl.removeById);
};