var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.restrito.profissionalCategoria;

    app.route('/restrito/profissionalCategoria')
        .get(common.checkAuthenticationRestrito, ctrl.getById)
        .post(common.checkAuthenticationRestrito, ctrl.save);

    app.route('/restrito/profissionalCategoria/:id')
        .get(common.checkAuthenticationRestrito, ctrl.getById)
        .delete(common.checkAuthenticationRestrito, ctrl.removeById);
};