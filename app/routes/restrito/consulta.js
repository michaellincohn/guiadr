var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.restrito.consulta;
    
    app.route('/restrito/consulta')
    	.get(common.checkAuthenticationRestrito, ctrl.listAll)
    	.post(common.checkAuthenticationRestrito, ctrl.status);
};