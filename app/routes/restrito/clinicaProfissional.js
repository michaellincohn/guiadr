var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.restrito.clinicaProfissional;

    app.route('/restrito/clinicaProfissional')
        .get(common.checkAuthenticationRestrito, ctrl.listAll)
        .post(common.checkAuthenticationRestrito, ctrl.save);

    app.route('/restrito/clinicaProfissional/:id')
        .get(common.checkAuthenticationRestrito, ctrl.getById)
        .delete(common.checkAuthenticationRestrito, ctrl.removeById);
};