var multipart = require('connect-multiparty');
var multipartMiddleware = multipart();
var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.restrito.upload;

    app.route('/restrito/upload/:module')
        .post(common.checkAuthenticationRestrito, multipartMiddleware, ctrl.upload);

    app.route('/restrito/resize/:module')
        .post(common.checkAuthenticationRestrito, ctrl.resize);
};