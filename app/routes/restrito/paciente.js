var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.restrito.paciente;

    app.route('/restrito/paciente/insert').get(ctrl.default);

    app.route('/restrito/paciente')
        .get(common.checkAuthenticationRestrito, ctrl.listAll);

    app.route('/restrito/paciente/:id')
        .get(common.checkAuthenticationRestrito, ctrl.getById)
        .delete(common.checkAuthenticationRestrito, ctrl.removeById);
};