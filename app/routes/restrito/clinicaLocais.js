var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.restrito.clinicaLocais;

    app.route('/restrito/clinicaLocais')
        .get(common.checkAuthenticationRestrito, ctrl.getById)
        .post(common.checkAuthenticationRestrito, ctrl.save);

    app.route('/restrito/clinicaLocais/:id')
        .get(common.checkAuthenticationRestrito, ctrl.getById)
        .delete(common.checkAuthenticationRestrito, ctrl.removeById);
};