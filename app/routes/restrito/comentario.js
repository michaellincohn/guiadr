var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.restrito.comentario;

    app.route('/restrito/comentario')
        .post(common.checkAuthenticationRestrito, ctrl.status);

    app.route('/restrito/comentario/:id')
        .get(common.checkAuthenticationRestrito, ctrl.listAll)
        .delete(common.checkAuthenticationRestrito, ctrl.removeById);
};