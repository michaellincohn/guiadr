function verificaAutenticacao(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    } else {
        res.redirect('/restrito/auth/logout');
    }
}

module.exports = function(app) {
    var ctrl = app.controllers.restrito.index;
    app.route('/restrito').get(verificaAutenticacao, ctrl.home);
};