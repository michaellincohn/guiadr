module.exports = function(app) {
    app.get('/restrito/auth/data', function(req, res) {
        var user = (req.user ? req.user : '');
        //if(user.tipo == 2) {
            res.json(user);
        //} else {
            //res.redirect('/restrito/auth/logout');
        //}
    });
    app.get('/restrito/auth/logout', function(req, res) {
        req.logOut();
        res.redirect('/restrito/auth/login');
    });

    // Login local
    var passport = require('passport');
    app.get('/restrito/auth/login', function(req, res) {
        res.render('auth-restrito', { message: req.flash('message') });
    });
    //app.post('/restrito/auth/login', passport.authenticate('restrito', function(err, user, info) {
        //if(err) { return next(err); }
        //if(!user) { return res.redirect('/'); }
        //if(user.tipo == 2) { return res.redirect('/restrito/'); } // Profissional
        //else if(user.tipo == 3) { return res.redirect('/clinica/'); } // Clinica
        //else if(user.tipo == 4) { return res.redirect('/paciente/'); } // Paciente
    //}));

    app.post('/restrito/auth/login', passport.authenticate('restrito', {
        successRedirect : '/restrito/',
        failureRedirect : '/restrito/auth/login',
        failureFlash    : true
    }));
};