module.exports = function(app) {
    var controller = app.controllers.restrito.categoria;
    app.route('/restrito/categoria').get(controller.listAll);
};