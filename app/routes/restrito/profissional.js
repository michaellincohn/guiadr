var common = require('../../../config/common');

module.exports = function(app) {
    var ctrl = app.controllers.restrito.profissional;
    app.route('/restrito/profissional')
    	.post(common.checkAuthenticationRestrito, ctrl.save)
    	.get(common.checkAuthenticationRestrito, ctrl.getById);
};