# README #

Este é um projeto em parceria com a PoloID em Natal/RN. Consiste em um aplicativo web completo de gerenciamento de consultas, com funções de agendamento, remarcação, avisos de proximidade da data agendada, controle de médicos/clínicas, etc...

### Com o que foi desenvolvido? ###

* MongoDB (DataBase)
* ExpressJS (Server-Side)
* AngularJS (Client-Side)
* NodeJS (Application-Server)
* Bootstrap (Design Layout)

### Quais as configurações necessárias ? ###
As configurações são básicas mas se faz extremamente importante, são elas:

* Rotas (Routes)
* Banco de dados (Database MongoDB)
* Dependências do gerenciador de pacotes do node (NPM)
* Dependências do gerenciador de pacotes para web (Bower)

### O projeto em produção ###
Ao entrar em produção, este projeto já contava com:

* Mais de 59 mil registros de logradouros do Brasil e Portugal
* Mais de 42 mil registros de cidades e seus bairros.
* Mais de 105 registros de estados
* Configuração das informações para funcionamento com administradores gerais.