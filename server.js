var http = require('http');
var passport = require('passport');
var app  = require('./config/express')();

var GuiaDrApp = function() {
	var self = this;

	self.setupVariables = function() {
        self.ipaddress = process.env.OPENSHIFT_NODEJS_IP;
        self.port      = process.env.OPENSHIFT_NODEJS_PORT || 8080;

        if (typeof self.ipaddress === "undefined") {
            console.warn('No OPENSHIFT_NODEJS_IP var, using 127.0.0.1');
            self.ipaddress = "127.0.0.1";
        };
    };

    self.terminator = function(sig){
        if (typeof sig === "string") {
           console.log('%s: Received %s - terminating sample app ...', Date(Date.now()), sig);
           process.exit(1);
        }
        console.log('%s: Node server stopped.', Date(Date.now()) );
    };

    self.setupTerminationHandlers = function(){
        process.on('exit', function() { self.terminator(); });

        // Removed 'SIGPIPE' from the list - bugz 852598.
        ['SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT',
         'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'
        ].forEach(function(element, index, array) {
            process.on(element, function() { self.terminator(element); });
        });
    };

    self.initializeServer = function() {
        self.app = http.createServer(app);
    };

    self.initializeDBServer = function() {
        require('./config/database')(self.ipaddress);
    };

    self.initialize = function() {
        self.setupVariables();
        self.initializeDBServer();
        self.setupTerminationHandlers();
        self.initializeServer();
        
        require('./config/passport')(passport, self.ipaddress);
    };

    self.start = function() {
        self.app.listen(self.port, self.ipaddress, function() {
            console.log('%s: Node server started on %s:%d ...', 
            	Date(Date.now() ), self.ipaddress, self.port);
        });
    };
};

var guiadrApp = new GuiaDrApp();
guiadrApp.initialize();
guiadrApp.start();